game.guiControls = {
	//Secondary variables
		
	
	//Methods
		checkMouseSelectionOf: function(controls, x, y) {
			for (let i = 0; i < controls.length; i++) {
				if (
					x >= controls[i].x - [
						0,
						3 * (controls[i].string || "").length
					][+(controls[i].type == "text")] &&
					x < controls[i].x + ({
						text: 3 * (controls[i].string || "").length,
						button: 120,
						squareButton: 20,
						textField: 120
					})[controls[i].type] &&
					y >= controls[i].y - [
						0,
						5
					][+(controls[i].type == "text")] &&
					y < controls[i].y + ({
						text: 5,
						button: 20,
						squareButton: 20,
						textField: 20
					})[controls[i].type]
				) {
					return i;
				}
			}
			
			return -1;
		},
		
		render: function(controls, selectedControl) {
			const layer = [];
			
			for (let i = 0; i < controls.length; i++) {
				switch (controls[i].type) {
					case "text":
						layer.push(
							{
								type: "text",
								
								fontSize: 8,
								fontColor: [
									game.colorScheme.text_light,
									game.colorScheme.text_selected
								][+(i == selectedControl)],
								textAlign: "center",
								
								string: controls[i].string,
								
								x: controls[i].x,
								y: controls[i].y
							}
						);
						
						break;
					case "button":
						layer.push(
							{
								type: "sprite",
								
								texture: game.textures.gui_button,
								
								y0: [
									0,
									20
								][+(i == selectedControl)],
								
								height0: 20,
								
								x: controls[i].x,
								y: controls[i].y,
								
								height: 20
							},
							
							{
								type: "text",
								
								fontSize: 12,
								fontColor: game.colorScheme.text_dark,
								textAlign: "center",
								
								string: controls[i].caption,
								
								x: controls[i].x + 60,
								y: controls[i].y + 8
							}
						);
						
						break;
					case "squareButton":
						layer.push(
							{
								type: "sprite",
								
								texture: game.textures.gui_squareButton,
								
								y0: [
									0,
									20
								][+(i == selectedControl)],
								
								height0: 20,
								
								x: controls[i].x,
								y: controls[i].y,
								
								height: 20
							},
							
							{
								type: "text",
								
								fontSize: 12,
								fontColor: game.colorScheme.text_dark,
								textAlign: "center",
								
								string: controls[i].caption,
								
								x: controls[i].x + 10,
								y: controls[i].y + 8
							}
						);
						
						break;
					case "textField":
						layer.push(
							{
								type: "sprite",
								
								texture: game.textures.gui_textField,
								
								y0: [
									0,
									20
								][+(i == selectedControl)],
								
								height0: 20,
								
								x: controls[i].x,
								y: controls[i].y,
								
								height: 20
							},
							
							{
								type: "text",
								
								fontSize: 12,
								fontColor: game.colorScheme.text_dark,
								textAlign: "center",
								
								string: controls[i].string.slice(0, 15),
								
								x: controls[i].x + 60,
								y: controls[i].y + 8
							}
						);
						
						break;
				}
			}
			
			return layer;
		}
};
