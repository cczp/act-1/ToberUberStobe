game.locales["ru-RU"] = {
	//GUI
		//Common
			gui_page:
				"Страница ",
		
		//Dialogs
			gui_dialogs_enterNewValue:
				"Введи новое значение",
			
			//Game world
				//Default
					gui_dialogs_gameWorld_default_commandEntry:
						"Введи команду",
				
				//Sign
					gui_dialogs_gameWorld_sign:
						"Введи текст для таблички",
				
				//Console
					gui_dialogs_gameWorld_console:
						"Введи команду",
		
		//Menu
			gui_menu_play:
				"Играть",
			gui_menu_settings:
				"Настройки",
		
		//Settings
			gui_settings_title:
				"Настройки",
			
			//Controls
				gui_settings_controls_title:
					"Управление",
				gui_settings_controls_up:
					"Вверх",
				gui_settings_controls_down:
					"Вниз",
				gui_settings_controls_left:
					"Влево",
				gui_settings_controls_right:
					"Вправо",
				gui_settings_controls_switch:
					"Переключить",
				gui_settings_controls_quit:
					"Выйти",
				gui_settings_controls_use:
					"Использовать",
				gui_settings_controls_run:
					"Бежать",
			
			//Controls (2)
				gui_settings_controls2_title:
					"Управление (2)",
				gui_settings_controls2_command:
					"Команда",
				gui_settings_controls2_magic:
					"Магия",
			
			//Language
				gui_settings_language_title:
					"Язык",
			
			//Tweaks
				gui_settings_tweaks_title:
					"Твики",
				gui_settings_tweaks_enableCoolHackerMode:
					"Включить режим крутого хакера".padEnd(43),
				gui_settings_tweaks_enableBackgroundMusic:
					"Включить фоновую музыку".padEnd(43),
		
		//Game world
			//Default
				gui_gameWorld_default_godMode:
					"РЕЖИМ БОГА",
			
			//Pause
				gui_gameWorld_pause_title:
					"Пауза",
				gui_gameWorld_pause_resume:
					"Продолжить",
				gui_gameWorld_pause_info:
					"Информация",
				gui_gameWorld_pause_exit:
					"Выйти",
			
			//Inventory
				gui_gameWorld_inventory_title:
					"Инвентарь",
				
				//God inventory
					gui_gameWorld_godInventory_allItems:
						"Все предметы",
					gui_gameWorld_godInventory_title:
						"Инвентарь",
			
			//Church
				gui_gameWorld_church_thirst:
					"Жажда",
				gui_gameWorld_church_health:
					"Здоровье",
				gui_gameWorld_church_hunger:
					"Голод",
			
			//Chest
				gui_gameWorld_chest_title:
					"Сундук",
				gui_gameWorld_chest_inventory:
					"Инвентарь",
			
			//Player
				gui_gameWorld_player_inventory:
					"Инвентарь",
			
			//Oven
				gui_gameWorld_oven_title:
					"Печь",
				gui_gameWorld_oven_inventory:
					"Инвентарь",
			
			//Background changer
				gui_gameWorld_backgroundChanger_title:
					"Изменитель фона",
			
			//Selfreplacer
				gui_gameWorld_selfreplacer_title:
					"Себязаменитель",
			
			//Particle placer
				gui_gameWorld_particlePlacer_title:
					"Установщик частиц",
		
		//Game over
			gui_gameOver_title:
				"ТЫ МЁРТВ"
};
