game.locales["en-US"] = {
	//GUI
		//Common
			gui_page:
				"Page ",
		
		//Dialogs
			gui_dialogs_enterNewValue:
				"Enter new value",
			
			//Game world
				//Default
					gui_dialogs_gameWorld_default_commandEntry:
						"Enter command",
				
				//Sign
					gui_dialogs_gameWorld_sign:
						"Enter text for sign",
				
				//Console
					gui_dialogs_gameWorld_console:
						"Enter command",
		
		//Menu
			gui_menu_play:
				"Play",
			gui_menu_settings:
				"Settings",
		
		//Settings
			gui_settings_title:
				"Settings",
			
			//Controls
				gui_settings_controls_title:
					"Controls",
				gui_settings_controls_up:
					"Up",
				gui_settings_controls_down:
					"Down",
				gui_settings_controls_left:
					"Left",
				gui_settings_controls_right:
					"Right",
				gui_settings_controls_switch:
					"Switch",
				gui_settings_controls_quit:
					"Quit",
				gui_settings_controls_use:
					"Use",
				gui_settings_controls_run:
					"Run",
			
			//Controls (2)
				gui_settings_controls2_title:
					"Controls (2)",
				gui_settings_controls2_command:
					"Command",
				gui_settings_controls2_magic:
					"Magic",
			
			//Language
				gui_settings_language_title:
					"Language",
			
			//Tweaks
				gui_settings_tweaks_title:
					"Tweaks",
				gui_settings_tweaks_enableCoolHackerMode:
					"Enable cool hacker mode".padEnd(43),
				gui_settings_tweaks_enableBackgroundMusic:
					"Enable background music".padEnd(43),
		
		//Game world
			//Default
				gui_gameWorld_default_godMode:
					"GOD MODE",
			
			//Pause
				gui_gameWorld_pause_title:
					"Pause",
				gui_gameWorld_pause_resume:
					"Resume",
				gui_gameWorld_pause_info:
					"Info",
				gui_gameWorld_pause_exit:
					"Exit",
			
			//Inventory
				gui_gameWorld_inventory_title:
					"Inventory",
				
				//God inventory
					gui_gameWorld_godInventory_allItems:
						"All items",
					gui_gameWorld_godInventory_title:
						"Inventory",
			
			//Church
				gui_gameWorld_church_thirst:
					"Thirst",
				gui_gameWorld_church_health:
					"Health",
				gui_gameWorld_church_hunger:
					"Hunger",
			
			//Chest
				gui_gameWorld_chest_title:
					"Chest",
				gui_gameWorld_chest_inventory:
					"Inventory",
			
			//Player
				gui_gameWorld_player_inventory:
					"Inventory",
			
			//Oven
				gui_gameWorld_oven_title:
					"Oven",
				gui_gameWorld_oven_inventory:
					"Inventory",
			
			//Background changer
				gui_gameWorld_backgroundChanger_title:
					"Background changer",
			
			//Selfreplacer
				gui_gameWorld_selfreplacer_title:
					"Selfreplacer",
			
			//Particle placer
				gui_gameWorld_particlePlacer_title:
					"Particle placer",
		
		//Game over
			gui_gameOver_title:
				"YOU ARE DEAD"
};
