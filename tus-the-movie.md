Yes, this was actually shot and edited. I'm sorry.

```
                 ToberUberStobe: The Movie
                      Written by ⚠️🚱

Every scene is narrated by an unintelligible distorted voice
saying random unrelated to the plot things.

CREDIT SEQUENCE

                      ToberUberStobe:
                      The
                      Movie

                    September 13th, 1985
                         Kiev, USSR

                                                     CUT TO:

INT. TYPICAL SOVIET APARTMENT - MORNING

IZZY LAIF, an Arab-born Japanese software developer, wakes
up in his bed.

EXT. TYPICAL SOVIET APARTMENT - DAY

Izzy walks to the airport.

EXT. PLANE - THUNDER

The AIRPLANE flies in the air.

INT. PLANE - THUNDER

Izzy looks around.

EXT. PLANE - THUNDER

The airplane keeps flying.

                                               CUT TO BLACK:

An EXPLOSION can be heard.

                                                     CUT TO:

EXT. KARAKUM DESERT - DAY

Izzy wakes up and looks around.

EXT. KARAKUM DESERT - AFTERNOON

Izzy breaks a cactus and kills a wild cat with his own
hands.

EXT. KARAKUM DESERT - NIGHT

Izzy sits in his HUT looking at the CAMPFIRE.

EXT. KARAKUM DESERT - DAY

Izzy walks though the desert.

                                                     CUT TO:

EXT. GIANT ACACIA TREE - DAY

NIKOLAI VALUEV, dressed in a green lycra suit, jumps off the
tree.

EXT. KARAKUM DESERT - DAY

Izzy and Nikolai walk through the desert when they suddenly
find a fully functioning ZHIGULI CAR.

EXT. CAR - DAY

Izzy and Nikolai drive trough the desert.

The car breaks down.

EXT. FACTORY - DAY

Izzy walks though the factory, which floor is covered with
red bricks.

A BOTTLE CORK suddenly fires and kills Izzy.

                                               CUT TO BLACK:

SCREAMING can be heard.

EXT. ITALIAN TOWN - NIGHT

Izzy walks out one door, looks around, and enters another.

                                               CUT TO BLACK:

SCREAMING can be heard.

EXT. HEAVEN - DAY

Izzy wakes up.

Izzy looks around and sees the FLYING SPAGHETTI MONSTER.

                                               CUT TO BLACK:

ANNOYING OLD MAN'S SCREAMING can be heard.

EXT. ARCTIC BASE - NIGHT

Izzy wakes up as a Russian solider guarding the base.

                                               CUT TO BLACK:

                          THE END
```
