## GNU General Public License, version 2 or later (LICENSE.GPL2)

* font.woff ([GNU Unifont v11.0.02](http://unifoundry.com/) by Free Software Foundation)

## Creative Commons Attribution 3.0 Unported (LICENSE.CCBY)

* sounds/gui/background.ogg ((modified (slowed down)) [Desert City](http://incompetech.com/) by Kevin MacLeod)

## BSD Zero Clause License (LICENSE.0BSD)

* All other files
