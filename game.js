Object.assign(game, {
	//Secondary variables
		name: "ToberUberStobe",
		version: "7.0.0-beta",
		
		screen: {
			width: 320,
			height: 240,
			
			multiplierX: 3,
			multiplierY: 3
		},
	
	//Methods
		postStart: function() {
			this.currentRealm = this.realms.menu;
			
			this.debugDataDisplayed = this.config.list.main.tweaks.coolHackerModeEnabled;
			
			if (this.config.list.main.tweaks.backgroundMusicEnabled) {
				this.sounds.gui_background.play();
			}
			
			if (!("launchOptions" in this.config.list.main)) {
				game.currentRealm = game.realms.controls;
				
				game.config.list.main.launchOptions = null;
				
				game.input.keyboard.mode = -game.input.keyboard.mode - 1;
				game.input.mouse.mode = -game.input.mouse.mode - 1;
				
				game.loadingScreen.hidden = false;
				
				game.config.write("main").then(function() {
					game.input.keyboard.mode = -game.input.keyboard.mode - 1;
					game.input.mouse.mode = -game.input.mouse.mode - 1;
					
					game.loadingScreen.hidden = true;
				});
			}
			
			game.realms.gameWorld.chunkUpdater = setInterval(function() {
				if (
					game.currentRealm != game.realms.gameWorld ||
					game.realms.gameWorld.currentGuiType == game.realms.gameWorld.guiTypes.pause
				) {
					return;
				}
				
				game.realms.gameWorld.updateChunk();
				
				game.realms.gameWorld.updatedAreas.length = 0;
				
				if (game.realms.gameWorld.tick < 24) {
					game.realms.gameWorld.tick++;
				} else {
					game.realms.gameWorld.tick = 0;
				}
			}, 40);
		}
});

Object.assign(game.config.defaultList, {
	main: {
		locale:
			"en-US",
		
		controls: {
			up:
				"KeyW",
			down:
				"KeyS",
			left:
				"KeyA",
			right:
				"KeyD",
			switch:
				"KeyG",
			quit:
				"KeyH",
			use:
				"KeyK",
			run:
				"KeyL",
			command:
				"KeyT",
			magic:
				"KeyY"
		},
		
		tweaks: {
			coolHackerModeEnabled:
				false,
			backgroundMusicEnabled:
				true
		}
	},
	
	gameWorld: {
		clusters: {},
		clusterSize: 96,
		singleBiome: "",
		
		cameraPosition: {},
		
		playerPosition: {}
	}
});

game.input.navigation.catch = function(event) {
	if (event) {
		event.preventDefault();
		event.stopPropagation();
	}
	
	game.input.keyboard.catch(
		game.config.list.main.controls.quit,
	"press");
	game.input.keyboard.catch(
		game.config.list.main.controls.quit,
	"release");
};
