game.realms.settings = {
	//Secondary variables
		controls: [],
		selectedPage: 0,
		selectedControl: 0,
	
	//Methods
		update: function() {
			this.handleInput();
			
			return [
				this.renderBase(),
				this.renderControls()
			];
		},
		
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				const selectedControl = game.guiControls.checkMouseSelectionOf(
					this.controls[this.selectedPage],
					
					game.input.mouse.moved[i][0],
					game.input.mouse.moved[i][1]
				);
				
				if (selectedControl >= 0 && this.selectedControl != selectedControl) {
					game.sounds.gui_scroll.play();
					
					this.selectedControl = selectedControl;
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				//Activate selected control
					const pressedControl = game.guiControls.checkMouseSelectionOf(
						this.controls[this.selectedPage],
						
						game.input.mouse.released[i][0],
						game.input.mouse.released[i][1]
					);
					
					if (pressedControl >= 0) {
						game.sounds.gui_click.play();
						
						(
							this.controls[this.selectedPage][pressedControl].event ||
							function() {}
						)();
						
						continue;
					}
				
				//Scroll pages
					if (
						game.input.mouse.released[i][1] >= 113 &&
						game.input.mouse.released[i][1] < 130
					) {
						if (
							game.input.mouse.released[i][0] >= 15 &&
							game.input.mouse.released[i][0] < 32
						) {
							game.input.keyboard.pressed.push(game.config.list.main.controls.left);
						} else if (
							game.input.mouse.released[i][0] >= 288 &&
							game.input.mouse.released[i][0] < 305
						) {
							game.input.keyboard.pressed.push(game.config.list.main.controls.right);
						}
					}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.input.keyboard.mode = -game.input.keyboard.mode - 1;
						game.input.mouse.mode = -game.input.mouse.mode - 1;
						
						game.loadingScreen.hidden = false;
						
						game.config.write("main").then(function() {
							game.input.keyboard.mode = -game.input.keyboard.mode - 1;
							game.input.mouse.mode = -game.input.mouse.mode - 1;
							
							game.loadingScreen.hidden = true;
							
							game.currentRealm = game.realms.menu;
						});
						
						break;
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						(
							this.controls[this.selectedPage][this.selectedControl].event ||
							function() {}
						)();
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (
							this.selectedControl <
								this.controls[this.selectedPage].length - 1
						) {
							this.selectedControl++;
						} else {
							this.selectedControl = 0;
						}
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.selectedPage > 0) {
							this.selectedPage--;
						} else {
							this.selectedPage = this.controls.length - 1;
						}
						this.selectedControl = 0;
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.selectedControl > 0) {
							this.selectedControl--;
						} else {
							this.selectedControl =
								this.controls[this.selectedPage].length - 1;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.selectedPage < this.controls.length - 1) {
							this.selectedPage++;
						} else {
							this.selectedPage = 0;
						}
						this.selectedControl = 0;
						
						break;
				}
			}
		},
		
		renderBase: function() {
			return game.realms.menu.renderBase().concat([
				{
					type: "sprite",
					
					texture: game.textures.gui_settings,
					
					x: 0,
					y: 0
				}
			]);
		},
		renderControls: function() {
			return [
				{
					type: "text",
					
					fontSize: 16,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: game.currentLocale.gui_settings_title,
					
					x: 160,
					y: 14
				},
				
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string:
						game.currentLocale.gui_page +
						(this.selectedPage + 1) + "/" +
						this.controls.length,
					
					x: 160,
					y: 228
				}
			].concat(game.guiControls.render(this.controls[
				this.selectedPage
			], this.selectedControl));
		}
};
