game.realms.settings.controls.push([
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls2_title;
		},
		
		x: 160,
		y: 33
	},
	
	//---
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls2_command;
		},
		
		x: 98,
		y: 53
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.command;
		},
		
		x: 38,
		y: 66,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.command
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.command = value;
				} else if (value == "") {
					game.config.list.main.controls.command = game.config.defaultList.main.controls.command;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls2_magic;
		},
		
		x: 222,
		y: 53
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.magic;
		},
		
		x: 162,
		y: 66,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.magic
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.magic = value;
				} else if (value == "") {
					game.config.list.main.controls.magic = game.config.defaultList.main.controls.magic;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return "";
		},
		
		x: 98,
		y: 97
	},
	{
		type: "textField",
		
		get string() {
			return "";
		},
		
		x: 38,
		y: 110,
		
		event: function() {}
	},
	
	{
		type: "text",
		
		get string() {
			return "";
		},
		
		x: 222,
		y: 97
	},
	{
		type: "textField",
		
		get string() {
			return "";
		},
		
		x: 162,
		y: 110,
		
		event: function() {}
	},
	
	{
		type: "text",
		
		get string() {
			return "";
		},
		
		x: 98,
		y: 141
	},
	{
		type: "textField",
		
		get string() {
			return "";
		},
		
		x: 38,
		y: 154,
		
		event: function() {}
	},
	
	{
		type: "text",
		
		get string() {
			return "";
		},
		
		x: 222,
		y: 141
	},
	{
		type: "textField",
		
		get string() {
			return "";
		},
		
		x: 162,
		y: 154,
		
		event: function() {}
	},
	
	{
		type: "text",
		
		get string() {
			return "";
		},
		
		x: 98,
		y: 185
	},
	{
		type: "textField",
		
		get string() {
			return "";
		},
		
		x: 38,
		y: 198,
		
		event: function() {}
	},
	
	{
		type: "text",
		
		get string() {
			return "";
		},
		
		x: 222,
		y: 185
	},
	{
		type: "textField",
		
		get string() {
			return "";
		},
		
		x: 162,
		y: 198,
		
		event: function() {}
	}
]);
