game.realms.settings.controls.push([
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_language_title;
		},
		
		x: 160,
		y: 33
	},
	
	//---
	
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.locale;
		},
		
		x: 100,
		y: 45,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.locale
			).then(function(value) {
				if (value) {
					game.config.list.main.locale = value;
				} else if (value == "") {
					game.config.list.main.locale = game.config.defaultList.main.locale;
				}
			});
		}
	},
	
	{
		type: "text",
		
		string: "English (US)",
		
		x: 160,
		y: 77,
		
		event: function() {
			game.config.list.main.locale = "en-US";
		}
	},
	{
		type: "text",
		
		string: "Русский (Россия)",
		
		x: 160,
		y: 89,
		
		event: function() {
			game.config.list.main.locale = "ru-RU";
		}
	}
]);
