game.realms.settings.controls.push([
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_tweaks_title;
		},
		
		x: 160,
		y: 33
	},
	
	//---
	
	{
		type: "text",
		
		get string() {
			return (
				game.currentLocale.gui_settings_tweaks_enableCoolHackerMode +
				["✖", "✔"][+game.config.list.main.tweaks.coolHackerModeEnabled]
			);
		},
		
		x: 160,
		y: 46,
		
		event: function() {
			game.debugDataDisplayed =
				game.config.list.main.tweaks.coolHackerModeEnabled =
					!game.config.list.main.tweaks.coolHackerModeEnabled;
		}
	},
	
	{
		type: "text",
		
		get string() {
			return (
				game.currentLocale.gui_settings_tweaks_enableBackgroundMusic +
				["✖", "✔"][+game.config.list.main.tweaks.backgroundMusicEnabled]
			);
		},
		
		x: 160,
		y: 58,
		
		event: function() {
			game.config.list.main.tweaks.backgroundMusicEnabled = !game.config.list.main.tweaks.backgroundMusicEnabled;
			
			if (game.config.list.main.tweaks.backgroundMusicEnabled) {
				game.sounds.gui_background.play();
			} else {
				game.sounds.gui_background.pause();
			}
		}
	}
]);
