game.realms.settings.controls.push([
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_title;
		},
		
		x: 160,
		y: 33
	},
	
	//---
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_up;
		},
		
		x: 98,
		y: 53
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.up;
		},
		
		x: 38,
		y: 66,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.up
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.up = value;
				} else if (value == "") {
					game.config.list.main.controls.up = game.config.defaultList.main.controls.up;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_down;
		},
		
		x: 222,
		y: 53
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.down;
		},
		
		x: 162,
		y: 66,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.down
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.down = value;
				} else if (value == "") {
					game.config.list.main.controls.down = game.config.defaultList.main.controls.down;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_left;
		},
		
		x: 98,
		y: 97
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.left;
		},
		
		x: 38,
		y: 110,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.left
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.left = value;
				} else if (value == "") {
					game.config.list.main.controls.left = game.config.defaultList.main.controls.left;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_right;
		},
		
		x: 222,
		y: 97
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.right;
		},
		
		x: 162,
		y: 110,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.right
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.right = value;
				} else if (value == "") {
					game.config.list.main.controls.right = game.config.defaultList.main.controls.right;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_switch;
		},
		
		x: 98,
		y: 141
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.switch;
		},
		
		x: 38,
		y: 154,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.switch
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.switch = value;
				} else if (value == "") {
					game.config.list.main.controls.switch = game.config.defaultList.main.controls.switch;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_quit;
		},
		
		x: 222,
		y: 141
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.quit;
		},
		
		x: 162,
		y: 154,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.quit
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.quit = value;
				} else if (value == "") {
					game.config.list.main.controls.quit = game.config.defaultList.main.controls.quit;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_use;
		},
		
		x: 98,
		y: 185
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.use;
		},
		
		x: 38,
		y: 198,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.use
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.use = value;
				} else if (value == "") {
					game.config.list.main.controls.use = game.config.defaultList.main.controls.use;
				}
			});
		}
	},
	
	{
		type: "text",
		
		get string() {
			return game.currentLocale.gui_settings_controls_run;
		},
		
		x: 222,
		y: 185
	},
	{
		type: "textField",
		
		get string() {
			return game.config.list.main.controls.run;
		},
		
		x: 162,
		y: 198,
		
		event: function() {
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_enterNewValue,
				game.config.list.main.controls.run
			).then(function(value) {
				if (value) {
					game.config.list.main.controls.run = value;
				} else if (value == "") {
					game.config.list.main.controls.run = game.config.defaultList.main.controls.run;
				}
			});
		}
	}
]);
