game.realms.gameWorld.particleTypes.attack = {
	update: game.realms.gameWorld.particleTypes._genericGarbage.update,
	
	render: function(particle, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.particles_attack,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	New: function GameWorldParticle(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.particleTypes.attack.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "attack",
		
		lifetime: 5
	}
};
