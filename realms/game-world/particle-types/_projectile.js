game.realms.gameWorld.particleTypes._projectile = {
	update: function(particle, x, y) {
		let particleUpdated = false;
		
		if (game.realms.gameWorld.updatedAreas.includes(particle)) {
			particleUpdated = true;
		} else {
			game.realms.gameWorld.updatedAreas.push(particle);
		}
		
		if (!particleUpdated) {
			particle.lifetime--;
		}
		
		if (game.realms.gameWorld.objects[[x, y]]) {
			game.realms.gameWorld.particles[[x, y]] = null;
			
			game.realms.gameWorld.objects[[x, y]].health -= particle.attackDamage;
			
			return;
		}
		
		if (!particleUpdated) {
			switch (particle.rotation) {
				case 0:
					[
						game.realms.gameWorld.particles[[x, y]],
						game.realms.gameWorld.particles[[x, y + 1]]
					] = [
						game.realms.gameWorld.particles[[x, y + 1]],
						game.realms.gameWorld.particles[[x, y]]
					];
					
					y++;
					
					break;
				case 1:
					[
						game.realms.gameWorld.particles[[x, y]],
						game.realms.gameWorld.particles[[x - 1, y]]
					] = [
						game.realms.gameWorld.particles[[x - 1, y]],
						game.realms.gameWorld.particles[[x, y]]
					];
					
					x--;
					
					break;
				case 2:
					[
						game.realms.gameWorld.particles[[x, y]],
						game.realms.gameWorld.particles[[x, y - 1]]
					] = [
						game.realms.gameWorld.particles[[x, y - 1]],
						game.realms.gameWorld.particles[[x, y]]
					];
					
					y--;
					
					break;
				case 3:
					[
						game.realms.gameWorld.particles[[x, y]],
						game.realms.gameWorld.particles[[x + 1, y]]
					] = [
						game.realms.gameWorld.particles[[x + 1, y]],
						game.realms.gameWorld.particles[[x, y]]
					];
					
					x++;
					
					break;
			}
		}
		
		if (particle.lifetime < 0) {
			game.realms.gameWorld.particles[[x, y]] = null;
		}
	},
	
	prototype: {
		type: "",
		
		attackDamage: 0,
		
		lifetime: 0,
		
		rotation: 0
	}
};
Object.defineProperty(
	game.realms.gameWorld.particleTypes, "_projectile",
	
	{
		enumerable: false
	}
);
