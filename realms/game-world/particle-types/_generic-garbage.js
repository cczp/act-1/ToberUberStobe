game.realms.gameWorld.particleTypes._genericGarbage = {
	update: function(particle, x, y) {
		let particleUpdated = false;
		
		if (game.realms.gameWorld.updatedAreas.includes(particle)) {
			particleUpdated = true;
		} else {
			game.realms.gameWorld.updatedAreas.push(particle);
		}
		
		if (!particleUpdated) {
			particle.lifetime--;
		}
		
		if (particle.lifetime < 0) {
			game.realms.gameWorld.particles[[x, y]] = null;
		}
	},
	
	prototype: {
		type: "",
		
		lifetime: 0
	}
};
Object.defineProperty(
	game.realms.gameWorld.particleTypes, "_genericGarbage",
	
	{
		enumerable: false
	}
);
