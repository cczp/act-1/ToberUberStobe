game.realms.gameWorld.particleTypes.arrow = {
	update: game.realms.gameWorld.particleTypes._projectile.update,
	
	render: function(particle, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.particles_arrow,
			
			x0: particle.rotation * 16,
			
			width0: 16,
			height0: 16,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	New: function GameWorldParticle(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.particleTypes.arrow.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "arrow",
		
		attackDamage: 5,
		
		lifetime: 6,
		
		rotation: 0
	}
};
