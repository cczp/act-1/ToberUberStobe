game.realms.gameWorld.objectTypes.stoneWall = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_stoneWall,
			
			y0: 32 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 32,
			
			x: x,
			y: y - 16,
			
			width: 16,
			height: 32
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.stoneWall.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stoneWall",
		
		inventory: [],
		
		health: 100
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.rock.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.rock.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.rock.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.rock.New,
			
			chance: 1
		}
	]
};
