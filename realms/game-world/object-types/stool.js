game.realms.gameWorld.objectTypes.stool = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_stool,
			
			width0: 16,
			height0: 16 + +(
				object.health <= this.prototype.health / 2
			),
			
			x: x,
			y: y,
			
			width: 16,
			height: 16 + +(
				object.health <= this.prototype.health / 2
			)
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.stool.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stool",
		
		inventory: [],
		
		health: 8
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.parquet.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.sticks.New,
			
			chance: 0.75
		},
		{
			New: game.realms.gameWorld.itemTypes.sticks.New,
			
			chance: 0.75
		}
	]
};
