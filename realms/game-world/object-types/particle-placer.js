game.realms.gameWorld.objectTypes.particlePlacer = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update_immortal,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_particlePlacer,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.particlePlacer,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.particlePlacer.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "particlePlacer",
		
		inventory: [],
		
		health: 0
	},
	drop: []
};
