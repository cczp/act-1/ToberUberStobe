game.realms.gameWorld.objectTypes.backgroundChanger = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update_immortal,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_backgroundChanger,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.backgroundChanger,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.backgroundChanger.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "backgroundChanger",
		
		inventory: [],
		
		health: 0
	},
	drop: []
};
