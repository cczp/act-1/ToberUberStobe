game.realms.gameWorld.objectTypes._mob = {
	update: function(object, x, y) {
		let objectUpdated = false;
		
		if (game.realms.gameWorld.updatedAreas.includes(object)) {
			objectUpdated = true;
		} else {
			game.realms.gameWorld.updatedAreas.push(object);
		}
		
		if (!objectUpdated) {
			[x, y] = game.realms.gameWorld.useAiOn(object, x, y);
		}
		
		if (object.health <= 0) {
			game.sounds.entities_death.play()
			
			game.realms.gameWorld.objects[[x, y]] = null;
		}
	},
	update_neutral: function(object, x, y) {
		let objectUpdated = false;
		
		if (game.realms.gameWorld.updatedAreas.includes(object)) {
			objectUpdated = true;
		} else {
			game.realms.gameWorld.updatedAreas.push(object);
		}
		
		if (object.health < this.prototype.health) {
			object.canWalkAround = false;
			object.canWalk = true;
			object.canAttack = true;
		}
		
		if (!objectUpdated) {
			[x, y] = game.realms.gameWorld.useAiOn(object, x, y);
		}
		
		if (object.health <= 0) {
			game.sounds.entities_death.play()
			
			game.realms.gameWorld.objects[[x, y]] = null;
		}
	},
	
	attack: function(object, x, y) {
		if (!game.realms.gameWorld.particles[[x, y]]) {
			game.realms.gameWorld.particles[[x, y]] = new game.realms.gameWorld.particleTypes.attack.New();
		}
		
		game.sounds.entities_attack.play();
		
		const
			selectedPoint = this.selectedPointOf(object, x, y),
			selectedObject = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
		
		if (selectedObject) {
			selectedObject.health -= object.attackDamage;
			
			(
				game.realms.gameWorld.objectTypes[
					selectedObject.type
				].update ||
				function() {}
			).apply(game.realms.gameWorld.objectTypes[
				selectedObject.type
			], [selectedObject, selectedPoint.x, selectedPoint.y]);
		}
	},
	
	direct: function(object, direction) {
		game.sounds.entities_walking.play();
		
		object.rotation = direction;
		object.step = [1, 0][object.step];
	},
	
	selectedPointOf: function(object, x, y) {
		return {
			x: [
				x,
				x - 1,
				x,
				x + 1
			][object.rotation],
			y: [
				y + 1,
				y,
				y - 1,
				y
			][object.rotation]
		};
	},
	
	prototype: {
		type: "",
		
		inventory: [],
		
		attackDamage: 0,
		health: 0,
		
		rotation: 0,
		step: 0,
		
		canWalkAround: false,
		canWalk: false,
		canAttack: false
	},
	drop: []
};
Object.defineProperty(
	game.realms.gameWorld.objectTypes, "_mob",
	
	{
		enumerable: false
	}
);
