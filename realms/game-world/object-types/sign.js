game.realms.gameWorld.objectTypes.sign = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_sign,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.sign,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.sign.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "sign",
		
		text: [ 
			"Sample Text",
			"",
			"My name is Jeff"
		],
		
		inventory: [],
		
		health: 1
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.sign.New,
			
			chance: 1
		}
	]
};
