game.realms.gameWorld.objectTypes.console = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update_immortal,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_console,
			
			x0: 16 * +(
				game.realms.gameWorld.tick < 13
			),
			
			width0: 16,
			height0: 16,
			
			x: x,
			y: y ,
			
			width: 16,
			height: 16
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.console,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.console.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
		
		this.log += "\n" + this.prompt;
	},
	prototype: {
		type: "console",
		
		inventory: [],
		
		prompt: "$ ",
		log: "",
		scroll: 0,
		
		health: 0
	},
	drop: []
};
