game.realms.gameWorld.objectTypes.barrier = {
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_barrier,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.barrier.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "barrier",
		
		inventory: [],
		
		health: 0
	},
	drop: []
};
Object.defineProperty(
	game.realms.gameWorld.objectTypes, "barrier",
	
	{
		enumerable: false
	}
);

game.realms.gameWorld.objectTypesYouMustNotTouch.barrier = null;
