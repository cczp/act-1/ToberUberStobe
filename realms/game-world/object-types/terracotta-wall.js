for (let i of game.realms.gameWorld.terracottaColors) {
	game.realms.gameWorld.objectTypes["terracottaWall_" + i] = {
		update: game.realms.gameWorld.objectTypes._genericGarbage.update,
		
		render: function(object, x, y) {
			return {
				type: "sprite",
				
				texture: game.textures.blocks_terracottaWall,
				
				x0: game.realms.gameWorld.terracottaColors.indexOf(i) * 16,
				y0: 32 * +(
					object.health <= this.prototype.health / 2
				),
				
				width0: 16,
				height0: 32,
				
				x: x,
				y: y - 16,
				
				width: 16,
				height: 32
			};
		},
		
		New: function GameWorldObject(additions) {
			Object.assign(this,
				JSON.parse(JSON.stringify(
					game.realms.gameWorld.objectTypes["terracottaWall_" + i].prototype
				)),
				
				JSON.parse(JSON.stringify(additions || {}))
			);
		},
		prototype: {
			type: "terracottaWall_" + i,
			
			inventory: [],
			
			health: 222
		},
		drop: [
			{
				New: game.realms.gameWorld.itemTypes["terracotta_" + i].New,
				
				chance: 1
			},
			{
				New: game.realms.gameWorld.itemTypes["terracotta_" + i].New,
				
				chance: 1
			}
		]
	};
}
