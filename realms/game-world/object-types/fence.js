game.realms.gameWorld.objectTypes.fence = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_fence,
			
			width0: 16,
			height0: 16 + +(
				object.health <= this.prototype.health / 2
			),
			
			x: x,
			y: y,
			
			width: 16,
			height: 16 + +(
				object.health <= this.prototype.health / 2
			)
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.fence.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "fence",
		
		inventory: [],
		
		health: 33
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.fence.New,
			
			chance: 1
		}
	]
};
