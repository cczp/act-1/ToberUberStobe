game.realms.gameWorld.objectTypes.ironOre = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_ironOre,
			
			y0: 16 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 16,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.ironOre.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "ironOre",
		
		inventory: [],
		
		health: 1337
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.ironOre.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.ironOre.New,
			
			chance: 0.01
		},
		
		{
			New: game.realms.gameWorld.itemTypes.rock.New,
			
			chance: 0.75
		}
	]
};
