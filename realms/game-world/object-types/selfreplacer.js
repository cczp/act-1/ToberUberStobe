game.realms.gameWorld.objectTypes.selfreplacer = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update_immortal,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_selfreplacer,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.selfreplacer,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.selfreplacer.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "selfreplacer",
		
		inventory: [],
		
		health: 0
	},
	drop: []
};
