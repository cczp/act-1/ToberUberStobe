game.realms.gameWorld.objectTypes.woodenWall = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_woodenWall,
			
			width0: 8 * (2 - +(
				object.health <= this.prototype.health / 2
			)),
			height0: 32,
			
			x: x,
			y: y - 16,
			
			width: 8 * (2 - +(
				object.health <= this.prototype.health / 2
			)),
			height: 32
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.woodenWall.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "woodenWall",
		
		inventory: [],
		
		health: 80
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 1
		}
	]
};
