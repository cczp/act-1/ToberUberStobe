game.realms.gameWorld.objectTypes.smallCactus = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_smallCactus,
			
			y0: 16 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 16,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.smallCactus.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "smallCactus",
		
		inventory: [],
		
		health: 30
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.cactusPulp.New,
			
			chance: 1
		}
	]
};
