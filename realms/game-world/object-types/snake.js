game.realms.gameWorld.objectTypes.snake = {
	update: game.realms.gameWorld.objectTypes._mob.update,
	
	attack: game.realms.gameWorld.objectTypes._mob.attack,
	
	direct: game.realms.gameWorld.objectTypes._mob.direct,
	
	selectedPointOf: game.realms.gameWorld.objectTypes._mob.selectedPointOf,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.entities_snake,
			
			x0: object.rotation * 48,
			y0: 96 * +(
				object.health <= this.prototype.health / 2
			) + 48 * object.step,
			
			width0: 48,
			height0: 48,
			
			x: x - 16,
			y: y - 16,
			
			width: 48,
			height: 48
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.snake.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "snake",
		
		inventory: [],
		
		attackDamage: 0.3,
		health: 100,
		
		rotation: 0,
		step: 0,
		
		canWalkAround: false,
		canWalk: true,
		canAttack: true
	},
	drop: [
		{
			New: function() {
				game.realms.gameWorld.playerObject.experience += game.realms.gameWorld.objectTypes.snake.prototype.health;
				
				return new game.realms.gameWorld.itemTypes.snakeMeat.New();
			},
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.snakeMeat.New,
			
			chance: 0.01
		},
		
		{
			New: game.realms.gameWorld.itemTypes.bones.New,
			
			chance: 1
		}
	]
};
