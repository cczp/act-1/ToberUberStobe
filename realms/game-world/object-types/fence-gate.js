game.realms.gameWorld.objectTypes.fenceGate = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_fenceGate,
			
			width0: 16,
			height0: 16 + +(
				object.health <= this.prototype.health / 2
			),
			
			x: x,
			y: y,
			
			width: 16,
			height: 16 + +(
				object.health <= this.prototype.health / 2
			)
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.door,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.fenceGate.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "fenceGate",
		
		inventory: [],
		
		health: 37
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.fenceGate.New,
			
			chance: 1
		}
	]
};
