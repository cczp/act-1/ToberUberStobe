game.realms.gameWorld.objectTypes.fox = {
	update: game.realms.gameWorld.objectTypes._mob.update,
	
	attack: game.realms.gameWorld.objectTypes._mob.attack,
	
	direct: game.realms.gameWorld.objectTypes._mob.direct,
	
	selectedPointOf: game.realms.gameWorld.objectTypes._mob.selectedPointOf,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.entities_fox,
			
			x0: object.rotation * 32,
			y0: 32 * +(
				object.health <= this.prototype.health / 2
			) + 16 * object.step,
			
			width0: 32,
			height0: 16,
			
			x: x - 8,
			y: y,
			
			width: 32,
			height: 16
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.fox.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "fox",
		
		inventory: [],
		
		attackDamage: 0.25,
		health: 90,
		
		rotation: 0,
		step: 0,
		
		canWalkAround: false,
		canWalk: true,
		canAttack: true
	},
	drop: [
		{
			New: function() {
				game.realms.gameWorld.playerObject.experience += game.realms.gameWorld.objectTypes.fox.prototype.health;
				
				return new game.realms.gameWorld.itemTypes.foxMeat.New();
			},
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.foxMeat.New,
			
			chance: 0.01
		},
		
		{
			New: game.realms.gameWorld.itemTypes.bones.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.bones.New,
			
			chance: 0.5
		}
	]
};
