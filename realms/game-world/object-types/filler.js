game.realms.gameWorld.objectTypes.filler = {
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_filler,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.filler.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "filler",
		
		inventory: [],
		
		health: 0
	},
	drop: []
};
