game.realms.gameWorld.objectTypes.gazelle = {
	update: game.realms.gameWorld.objectTypes._mob.update_neutral,
	
	attack: game.realms.gameWorld.objectTypes._mob.attack,
	
	direct: game.realms.gameWorld.objectTypes._mob.direct,
	
	selectedPointOf: game.realms.gameWorld.objectTypes._mob.selectedPointOf,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.entities_gazelle,
			
			x0: object.rotation * 32,
			y0: 64 * +(
				object.health <= this.prototype.health / 2
			) + 32 * object.step,
			
			width0: 32,
			height0: 32,
			
			x: x - 8,
			y: y - 16,
			
			width: 32,
			height: 32
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.gazelle.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "gazelle",
		
		inventory: [],
		
		attackDamage: 0.4,
		health: 120,
		
		rotation: 0,
		step: 0,
		
		canWalkAround: true,
		canWalk: false,
		canAttack: false
	},
	drop: [
		{
			New: function() {
				game.realms.gameWorld.playerObject.experience += game.realms.gameWorld.objectTypes.gazelle.prototype.health;
					
				return new game.realms.gameWorld.itemTypes.gazelleMeat.New();
			},
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.gazelleMeat.New,
			
			chance: 0.1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.bones.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.bones.New,
			
			chance: 1
		}
	]
};
