game.realms.gameWorld.objectTypes.brickWall = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_brickWall,
			
			y0: 32 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 32,
			
			x: x,
			y: y - 16,
			
			width: 16,
			height: 32
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.brickWall.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "brickWall",
		
		inventory: [],
		
		health: 90
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.brick.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.brick.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.brick.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.brick.New,
			
			chance: 1
		}
	]
};
