game.realms.gameWorld.objectTypes.haloxylon = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_haloxylon,
			
			y0: 32 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 32,
			height0: 32,
			
			x: x - 13,
			y: y - 16,
			
			width: 32,
			height: 32
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.haloxylon.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "haloxylon",
		
		inventory: [],
		
		health: 150
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.sticks.New,
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.leaves.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.leaves.New,
			
			chance: 0.75
		}
	]
};
