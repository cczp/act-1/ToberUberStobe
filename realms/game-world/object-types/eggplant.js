game.realms.gameWorld.objectTypes.eggplant = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_eggplant,
			
			y0: 24 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 24,
			
			x: x,
			y: y - 8,
			
			width: 16,
			height: 24
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.eggplant.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "eggplant",
		
		inventory: [],
		
		health: 8
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.eggplant.New,
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.eggplant.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.eggplant.New,
			
			chance: 0.01
		}
	]
};
