for (let i of game.realms.gameWorld.terracottaColors) {
	game.realms.gameWorld.objectTypes["terracotta_" + i] = {
		update: game.realms.gameWorld.objectTypes._genericGarbage.update,
		
		render: function(object, x, y) {
			return {
				type: "sprite",
				
				texture: game.textures.blocks_terracotta,
				
				x0: game.realms.gameWorld.terracottaColors.indexOf(i) * 16,
				y0: 16 * +(
					object.health <= this.prototype.health / 2
				),
				
				width0: 16,
				height0: 16,
				
				x: x,
				y: y,
				
				width: 16,
				height: 16
			};
		},
		
		New: function GameWorldObject(additions) {
			Object.assign(this,
				JSON.parse(JSON.stringify(
					game.realms.gameWorld.objectTypes["terracotta_" + i].prototype
				)),
				
				JSON.parse(JSON.stringify(additions || {}))
			);
		},
		prototype: {
			type: "terracotta_" + i,
			
			inventory: [],
			
			health: 222
		},
		drop: [
			{
				New: game.realms.gameWorld.itemTypes["terracotta_" + i].New,
				
				chance: 1
			}
		]
	};
}
