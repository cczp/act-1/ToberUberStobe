game.realms.gameWorld.objectTypes.puddle = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_puddle,
			
			x: x,
			y: y,
			
			width: 16,
			height: 17
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.puddle.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "puddle",
		
		inventory: [],
		
		health: 1
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.clay.New,
			
			chance: 0.5
		}
	]
};
