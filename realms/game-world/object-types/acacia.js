game.realms.gameWorld.objectTypes.acacia = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_acacia,
			
			y0: 64 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 64,
			height0: 64,
			
			x: x - 24,
			y: y - 48,
			
			width: 64,
			height: 64
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.acacia.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "acacia",
		
		inventory: [],
		
		health: 300
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 1
		},
		
		{
			New: game.realms.gameWorld.itemTypes.log.New,
			
			chance: 0.75
		},
		
		{
			New: game.realms.gameWorld.itemTypes.sticks.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.sticks.New,
			
			chance: 0.75
		},
		
		{
			New: game.realms.gameWorld.itemTypes.leaves.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.leaves.New,
			
			chance: 0.75
		}
	]
};
