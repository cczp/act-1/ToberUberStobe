game.realms.gameWorld.objectTypes.chest = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	catchItems: game.realms.gameWorld.objectTypes._genericGarbage.catchItems,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_chest,
			
			width0: 16,
			height0: 16 + +(
				object.health <= this.prototype.health / 2
			),
			
			x: x,
			y: y,
			
			width: 16,
			height: 16 + +(
				object.health <= this.prototype.health / 2
			)
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.chest,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.chest.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "chest",
		
		inventory: new Array(48),
		
		health: 50
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.chest.New,
			
			chance: 1
		}
	]
};
