game.realms.gameWorld.objectTypes.player = {
	update: function(object, x, y) {
		let objectUpdated = false;
		
		if (game.realms.gameWorld.updatedAreas.includes(object)) {
			objectUpdated = true;
		} else {
			game.realms.gameWorld.updatedAreas.push(object);
		}
		
		//Resist KopatelCheat2011
			if (game.realms.gameWorld.currentGuiType != game.realms.gameWorld.guiTypes.default) {
				object.isKillingMachine = false;
				object.isRunning = false;
			}
		
		//Walk
			if ([
				game.realms.gameWorld.tick,
				0
			][+object.isRunning] % 5 == 0 && !objectUpdated) {
				switch (object.walkingDirection) {
					case 0:
						if (!game.realms.gameWorld.objects[[x, y + 1]]) {
							[
								game.realms.gameWorld.objects[[x, y]],
								game.realms.gameWorld.objects[[x, y + 1]]
							] = [
								game.realms.gameWorld.objects[[x, y + 1]],
								game.realms.gameWorld.objects[[x, y]]
							];
							
							y++;
							
							game.config.list.gameWorld.cameraPosition.y++;
							game.config.list.gameWorld.playerPosition.y++;
						}
						
						this.direct(object, 0);
						
						break;
					case 1:
						if (!game.realms.gameWorld.objects[[x - 1, y]]) {
							[
								game.realms.gameWorld.objects[[x, y]],
								game.realms.gameWorld.objects[[x - 1, y]]
							] = [
								game.realms.gameWorld.objects[[x - 1, y]],
								game.realms.gameWorld.objects[[x, y]]
							];
							
							x--;
							
							game.config.list.gameWorld.cameraPosition.x--;
							game.config.list.gameWorld.playerPosition.x--;
						}
						
						this.direct(object, 1);
						
						break;
					case 2:
						if (!game.realms.gameWorld.objects[[x, y - 1]]) {
							[
								game.realms.gameWorld.objects[[x, y]],
								game.realms.gameWorld.objects[[x, y - 1]]
							] = [
								game.realms.gameWorld.objects[[x, y - 1]],
								game.realms.gameWorld.objects[[x, y]]
							];
							
							y--;
							
							game.config.list.gameWorld.cameraPosition.y--;
							game.config.list.gameWorld.playerPosition.y--;
						}
						
						this.direct(object, 2);
						
						break;
					case 3:
						if (!game.realms.gameWorld.objects[[x + 1, y]]) {
							[
								game.realms.gameWorld.objects[[x, y]],
								game.realms.gameWorld.objects[[x + 1, y]]
							] = [
								game.realms.gameWorld.objects[[x + 1, y]],
								game.realms.gameWorld.objects[[x, y]]
							];
							
							x++;
							
							game.config.list.gameWorld.cameraPosition.x++;
							game.config.list.gameWorld.playerPosition.x++;
						}
						
						this.direct(object, 3);
						
						break;
				}
				
				if (!object.isRunning) {
					object.walkingDirection = -1;
				}
			}
		
		//Use item
			if (
				object.isUsingItem &&
				
				game.realms.gameWorld.tick % 5 == 0 &&
				
				!objectUpdated
			) {
				this.useItem(object, x, y);
				
				if (!object.isKillingMachine) {
					object.isUsingItem = false;
				}
			}
		
		if (!object.isGod) {
			if (!objectUpdated) {
				object.hunger -=
					this.indicatorsData.reduction.onStay.hunger * (1 -
					this.indicatorsData.reduction.onDarknessPercent.hunger
					* (1 - game.realms.gameWorld.lightingLevel));
				object.thirst -=
					this.indicatorsData.reduction.onStay.thirst * (1 -
					this.indicatorsData.reduction.onDarknessPercent.thirst
					* (1 - game.realms.gameWorld.lightingLevel));
				
				if (object.health < this.prototype.health) {
					object.health +=
						this.indicatorsData.healthRegeneration * ((
							object.hunger / this.prototype.hunger +
							object.thirst / this.prototype.thirst
						) / 2);
				}
			}
			
			if (
				object.health <= 0 ||
				object.health >= this.indicatorsData.maxValues.health ||
				object.hunger <= 0 ||
				object.hunger >= this.indicatorsData.maxValues.hunger ||
				object.thirst <= 0 ||
				object.thirst >= this.indicatorsData.maxValues.thirst
			) {
				game.sounds.entities_death.play()
				
				game.realms.gameWorld.fail(object, x, y);
				
				return;
			}
		} else {
			object.health = this.prototype.health;
			object.hunger = this.prototype.hunger;
			object.thirst = this.prototype.thirst;
		}
	},
	
	useItem: function(object, x, y) {
		if (!object.isGod) {
			object.hunger -=
				this.indicatorsData.reduction.onItemUsing.hunger * (1 -
				this.indicatorsData.reduction.onDarknessPercent.hunger
				* (1 - game.realms.gameWorld.lightingLevel));
			object.thirst -=
				this.indicatorsData.reduction.onItemUsing.thirst * (1 -
				this.indicatorsData.reduction.onDarknessPercent.thirst
				* (1 - game.realms.gameWorld.lightingLevel));
		}
		
		if (!game.realms.gameWorld.particles[[x, y - 1]]) {
			game.realms.gameWorld.particles[[x, y - 1]] = new game.realms.gameWorld.particleTypes.attack.New();
		}
		
		if (object.inventory[0]) {
			const itemType = game.realms.gameWorld.itemTypes[
				object.inventory[0].type
			];
			
			if (itemType.use) {
				itemType.use(object, 0, x, y);
				
				return;
			}
		}
		
		game.sounds.entities_attack.play();
		
		const
			selectedPoint = this.selectedPointOf(object, x, y),
			selectedObject = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
		
		if (selectedObject) {
			selectedObject.health -= [
				object.attackDamage,
				Infinity
			][+object.isGod];
			
			(
				game.realms.gameWorld.objectTypes[
					selectedObject.type
				].update ||
				function() {}
			).apply(game.realms.gameWorld.objectTypes[
				selectedObject.type
			], [selectedObject, selectedPoint.x, selectedPoint.y]);
			
			const selectedObject1 = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
			
			if (!selectedObject1) {
				this.catchItems(object, selectedObject.inventory.concat(
					game.realms.gameWorld.objectTypes[
						selectedObject.type
					].drop.map(function(currentItem) {
						if (
							Math.random() < currentItem.chance &&
							
							!object.isGod
						) {
							return new currentItem.New();
						}
					})
				));
			}
		}
	},
	catchItems: function(object, items) {
		game.sounds.entities_itemCatching.play();
		
		for (let i = 0; i < items.length; i++) {
			for (let j = 1; j < object.inventory.length; j++) {
				if (!object.inventory[j]) {
					object.inventory[j] = items[i];
					
					break;
				}
			}
		}
	},
	
	direct: function(object, direction) {
		game.sounds.entities_walking.play();
		
		if (!object.isGod) {
			object.hunger -=
				this.indicatorsData.reduction.onWalking.hunger *
				(1 + 3 * +object.isRunning) * (1 -
				this.indicatorsData.reduction.onDarknessPercent.hunger
				* (1 - game.realms.gameWorld.lightingLevel));
			object.thirst -=
				this.indicatorsData.reduction.onWalking.thirst *
				(1 + 3 * +object.isRunning) * (1 -
				this.indicatorsData.reduction.onDarknessPercent.thirst
				* (1 - game.realms.gameWorld.lightingLevel));
		}
		
		object.rotation = direction;
		object.step = [1, 0][object.step];
		object.distanceWalked++;
	},
	
	selectedPointOf: function(object, x, y) {
		return {
			x: [
				x,
				x - 1,
				x,
				x + 1
			][object.rotation],
			y: [
				y + 1,
				y,
				y - 1,
				y
			][object.rotation]
		};
	},
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.entities_player,
			
			x0: object.rotation * 16,
			y0: 64 * +(
				object.health <= this.prototype.health / 2
			) + 32 * object.step,
			
			width0: 16,
			height0: 32,
			
			x: x,
			y: y - 16,
			
			width: 16,
			height: 32
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.player.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "player",
		
		inventory: new Array(47).concat([
			new game.realms.gameWorld.itemTypes.waterBottle.New({
				fullness: 0
			})
		]),
		
		isGod: false,
		
		attackDamage: 1,
		health: 100,
		hunger: 100,
		thirst: 100,
		experience: 0,
		
		rotation: 0,
		step: 0,
		
		isKillingMachine: false,
		isUsingItem: false,
		isRunning: false,
		walkingDirection: -1,
		
		distanceWalked: 0
	},
	drop: [],
	indicatorsData: {
		healthRegeneration: 0.01,
		
		reduction: {
			onDarknessPercent: {
				hunger: -0.25,
				thirst: 0.25
			},
			
			onStay: {
				hunger: 0.004165,
				thirst: 0.00833
			},
			onItemUsing: {
				hunger: 0.01666,
				thirst: 0.03332
			},
			onWalking: {
				hunger: 0.00833,
				thirst: 0.01666
			}
		},
		
		maxValues: {
			health: 200,
			hunger: 200,
			thirst: 200
		}
	}
};
Object.defineProperty(
	game.realms.gameWorld.objectTypes, "player",
	
	{
		enumerable: false
	}
);

game.realms.gameWorld.objectTypesYouMustNotTouch.player = null;
