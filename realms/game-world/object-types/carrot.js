game.realms.gameWorld.objectTypes.carrot = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_carrot,
			
			y0: 17 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 17,
			
			x: x,
			y: y,
			
			width: 16,
			height: 17
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.carrot.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "carrot",
		
		inventory: [],
		
		health: 12
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.carrot.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.carrot.New,
			
			chance: 1
		},
		{
			New: game.realms.gameWorld.itemTypes.carrot.New,
			
			chance: 0.01
		}
	]
};
