game.realms.gameWorld.objectTypes._genericGarbage = {
	update: function(object, x, y) {
		if (object.health <= 0) {
			game.sounds.blocks_death.play();
			
			game.realms.gameWorld.objects[[x, y]] = null;
		}
	},
	update_immortal: function(object, x, y) {
		if (!isFinite(object.health)) {
			game.realms.gameWorld.objects[[x, y]] = null;
		}
	},
	
	catchItems: function(object, items) {
		game.sounds.entities_itemCatching.play();
		
		for (let i = 0; i < items.length; i++) {
			for (let j = 0; j < object.inventory.length; j++) {
				if (!object.inventory[j]) {
					object.inventory[j] = items[i];
					
					break;
				}
			}
		}
	},
	
	prototype: {
		type: "",
		
		inventory: [],
		
		health: 0
	},
	drop: []
};
Object.defineProperty(
	game.realms.gameWorld.objectTypes, "_genericGarbage",
	
	{
		enumerable: false
	}
);
