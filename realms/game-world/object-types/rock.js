game.realms.gameWorld.objectTypes.rock = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_rock,
			
			y0: 17 * +(
				object.health <= this.prototype.health / 2
			),
			
			width0: 16,
			height0: 17,
			
			x: x,
			y: y,
			
			width: 16,
			height: 17
		};
	},
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.rock.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "rock",
		
		inventory: [],
		
		health: 2
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.rock.New,
			
			chance: 1
		}
	]
};
