game.realms.gameWorld.objectTypes.door = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_door,
			
			width0: 16,
			height0: 32 + +(
				object.health <= this.prototype.health / 2
			),
			
			x: x,
			y: y - 16,
			
			width: 16,
			height: 32 + +(
				object.health <= this.prototype.health / 2
			)
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.door,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.door.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "door",
		
		inventory: [],
		
		health: 75
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.door.New,
			
			chance: 1
		}
	]
};
