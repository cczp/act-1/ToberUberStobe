game.realms.gameWorld.objectTypes.oven = {
	update: game.realms.gameWorld.objectTypes._genericGarbage.update,
	
	catchItems: game.realms.gameWorld.objectTypes._genericGarbage.catchItems,
	
	render: function(object, x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.blocks_oven,
			
			width0: 16,
			height0: 16 + +(
				object.health <= this.prototype.health / 2
			),
			
			x: x,
			y: y,
			
			width: 16,
			height: 16 + +(
				object.health <= this.prototype.health / 2
			)
		};
	},
	
	gui: game.realms.gameWorld.guiTypes.oven,
	
	New: function GameWorldObject(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.objectTypes.oven.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "oven",
		
		inventory: new Array(3),
		
		health: 65
	},
	drop: [
		{
			New: game.realms.gameWorld.itemTypes.oven.New,
			
			chance: 1
		}
	]
};
