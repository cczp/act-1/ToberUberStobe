game.realms.gameWorld.itemTypes.zelyonkaBottle = {
	use: function(object, slot, x, y) {
		const
			selectedPoint = game.realms.gameWorld.objectTypes[
				object.type
			].selectedPointOf(object, x, y),
			selectedObject = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
		
		if (selectedObject) {
			if (selectedObject.canWalk) {
				game.sounds.items_splashing.play();
				
				if (
					Math.random() <
						this.chanceData.using
				) {
					selectedObject.canWalk = false;
				}
				
				if (!object.isGod) {
					object.inventory[slot] = new game.realms.gameWorld.itemTypes.bottle.New();
				}
			}
		}
	},
	
	textureOf: function(item) {
		return game.textures.items_zelyonkaBottle;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.zelyonkaBottle.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "zelyonkaBottle"
	},
	chanceData: {
		using: 0.5
	}
};

game.realms.gameWorld.craftingRecipes.zelyonkaBottle = {
	in: [
		"bottle",
		"dye_teal",
		"dye_teal"
	],
	
	Out: game.realms.gameWorld.itemTypes.zelyonkaBottle.New
};
