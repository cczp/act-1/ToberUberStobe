game.realms.gameWorld.itemTypes.glass = {
	textureOf: function() {
		return game.textures.items_glass;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.glass.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "glass"
	}
};

game.realms.gameWorld.cookingRecipes.glass = {
	in: "sand",
	
	Out: game.realms.gameWorld.itemTypes.glass.New
};
