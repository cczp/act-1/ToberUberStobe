game.realms.gameWorld.itemTypes.ironShovel = {
	use: game.realms.gameWorld.itemTypes._weapon.use,
	
	textureOf: function(item) {
		if (item.durability > this.prototype.durability / 2) {
			return game.textures.items_ironShovel;
		} else {
			return game.textures.items_damagedIronShovel;
		}
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.ironShovel.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "ironShovel",
		
		attackDamage: 20,
		
		durability: 200
	}
};

game.realms.gameWorld.craftingRecipes.ironShovel = {
	in: [
		"sticks",
		"ironIngot",
		"ironIngot",
		"ironIngot",
		"ironIngot"
	],
	
	Out: game.realms.gameWorld.itemTypes.ironShovel.New
};
