game.realms.gameWorld.itemTypes._covering = {
	use: function(object, slot, x, y) {
		const selectedPoint = game.realms.gameWorld.objectTypes[
			object.type
		].selectedPointOf(object, x, y);
		
		game.sounds.items_placement.play();
		
		if (!object.isGod) {
			object.inventory[slot] = null;
		}
		
		game.realms.gameWorld.background[[
			selectedPoint.x,
			selectedPoint.y
		]] = this.placeableBackgroundTextures[
			Math.floor(Math.random() * this.placeableBackgroundTextures.length)
		];
	},
	
	prototype: {
		type: ""
	},
	placeableBackgroundTextures: [
		""
	]
};
Object.defineProperty(
	game.realms.gameWorld.itemTypes, "_covering",
	
	{
		enumerable: false
	}
);
