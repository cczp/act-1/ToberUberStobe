for (let i of [
	"red",
	"pink",
	"purple",
	"deepPurple",
	"indigo",
	"blue",
	"lightBlue",
	"cyan",
	"teal",
	"green",
	"lightGreen",
	"lime",
	"yellow",
	"amber",
	"orange",
	"deepOrange",
	"brown",
	"gray",
	"blueGray",
	"darkGray",
	"black",
	"white"
]) {
	game.realms.gameWorld.itemTypes["floorTile_" + i] = {
		use: game.realms.gameWorld.itemTypes._covering.use,
		
		textureOf: function() {
			return game.textures["items_floorTile_" + i];
		},
		
		New: function GameWorldItem(additions) {
			Object.assign(this,
				JSON.parse(JSON.stringify(
					game.realms.gameWorld.itemTypes["floorTile_" + i].prototype
				)),
				
				JSON.parse(JSON.stringify(additions || {}))
			);
		},
		prototype: {
			type: "floorTile_" + i
		},
		placeableBackgroundTextures: [
			"terracotta_" + i
		]
	};
	
	game.realms.gameWorld.craftingRecipes["floorTile_" + i] = {
		in: [
			"terracotta_" + i
		],
		
		Out: function() {
			game.realms.gameWorld.objectTypes[
				game.realms.gameWorld.playerObject.type
			].catchItems(game.realms.gameWorld.playerObject, [
				new game.realms.gameWorld.itemTypes["floorTile_" + i].New(),
				new game.realms.gameWorld.itemTypes["floorTile_" + i].New(),
				new game.realms.gameWorld.itemTypes["floorTile_" + i].New()
			]);
			
			return new game.realms.gameWorld.itemTypes["floorTile_" + i].New();
		}
	};
}
