game.realms.gameWorld.itemTypes.stoneCovering = {
	use: game.realms.gameWorld.itemTypes._covering.use,
	
	textureOf: function() {
		return game.textures.items_stoneCovering;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.stoneCovering.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stoneCovering"
	},
	placeableBackgroundTextures: [
		"stone",
		"stone_1",
		"stone_2",
		"stone_3"
	]
};

game.realms.gameWorld.craftingRecipes.stoneCovering = {
	in: [
		"rock"
	],
	
	Out: function() {
		game.realms.gameWorld.objectTypes[
			game.realms.gameWorld.playerObject.type
		].catchItems(game.realms.gameWorld.playerObject, [
			new game.realms.gameWorld.itemTypes.stoneCovering.New()
		]);
		
		return new game.realms.gameWorld.itemTypes.stoneCovering.New();
	}
};
