game.realms.gameWorld.itemTypes.oven = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_oven;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.oven.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "oven"
	}
};

game.realms.gameWorld.craftingRecipes.oven = {
	in: [
		"brick",
		"brick",
		"brick"
	],
	
	Out: game.realms.gameWorld.itemTypes.oven.New
};
