game.realms.gameWorld.itemTypes.coal = {
	textureOf: function() {
		return game.textures.items_coal;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.coal.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "coal"
	}
};

game.realms.gameWorld.cookingRecipes._fuelTypes.coal = null;
