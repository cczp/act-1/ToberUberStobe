game.realms.gameWorld.itemTypes.chest = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_chest;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.chest.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "chest"
	}
};

game.realms.gameWorld.craftingRecipes.chest = {
	in: [
		"log",
		"log",
		"log"
	],
	
	Out: game.realms.gameWorld.itemTypes.chest.New
};
