game.realms.gameWorld.itemTypes.fence = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_fence;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.fence.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "fence"
	}
};

game.realms.gameWorld.craftingRecipes.fence = {
	in: [
		"sticks",
		"sticks",
		"sticks",
		"sticks"
	],
	
	Out: game.realms.gameWorld.itemTypes.fence.New
};
