game.realms.gameWorld.itemTypes.dirt = {
	textureOf: function() {
		return game.textures.items_dirt;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.dirt.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "dirt"
	}
};

game.realms.gameWorld.craftingRecipes.dirt = {
	in: [
		"dirtCovering",
		"dirtCovering"
	],
	
	Out: game.realms.gameWorld.itemTypes.dirt.New
};
