for (let i of [
	"amber",
	"black",
	"blue",
	"blueGray",
	"brown",
	"cyan",
	"darkGray",
	"deepPurple",
	"gray",
	"green",
	"indigo",
	"lightBlue",
	"lightGreen",
	"lime",
	"orange",
	"pink",
	"purple",
	"red",
	"teal",
	"white",
	"yellow"
]) {
	game.realms.gameWorld.itemTypes["terracotta_" + i] = {
		use: game.realms.gameWorld.itemTypes._placeable.use,
		
		textureOf: function() {
			return game.textures["items_terracotta_" + i];
		},
		
		New: function GameWorldItem(additions) {
			Object.assign(this,
				JSON.parse(JSON.stringify(
					game.realms.gameWorld.itemTypes["terracotta_" + i].prototype
				)),
				
				JSON.parse(JSON.stringify(additions || {}))
			);
		},
		prototype: {
			type: "terracotta_" + i
		}
	};
	
	game.realms.gameWorld.craftingRecipes["terracotta_" + i] = {
		in: [
			"terracotta_deepOrange",
			"dye_" + i
		],
		
		Out: game.realms.gameWorld.itemTypes["terracotta_" + i].New
	};
}

game.realms.gameWorld.itemTypes.terracotta_deepOrange = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_terracotta_deepOrange;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.terracotta_deepOrange.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "terracotta_deepOrange"
	}
};

game.realms.gameWorld.cookingRecipes.terracotta_deepOrange = {
	in: "clay",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_1 = {
	in: "terracotta_red",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_2 = {
	in: "terracotta_pink",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_3 = {
	in: "terracotta_purple",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_4 = {
	in: "terracotta_deepPurple",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_5 = {
	in: "terracotta_indigo",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_6 = {
	in: "terracotta_blue",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_7 = {
	in: "terracotta_lightBlue",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_8 = {
	in: "terracotta_cyan",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_9 = {
	in: "terracotta_teal",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_10 = {
	in: "terracotta_green",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_11 = {
	in: "terracotta_lightGreen",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_12 = {
	in: "terracotta_lime",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_13 = {
	in: "terracotta_yellow",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_14 = {
	in: "terracotta_amber",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_15 = {
	in: "terracotta_orange",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_16 = {
	in: "terracotta_brown",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_17 = {
	in: "terracotta_gray",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_18 = {
	in: "terracotta_blueGray",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_19 = {
	in: "terracotta_darkGray",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_20 = {
	in: "terracotta_black",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
game.realms.gameWorld.cookingRecipes.terracotta_deepOrange_21 = {
	in: "terracotta_white",
	
	Out: game.realms.gameWorld.itemTypes.terracotta_deepOrange.New
};
