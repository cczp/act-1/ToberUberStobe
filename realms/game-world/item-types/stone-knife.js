game.realms.gameWorld.itemTypes.stoneKnife = {
	use: game.realms.gameWorld.itemTypes._weapon.use,
	
	textureOf: function(item) {
		if (item.durability > this.prototype.durability / 2) {
			return game.textures.items_stoneKnife;
		} else {
			return game.textures.items_damagedStoneKnife;
		}
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.stoneKnife.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stoneKnife",
		
		attackDamage: 2.5,
		
		durability: 25
	}
};
