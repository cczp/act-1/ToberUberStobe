game.realms.gameWorld.itemTypes.clay = {
	textureOf: function() {
		return game.textures.items_clay;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.clay.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "clay"
	}
};
