for (let i of game.realms.gameWorld.terracottaColors) {
	game.realms.gameWorld.itemTypes["terracottaWall_" + i] = {
		use: game.realms.gameWorld.itemTypes._placeable.use,
		
		textureOf: function() {
			return game.textures["items_terracottaWall_" + i];
		},
		
		New: function GameWorldItem(additions) {
			Object.assign(this,
				JSON.parse(JSON.stringify(
					game.realms.gameWorld.itemTypes["terracottaWall_" + i].prototype
				)),
				
				JSON.parse(JSON.stringify(additions || {}))
			);
		},
		prototype: {
			type: "terracottaWall_" + i
		}
	};
	
	game.realms.gameWorld.craftingRecipes["terracottaWall_" + i] = {
		in: [
			"terracotta_" + i,
			"terracotta_" + i
		],
		
		Out: game.realms.gameWorld.itemTypes["terracottaWall_" + i].New
	};
}
