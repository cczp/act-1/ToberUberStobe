game.realms.gameWorld.itemTypes.bones = {
	textureOf: function() {
		return game.textures.items_bones;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.bones.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "bones"
	}
};
