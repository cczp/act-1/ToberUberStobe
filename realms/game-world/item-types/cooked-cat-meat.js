game.realms.gameWorld.itemTypes.cookedCatMeat = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_cookedCatMeat;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.cookedCatMeat.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "cookedCatMeat"
	},
	saturationData: {
		hunger: 50,
		thirst: 10
	}
};

game.realms.gameWorld.cookingRecipes.cookedCatMeat = {
	in: "catMeat",
	
	Out: game.realms.gameWorld.itemTypes.cookedCatMeat.New
};
