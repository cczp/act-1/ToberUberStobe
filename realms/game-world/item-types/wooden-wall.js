game.realms.gameWorld.itemTypes.woodenWall = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_woodenWall;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.woodenWall.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "woodenWall"
	}
};

game.realms.gameWorld.craftingRecipes.woodenWall = {
	in: [
		"log",
		"log",
		"log",
		"log"
	],
	
	Out: game.realms.gameWorld.itemTypes.woodenWall.New
};
