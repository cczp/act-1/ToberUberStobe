game.realms.gameWorld.itemTypes.bottle = {
	use: function(object, slot, x, y) {
		const
			selectedPoint = game.realms.gameWorld.objectTypes[
				object.type
			].selectedPointOf(object, x, y),
			selectedObject = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
		
		if (selectedObject) {
			if (selectedObject.type in this.drinkableObjects) {
				game.sounds.items_drinking.play();
				
				selectedObject.health--;
				
				if (!object.isGod) {
					object.inventory[slot] = new game.realms.gameWorld.itemTypes.waterBottle.New();
				}
				
				(
					game.realms.gameWorld.objectTypes[
						selectedObject.type
					].update ||
					function() {}
				).apply(game.realms.gameWorld.objectTypes[
					selectedObject.type
				], [selectedObject, selectedPoint.x, selectedPoint.y]);
				
				selectedObject1 = game.realms.gameWorld.objects[[
					selectedPoint.x,
					selectedPoint.y
				]];
				
				if (!selectedObject1) {
					game.realms.gameWorld.objectTypes[
						object.type
					].catchItems(object, selectedObject.inventory.concat(
						game.realms.gameWorld.objectTypes[
							selectedObject.type
						].drop.map(function(currentItem) {
							if (Math.random() < currentItem.chance) {
								return new currentItem.New()
							}
						})
					));
				}
			}
		}
	},
	
	textureOf: function() {
		return game.textures.items_bottle;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.bottle.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "bottle"
	},
	drinkableObjects: {
		puddle: null
	}
};

game.realms.gameWorld.craftingRecipes.bottle = {
	in: [
		"glass",
		"sticks"
	],
	
	Out: game.realms.gameWorld.itemTypes.bottle.New
};
