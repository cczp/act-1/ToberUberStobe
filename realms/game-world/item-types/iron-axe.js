game.realms.gameWorld.itemTypes.ironAxe = {
	use: game.realms.gameWorld.itemTypes._weapon.use,
	
	textureOf: function(item) {
		if (item.durability > this.prototype.durability / 2) {
			return game.textures.items_ironAxe;
		} else {
			return game.textures.items_damagedIronAxe;
		}
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.ironAxe.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "ironAxe",
		
		attackDamage: 15,
		
		durability: 150
	}
};

game.realms.gameWorld.craftingRecipes.ironAxe = {
	in: [
		"sticks",
		"ironIngot",
		"ironIngot"
	],
	
	Out: game.realms.gameWorld.itemTypes.ironAxe.New
};
