game.realms.gameWorld.itemTypes.fenceGate = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_fenceGate;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.fenceGate.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "fenceGate"
	}
};

game.realms.gameWorld.craftingRecipes.fenceGate = {
	in: [
		"sticks",
		"sticks",
		"sticks",
		"sticks",
		"sticks",
		"sticks"
	],
	
	Out: game.realms.gameWorld.itemTypes.fenceGate.New
};
