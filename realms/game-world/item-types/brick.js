game.realms.gameWorld.itemTypes.brick = {
	textureOf: function() {
		return game.textures.items_brick;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.brick.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "brick"
	}
};

game.realms.gameWorld.craftingRecipes.brick = {
	in: [
		"rock",
		"sand"
	],
	
	Out: game.realms.gameWorld.itemTypes.brick.New
};
