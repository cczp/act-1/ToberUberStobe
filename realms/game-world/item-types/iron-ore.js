game.realms.gameWorld.itemTypes.ironOre = {
	textureOf: function() {
		return game.textures.items_ironOre;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.ironOre.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "ironOre"
	}
};
