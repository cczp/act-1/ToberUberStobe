game.realms.gameWorld.itemTypes.snakeMeat = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_snakeMeat;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.snakeMeat.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "snakeMeat"
	},
	saturationData: {
		hunger: 10,
		thirst: 40
	}
};
