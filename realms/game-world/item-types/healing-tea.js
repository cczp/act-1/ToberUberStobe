game.realms.gameWorld.itemTypes.healingTea = {
	use: function(object, slot) {
		game.sounds.items_drinking.play();
		
		if (!object.isGod) {
			object.inventory[slot] = null;
		}
		
		object.health += this.saturationData.healthIncreasing;
		object.hunger += this.saturationData.hungerIncreasing;
		object.thirst += this.saturationData.thirstIncreasing;
	},
	
	textureOf: function() {
		return game.textures.items_healingTea;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.healingTea.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "healingTea"
	},
	saturationData: {
		healthIncreasing: 10,
		hungerIncreasing: 10,
		thirstIncreasing: 10
	}
};

game.realms.gameWorld.cookingRecipes.healingTea = {
	in: "rawHealingTea",
	
	Out: game.realms.gameWorld.itemTypes.healingTea.New
};
