game.realms.gameWorld.itemTypes.dirtCovering = {
	use: game.realms.gameWorld.itemTypes._covering.use,
	
	textureOf: function() {
		return game.textures.items_dirtCovering;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.dirtCovering.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "dirtCovering"
	},
	placeableBackgroundTextures: [
		"dirt",
		"dirt_1",
		"dirt_2",
		"dirt_3"
	]
};

game.realms.gameWorld.craftingRecipes.dirtCovering = {
	in: [
		"dirt"
	],
	
	Out: function() {
		game.realms.gameWorld.objectTypes[
			game.realms.gameWorld.playerObject.type
		].catchItems(game.realms.gameWorld.playerObject, [
			new game.realms.gameWorld.itemTypes.dirtCovering.New()
		]);
		
		return new game.realms.gameWorld.itemTypes.dirtCovering.New();
	}
};
