game.realms.gameWorld.itemTypes.brickWall = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_brickWall;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.brickWall.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "brickWall"
	}
};

game.realms.gameWorld.craftingRecipes.brickWall = {
	in: [
		"brick",
		"brick",
		"brick",
		"brick"
	],
	
	Out: game.realms.gameWorld.itemTypes.brickWall.New
};
