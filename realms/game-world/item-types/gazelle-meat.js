game.realms.gameWorld.itemTypes.gazelleMeat = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_gazelleMeat;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.gazelleMeat.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "gazelleMeat"
	},
	saturationData: {
		hunger: 25,
		thirst: 35
	}
};
