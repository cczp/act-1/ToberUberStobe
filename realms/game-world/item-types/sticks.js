game.realms.gameWorld.itemTypes.sticks = {
	textureOf: function() {
		return game.textures.items_sticks;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.sticks.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "sticks"
	}
};

game.realms.gameWorld.craftingRecipes.sticks = {
	in: [
		"log",
		"rock"
	],
	
	Out: function() {
		game.realms.gameWorld.objectTypes[
			game.realms.gameWorld.playerObject.type
		].catchItems(game.realms.gameWorld.playerObject, [
			new game.realms.gameWorld.itemTypes.rock.New()
		]);
		
		return new game.realms.gameWorld.itemTypes.sticks.New();
	}
};

game.realms.gameWorld.cookingRecipes._fuelTypes.sticks = null;
