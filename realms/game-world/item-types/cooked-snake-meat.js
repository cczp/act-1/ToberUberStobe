game.realms.gameWorld.itemTypes.cookedSnakeMeat = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_cookedSnakeMeat;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.cookedSnakeMeat.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "cookedSnakeMeat"
	},
	saturationData: {
		hunger: 30,
		thirst: 20
	}
};

game.realms.gameWorld.cookingRecipes.cookedSnakeMeat = {
	in: "snakeMeat",
	
	Out: game.realms.gameWorld.itemTypes.cookedSnakeMeat.New
};
