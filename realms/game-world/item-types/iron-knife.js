game.realms.gameWorld.itemTypes.ironKnife = {
	use: game.realms.gameWorld.itemTypes._weapon.use,
	
	textureOf: function(item) {
		if (item.durability > this.prototype.durability / 2) {
			return game.textures.items_ironKnife;
		} else {
			return game.textures.items_damagedIronKnife;
		}
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.ironKnife.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "ironKnife",
		
		attackDamage: 10,
		
		durability: 100
	}
};

game.realms.gameWorld.craftingRecipes.ironKnife = {
	in: [
		"ironIngot",
		"ironIngot"
	],
	
	Out: game.realms.gameWorld.itemTypes.ironKnife.New
};
