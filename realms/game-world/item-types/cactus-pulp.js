game.realms.gameWorld.itemTypes.cactusPulp = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_cactusPulp;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.cactusPulp.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "cactusPulp"
	},
	saturationData: {
		hunger: 20,
		thirst: 20
	}
};
