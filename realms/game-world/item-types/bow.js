game.realms.gameWorld.itemTypes.bow = {
	use: function(object, slot, x, y) {
		const selectedPoint = game.realms.gameWorld.objectTypes[
			object.type
		].selectedPointOf(object, x, y);
		
		let hasAmmo = false;
		
		game.sounds.entities_attack.play();
		
		if (!object.isGod) {
			for (let i = 0; i < object.inventory.length; i++) {
				if (object.inventory[i]) {
					if (object.inventory[i].type == "arrow") {
						object.inventory[i] = null;
						
						hasAmmo = true;
						
						if (Math.random() < this.chanceData.breaking) {
							object.inventory[slot] = new game.realms.gameWorld.itemTypes.damagedBow.New();
						} else if (Math.random() < this.chanceData.veryBreaking) {
							object.inventory[slot] = null;
						}
						
						break;
					}
				}
			}
		} else {
			hasAmmo = true;
		}
		
		if (hasAmmo) {
			const particle = game.realms.gameWorld.particles[[
				selectedPoint.x,
				selectedPoint.y
			]] = new game.realms.gameWorld.particleTypes.arrow.New();
			
			particle.rotation = object.rotation;
		}
	},
	
	textureOf: function(item) {
		return game.textures.items_bow;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.bow.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "bow"
	},
	chanceData: {
		breaking: 0.05,
		veryBreaking: 0.01666
	}
};

game.realms.gameWorld.craftingRecipes.bow = {
	in: [
		"sticks",
		"sticks",
		"grass",
		"grass"
	],
	
	Out: game.realms.gameWorld.itemTypes.bow.New
};
game.realms.gameWorld.craftingRecipes.bow1 = {
	in: [
		"damagedBow",
		"grass"
	],
	
	Out: game.realms.gameWorld.itemTypes.bow.New
};
