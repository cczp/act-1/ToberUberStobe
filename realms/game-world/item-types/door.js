game.realms.gameWorld.itemTypes.door = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_door;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.door.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "door"
	}
};

game.realms.gameWorld.craftingRecipes.door = {
	in: [
		"log",
		"log"
	],
	
	Out: game.realms.gameWorld.itemTypes.door.New
};
