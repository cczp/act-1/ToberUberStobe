game.realms.gameWorld.itemTypes.parquet = {
	use: game.realms.gameWorld.itemTypes._covering.use,
	
	textureOf: function() {
		return game.textures.items_parquet;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.parquet.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "parquet"
	},
	placeableBackgroundTextures: [
		"parquet",
		"parquet_1"
	]
};

game.realms.gameWorld.craftingRecipes.parquet = {
	in: [
		"log"
	],
	
	Out: function() {
		game.realms.gameWorld.objectTypes[
			game.realms.gameWorld.playerObject.type
		].catchItems(game.realms.gameWorld.playerObject, [
			new game.realms.gameWorld.itemTypes.parquet.New()
		]);
		
		return new game.realms.gameWorld.itemTypes.parquet.New();
	}
};
