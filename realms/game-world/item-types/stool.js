game.realms.gameWorld.itemTypes.stool = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_stool;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.stool.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stool"
	}
};

game.realms.gameWorld.craftingRecipes.stool = {
	in: [
		"parquet",
		"sticks",
		"sticks"
	],
	
	Out: game.realms.gameWorld.itemTypes.stool.New
};
