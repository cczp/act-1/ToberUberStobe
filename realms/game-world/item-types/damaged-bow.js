game.realms.gameWorld.itemTypes.damagedBow = {
	textureOf: function() {
		return game.textures.items_damagedBow;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.damagedBow.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "damagedBow"
	}
};
