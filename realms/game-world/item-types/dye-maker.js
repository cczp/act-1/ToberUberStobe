game.realms.gameWorld.itemTypes.dyeMaker = {
	use: function(object, slot) {
		game.sounds.gui_click.play();
		
		game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.dyeMaker;
		
		if (!object.isGod) {
			object.inventory[slot] = null;
		}
	},
	
	textureOf: function() {
		return game.textures.items_dyeMaker;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.dyeMaker.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "dyeMaker"
	}
};

game.realms.gameWorld.craftingRecipes.dyeMaker = {
	in: [
		"tomato",
		"cactusPulp",
		"eggplant"
	],
	
	Out: game.realms.gameWorld.itemTypes.dyeMaker.New
};
