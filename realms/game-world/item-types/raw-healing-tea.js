game.realms.gameWorld.itemTypes.rawHealingTea = {
	textureOf: function(item) {
		return game.textures.items_rawHealingTea;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.rawHealingTea.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "rawHealingTea"
	}
};

game.realms.gameWorld.craftingRecipes.rawHealingTea = {
	in: [
		"bottle",
		"dye_amber",
		"leaves",
		"leaves"
	],
	
	Out: game.realms.gameWorld.itemTypes.rawHealingTea.New
};
