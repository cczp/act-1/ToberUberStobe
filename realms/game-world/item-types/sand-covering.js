game.realms.gameWorld.itemTypes.sandCovering = {
	use: game.realms.gameWorld.itemTypes._covering.use,
	
	textureOf: function() {
		return game.textures.items_sandCovering;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.sandCovering.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "sandCovering"
	},
	placeableBackgroundTextures: [
		"sand",
		"sand_1",
		"sand_2",
		"sand_3",
		"sand_4",
		"sand_5",
		"sand_6",
		"sand_7"
	]
};

game.realms.gameWorld.craftingRecipes.sandCovering = {
	in: [
		"sand"
	],
	
	Out: function() {
		game.realms.gameWorld.objectTypes[
			game.realms.gameWorld.playerObject.type
		].catchItems(game.realms.gameWorld.playerObject, [
			new game.realms.gameWorld.itemTypes.sandCovering.New()
		]);
		
		return new game.realms.gameWorld.itemTypes.sandCovering.New();
	}
};
