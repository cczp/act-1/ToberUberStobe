game.realms.gameWorld.itemTypes.rawStoneShovel = {
	use: function(object, slot) {
		game.sounds.gui_click.play();
		
		if (Math.random() < this.chanceData.using) {
			object.inventory[slot] = new game.realms.gameWorld.itemTypes.stoneShovel.New();
		} else if (!object.isGod) {
			object.inventory[slot] = null;
		}
	},
	
	textureOf: function() {
		return game.textures.items_rawStoneShovel;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.rawStoneShovel.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "rawStoneShovel"
	},
	chanceData: {
		using: 0.375
	}
};

game.realms.gameWorld.craftingRecipes.rawStoneShovel = {
	in: [
		"sticks",
		"rock",
		"rock",
		"rock",
		"rock"
	],
	
	Out: game.realms.gameWorld.itemTypes.rawStoneShovel.New
};
