game.realms.gameWorld.itemTypes.stoneWall = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return game.textures.items_stoneWall;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.stoneWall.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stoneWall"
	}
};

game.realms.gameWorld.craftingRecipes.stoneWall = {
	in: [
		"rock",
		"rock",
		"rock",
		"rock"
	],
	
	Out: game.realms.gameWorld.itemTypes.stoneWall.New
};
