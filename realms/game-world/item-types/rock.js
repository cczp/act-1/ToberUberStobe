game.realms.gameWorld.itemTypes.rock = {
	textureOf: function() {
		return game.textures.items_rock;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.rock.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "rock"
	}
};
