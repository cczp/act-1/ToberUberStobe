game.realms.gameWorld.itemTypes._placeable = {
	use: function(object, slot, x, y) {
		const
			selectedPoint = game.realms.gameWorld.objectTypes[
				object.type
			].selectedPointOf(object, x, y),
			selectedObject = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
		
		if (!selectedObject) {
			game.sounds.items_placement.play();
			
			game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]] = new game.realms.gameWorld.objectTypes[
				this.placeableObjectType ||
				object.inventory[slot].type
			].New();
			
			if (!object.isGod) {
				object.inventory[slot] = null;
			}
		}
	},
	
	prototype: {
		type: ""
	},
	placeableObjectType: ""
};
Object.defineProperty(
	game.realms.gameWorld.itemTypes, "_placeable",
	
	{
		enumerable: false
	}
);
