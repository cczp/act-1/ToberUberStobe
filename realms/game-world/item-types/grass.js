game.realms.gameWorld.itemTypes.grass = {
	textureOf: function() {
		return game.textures.items_grass;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.grass.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "grass"
	}
};
