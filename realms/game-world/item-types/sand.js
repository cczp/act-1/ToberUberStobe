game.realms.gameWorld.itemTypes.sand = {
	textureOf: function() {
		return game.textures.items_sand;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.sand.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "sand"
	}
};

game.realms.gameWorld.cookingRecipes.sand = {
	in: "dirt",
	
	Out: game.realms.gameWorld.itemTypes.sand.New
};

game.realms.gameWorld.craftingRecipes.sand = {
	in: [
		"sandCovering",
		"sandCovering"
	],
	
	Out: game.realms.gameWorld.itemTypes.sand.New
};
