game.realms.gameWorld.itemTypes.stoneShovel = {
	use: game.realms.gameWorld.itemTypes._weapon.use,
	
	textureOf: function(item) {
		if (item.durability > this.prototype.durability / 2) {
			return game.textures.items_stoneShovel;
		} else {
			return game.textures.items_damagedStoneShovel;
		}
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.stoneShovel.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stoneShovel",
		
		attackDamage: 5,
		
		durability: 50
	}
};
