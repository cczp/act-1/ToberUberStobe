game.realms.gameWorld.itemTypes.dryGrass = {
	textureOf: function() {
		return game.textures.items_dryGrass;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.dryGrass.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "dryGrass"
	}
};

game.realms.gameWorld.cookingRecipes.dryGrass = {
	in: "grass",
	
	Out: game.realms.gameWorld.itemTypes.dryGrass.New
};

game.realms.gameWorld.cookingRecipes._fuelTypes.dryGrass = null;
