game.realms.gameWorld.itemTypes.sign = {
	use: function(object, slot, x, y) {
		const
			selectedPoint = game.realms.gameWorld.objectTypes[
				object.type
			].selectedPointOf(object, x, y),
			selectedObject = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]];
		
		if (!selectedObject) {
			game.sounds.items_placement.play();
			
			if (!object.isGod) {
				object.inventory[slot] = null;
			}
			
			const sign = game.realms.gameWorld.objects[[
				selectedPoint.x,
				selectedPoint.y
			]] = new game.realms.gameWorld.objectTypes.sign.New();
			
			game.dialogs.prompt(
				game.currentLocale.gui_dialogs_gameWorld_sign,
			"").then(function(text) {
				sign.text = [
					"",
					String(text)
				][+!!text].split("\\n").slice(0, 12);
			});
		}
	},
	
	textureOf: function() {
		return game.textures.items_sign;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.sign.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "sign"
	}
};

game.realms.gameWorld.craftingRecipes.sign = {
	in: [
		"rock",
		"rock",
		"rock"
	],
	
	Out: game.realms.gameWorld.itemTypes.sign.New
};
