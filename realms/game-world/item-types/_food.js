game.realms.gameWorld.itemTypes._food = {
	use: function(object, slot) {
		game.sounds.items_eating.play();
		
		if (!object.isGod) {
			object.inventory[slot] = null;
		}
		
		object.health += this.saturationData.health || 0;
		object.hunger += this.saturationData.hunger || 0;
		object.thirst += this.saturationData.thirst || 0;
	},
	
	prototype: {
		type: ""
	},
	saturationData: {
		health: 0,
		hunger: 0,
		thirst: 0
	}
};
Object.defineProperty(
	game.realms.gameWorld.itemTypes, "_food",
	
	{
		enumerable: false
	}
);
