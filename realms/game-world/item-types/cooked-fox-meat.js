game.realms.gameWorld.itemTypes.cookedFoxMeat = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_cookedFoxMeat;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.cookedFoxMeat.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "cookedFoxMeat"
	},
	saturationData: {
		hunger: 60,
		thirst: 15
	}
};

game.realms.gameWorld.cookingRecipes.cookedFoxMeat = {
	in: "foxMeat",
	
	Out: game.realms.gameWorld.itemTypes.cookedFoxMeat.New
};
