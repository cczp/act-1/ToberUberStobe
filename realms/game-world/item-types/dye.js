for (let i of [
	"red",
	"pink",
	"purple",
	"deepPurple",
	"indigo",
	"blue",
	"lightBlue",
	"cyan",
	"teal",
	"green",
	"lightGreen",
	"lime",
	"yellow",
	"amber",
	"orange",
	"deepOrange",
	"brown",
	"gray",
	"blueGray",
	"darkGray",
	"black",
	"white"
]) {
	game.realms.gameWorld.itemTypes["dye_" + i] = {
		textureOf: function() {
			return game.textures["items_dye_" + i];
		},
		
		New: function GameWorldItem(additions) {
			Object.assign(this,
				JSON.parse(JSON.stringify(
					game.realms.gameWorld.itemTypes["dye_" + i].prototype
				)),
				
				JSON.parse(JSON.stringify(additions || {}))
			);
		},
		prototype: {
			type: "dye_" + i
		}
	};
}
