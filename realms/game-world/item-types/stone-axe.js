game.realms.gameWorld.itemTypes.stoneAxe = {
	use: game.realms.gameWorld.itemTypes._weapon.use,
	
	textureOf: function(item) {
		if (item.durability > this.prototype.durability / 2) {
			return game.textures.items_stoneAxe;
		} else {
			return game.textures.items_damagedStoneAxe;
		}
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.stoneAxe.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "stoneAxe",
		
		attackDamage: 3.75,
		
		durability: 37
	}
};
