game.realms.gameWorld.itemTypes.console = {
	use: game.realms.gameWorld.itemTypes._placeable.use,
	
	textureOf: function() {
		return [
			game.textures.items_console,
			game.textures.items_console_1
		][+(game.realms.gameWorld.tick < 13)];
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.console.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "console"
	}
};
