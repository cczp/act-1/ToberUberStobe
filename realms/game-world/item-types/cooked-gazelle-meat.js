game.realms.gameWorld.itemTypes.cookedGazelleMeat = {
	use: game.realms.gameWorld.itemTypes._food.use,
	
	textureOf: function() {
		return game.textures.items_cookedGazelleMeat;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.cookedGazelleMeat.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "cookedGazelleMeat"
	},
	saturationData: {
		hunger: 70,
		thirst: 20
	}
};

game.realms.gameWorld.cookingRecipes.cookedGazelleMeat = {
	in: "gazelleMeat",
	
	Out: game.realms.gameWorld.itemTypes.cookedGazelleMeat.New
};
