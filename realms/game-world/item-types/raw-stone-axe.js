game.realms.gameWorld.itemTypes.rawStoneAxe = {
	use: function(object, slot) {
		game.sounds.gui_click.play();
		
		if (Math.random() < this.chanceData.using) {
			object.inventory[slot] = new game.realms.gameWorld.itemTypes.stoneAxe.New();
		} else if (!object.isGod) {
			object.inventory[slot] = null;
		}
	},
	
	textureOf: function() {
		return game.textures.items_rawStoneAxe;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.rawStoneAxe.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "rawStoneAxe"
	},
	chanceData: {
		using: 0.5
	}
};

game.realms.gameWorld.craftingRecipes.rawStoneAxe = {
	in: [
		"sticks",
		"rock",
		"rock"
	],
	
	Out: game.realms.gameWorld.itemTypes.rawStoneAxe.New
};
