game.realms.gameWorld.itemTypes.arrow = {
	textureOf: function() {
		return game.textures.items_arrow;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.arrow.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "arrow"
	}
};

game.realms.gameWorld.craftingRecipes.arrow = {
	in: [
		"sticks",
		"bones"
	],
	
	Out: game.realms.gameWorld.itemTypes.arrow.New
};
