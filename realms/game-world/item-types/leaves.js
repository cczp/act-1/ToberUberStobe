game.realms.gameWorld.itemTypes.leaves = {
	textureOf: function() {
		return game.textures.items_leaves;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.leaves.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "leaves"
	}
};
