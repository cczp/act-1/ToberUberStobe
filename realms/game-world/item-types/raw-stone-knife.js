game.realms.gameWorld.itemTypes.rawStoneKnife = {
	use: function(object, slot) {
		game.sounds.gui_click.play();
		
		if (Math.random() < this.chanceData.using) {
			object.inventory[slot] = new game.realms.gameWorld.itemTypes.stoneKnife.New();
		} else if (!object.isGod) {
			object.inventory[slot] = null;
		}
	},
	
	textureOf: function() {
		return game.textures.items_rawStoneKnife;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.rawStoneKnife.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "rawStoneKnife"
	},
	chanceData: {
		using: 0.75
	}
};

game.realms.gameWorld.craftingRecipes.rawStoneKnife = {
	in: [
		"rock",
		"rock"
	],
	
	Out: game.realms.gameWorld.itemTypes.rawStoneKnife.New
};
