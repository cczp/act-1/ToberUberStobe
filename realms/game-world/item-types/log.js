game.realms.gameWorld.itemTypes.log = {
	textureOf: function() {
		return game.textures.items_log;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.log.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "log"
	}
};
