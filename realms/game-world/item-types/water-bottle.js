game.realms.gameWorld.itemTypes.waterBottle = {
	use: function(object, slot) {
		game.sounds.items_drinking.play();
		
		if (!object.isGod) {
			object.inventory[slot].fullness--;
		}
		if (object.inventory[slot].fullness < 0) {
			object.inventory[slot] = new game.realms.gameWorld.itemTypes.bottle.New();
		}
		
		object.thirst += this.saturationData.thirstIncreasing;
	},
	
	textureOf: function(item) {
		return [
			game.textures.items_waterBottle,
			game.textures.items_waterBottle_1,
			game.textures.items_waterBottle_2
		][item.fullness];
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.waterBottle.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "waterBottle",
		
		fullness: 2
	},
	saturationData: {
		thirstIncreasing: 20
	}
};

game.realms.gameWorld.craftingRecipes.waterBottle = {
	in: [
		"bottle",
		"cactusPulp",
		"cactusPulp",
		"cactusPulp"
	],
	
	Out: game.realms.gameWorld.itemTypes.waterBottle.New
};
game.realms.gameWorld.craftingRecipes.waterBottle_1 = {
	in: [
		"bottle",
		"tomato",
		"tomato",
		"tomato"
	],
	
	Out: game.realms.gameWorld.itemTypes.waterBottle.New
};
game.realms.gameWorld.craftingRecipes.waterBottle_2 = {
	in: [
		"bottle",
		"eggplant",
		"eggplant"
	],
	
	Out: game.realms.gameWorld.itemTypes.waterBottle.New
};
