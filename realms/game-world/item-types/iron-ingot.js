game.realms.gameWorld.itemTypes.ironIngot = {
	textureOf: function() {
		return game.textures.items_ironIngot;
	},
	
	New: function GameWorldItem(additions) {
		Object.assign(this,
			JSON.parse(JSON.stringify(game.realms.gameWorld.itemTypes.ironIngot.prototype)),
			
			JSON.parse(JSON.stringify(additions || {}))
		);
	},
	prototype: {
		type: "ironIngot"
	}
};

game.realms.gameWorld.cookingRecipes.ironIngot = {
	in: "ironOre",
	
	Out: game.realms.gameWorld.itemTypes.ironIngot.New
};
