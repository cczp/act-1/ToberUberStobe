game.realms.gameWorld.itemTypes._weapon = {
	use: function(object, slot, x, y) {
		game.sounds.entities_attack.play();
		
		if (object.inventory[slot].durability <= 0) {
			object.inventory[slot] = null;
		} else {
			const
				selectedPoint = game.realms.gameWorld.objectTypes[
					object.type
				].selectedPointOf(object, x, y),
				selectedObject = game.realms.gameWorld.objects[[
					selectedPoint.x,
					selectedPoint.y
				]];
			
			if (selectedObject) {
				selectedObject.health -=
					object.attackDamage +
					object.inventory[slot].attackDamage;
				
				if (!object.isGod) {
					object.inventory[slot].durability--;
				}
				
				(
					game.realms.gameWorld.objectTypes[
						selectedObject.type
					].update ||
					function() {}
				).apply(game.realms.gameWorld.objectTypes[
					selectedObject.type
				], [selectedObject, selectedPoint.x, selectedPoint.y]);
				
				const selectedObject1 = game.realms.gameWorld.objects[[
					selectedPoint.x,
					selectedPoint.y
				]];
				
				if (!selectedObject1) {
					game.realms.gameWorld.objectTypes[
						object.type
					].catchItems(object, selectedObject.inventory.concat(
						game.realms.gameWorld.objectTypes[
							selectedObject.type
						].drop.map(function(currentItem) {
							if (Math.random() < currentItem.chance) {
								return new currentItem.New()
							}
						})
					));
				}
			}
		}
	},
	
	prototype: {
		type: "",
		
		attackDamage: 0,
		
		durability: 0
	}
};
Object.defineProperty(
	game.realms.gameWorld.itemTypes, "_weapon",
	
	{
		enumerable: false
	}
);
