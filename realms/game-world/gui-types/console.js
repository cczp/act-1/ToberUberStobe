game.realms.gameWorld.guiTypes.console = {
	//Secondary variables
		data: {},
		
		allowedObjects: {
			console: null
		},
		
		commands: {
			"no-such-command": {
				help: "",
				
				run: function(parameters, object) {
					throw new ReferenceError("no such command");
				}
			},
			
			"exit": {
				help: "",
				
				run: function() {
					game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
				}
			},
			
			"clear": {
				help: "",
				
				run: function(parameters, object) {
					object.log = "";
					object.scroll = 0;
				}
			},
			
			"run": {
				help: "[{command: [String], parameters: ..., *add-to-player-x(1): [Number], *add-to-player-y(1): [Number]}, ...]",
				
				run: function(parameters, object) {
					if (!(Array.isArray(parameters))) {
						throw new SyntaxError("command list is not an array");
					}
					
					for (let i = 0; i < parameters.length; i++) {
						if (
							parameters[i]["add-to-player-x"] &&
							parameters[i].parameters.x == null
						) {
							parameters[i].parameters.x =
								game.config.list.gameWorld.playerPosition.x +
								parameters[i]["add-to-player-x"];
						}
						if (
							parameters[i]["add-to-player-x1"] &&
							parameters[i].parameters.x1 == null
						) {
							parameters[i].parameters.x1 =
								game.config.list.gameWorld.playerPosition.x +
								parameters[i]["add-to-player-x1"];
						}
						if (
							parameters[i]["add-to-player-y"] &&
							parameters[i].parameters.y == null
						) {
							parameters[i].parameters.y =
								game.config.list.gameWorld.playerPosition.y +
								parameters[i]["add-to-player-y"];
						}
						if (
							parameters[i]["add-to-player-y1"] &&
							parameters[i].parameters.y1 == null
						) {
							parameters[i].parameters.y1 =
								game.config.list.gameWorld.playerPosition.y +
								parameters[i]["add-to-player-y1"];
						}
						
						(
							game.realms.gameWorld.guiTypes.console.commands[
								parameters[i].command
							] ||
							game.realms.gameWorld.guiTypes.console.commands["no-such-command"]
						).run(parameters[i].parameters, object);
					}
				}
			},
			
			"say": {
				help: "[String]",
				
				run: function(parameters, object) {
					object.log += String(parameters) + "\n";
				}
			},
			
			"tell": {
				help: "[String]",
				
				run: function(parameters, object) {
					game.dialogs.alert(parameters);
				}
			},
			
			"help": {
				help: "",
				
				run: function(parameters, object) {
					object.log +=
						"List of all commands " +
						"(parameters with * can be omitted):\n";
					
					for (let i in game.realms.gameWorld.guiTypes.console.commands) {
						object.log +=
							i + " " +
							game.realms.gameWorld.guiTypes.console.commands[i].help +
							"\n";
					}
				}
			},
			
			"position": {
				help: "",
				
				run: function(parameters, object) {
					object.log +=
						"Camera:\n    " +
							"X: " + game.config.list.gameWorld.cameraPosition.x + ", " +
							"Y: " + game.config.list.gameWorld.cameraPosition.y + "\n" +
						"Player:\n    " +
							"X: " + game.config.list.gameWorld.playerPosition.x + ", " +
							"Y: " + game.config.list.gameWorld.playerPosition.y + "\n";
				}
			},
			
			"suicide": {
				help: "",
				
				run: function(parameters, object) {
					game.realms.gameWorld.fail(
						game.realms.gameWorld.playerObject,
						
						game.config.list.gameWorld.playerPosition.x,
						game.config.list.gameWorld.playerPosition.y
					);
				}
			},
			
			"move-camera-to": {
				help: "{x: [Number or null], y: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					game.config.list.gameWorld.cameraPosition.x = parameters.x;
					game.config.list.gameWorld.cameraPosition.y = parameters.y;
				}
			},
			
			"teleport-to": {
				help: "{x: [Number or null], y: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					if (game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]]) {
						throw new ReferenceError("target is busy");
					}
					
					[
						game.realms.gameWorld.objects[[
							parameters.x,
							parameters.y
						]],
						game.realms.gameWorld.objects[[
							game.config.list.gameWorld.playerPosition.x,
							game.config.list.gameWorld.playerPosition.y
						]]
					] = [
						game.realms.gameWorld.objects[[
							game.config.list.gameWorld.playerPosition.x,
							game.config.list.gameWorld.playerPosition.y
						]],
						game.realms.gameWorld.objects[[
							parameters.x,
							parameters.y
						]]
					];
					
					game.config.list.gameWorld.cameraPosition.x =
						parameters.x -
						game.realms.gameWorld.playerOffset.x;
					game.config.list.gameWorld.cameraPosition.y =
						parameters.y -
						game.realms.gameWorld.playerOffset.y;
					
					game.config.list.gameWorld.playerPosition.x = parameters.x;
					game.config.list.gameWorld.playerPosition.y = parameters.y;
				}
			},
			
			"set-experience": {
				help: "[Number/String]",
				
				run: function(parameters, object) {
					game.realms.gameWorld.playerObject.experience = parameters;
				}
			},
			
			"get-item-at": {
				help: "{x: [Number or null], y: [Number or null], slot: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					const destinationObject = game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]];
					
					if (!destinationObject) {
						throw new SyntaxError("destination object is empty");
					}
					
					if (parameters.slot == null) {
						for (let i = 0; i < destinationObject.inventory.length; i++) {
							if (!destinationObject.inventory[i]) {
								parameters.slot = i;
								
								break;
							}
						}
					}
					
					parameters.slot = parseInt(parameters.slot);
					if (!isFinite(parameters.slot) || parameters.slot < 0) {
						throw new SyntaxError("invalid slot");
					}
					
					object.log += JSON.stringify(
						destinationObject.inventory[parameters.slot] || null,
					null, 4) + "\n";
				}
			},
			
			"set-item-at": {
				help: "{x: [Number or null], y: [Number or null], slot: [Number or null], type: [String]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					const destinationObject = game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]];
					
					if (!destinationObject) {
						throw new SyntaxError("destination object is empty");
					}
					
					if (parameters.slot == null) {
						for (let i = 0; i < destinationObject.inventory.length; i++) {
							if (!destinationObject.inventory[i]) {
								parameters.slot = i;
								
								break;
							}
						}
					}
					
					parameters.slot = parseInt(parameters.slot) % game.realms.gameWorld.objectTypes[
						destinationObject.type
					].prototype.inventory.length;
					if (!isFinite(parameters.slot) || parameters.slot < 0) {
						throw new SyntaxError("invalid slot");
					}
					
					if (parameters.type == null) {
						destinationObject.inventory[parameters.slot] = null;
						
						return;
					}
					
					if (!(parameters.type in game.realms.gameWorld.itemTypes)) {
						throw new ReferenceError("no such item type");
					}
					
					destinationObject.inventory[parameters.slot] = 
						new game.realms.gameWorld.itemTypes[parameters.type].New();
				}
			},
			
			"clone-item-at": {
				help: "{x: [Number or null], y: [Number or null], slot: [Number or null], x1: [Number or null], y1: [Number or null], slot1: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					if (parameters.x1 == null) {
						parameters.x1 = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y1 == null) {
						parameters.y1 = game.config.list.gameWorld.playerPosition.y;
					}
					
					const originObject = game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]];
					
					if (!originObject) {
						throw new SyntaxError("origin object is empty");
					}
					
					const destinationObject = game.realms.gameWorld.objects[[
						parameters.x1,
						parameters.y1
					]];
					
					if (!destinationObject) {
						throw new SyntaxError("destination object is empty");
					}
					
					if (parameters.slot1 == null) {
						for (const i = 0; i < destinationObject.inventory.length; i++) {
							if (!destinationObject.inventory[i]) {
								parameters.slot1 = i;
								
								break;
							}
						}
					}
					
					parameters.slot = parseInt(parameters.slot);
					parameters.slot1 = parseInt(parameters.slot1);
					if (
						!isFinite(parameters.slot) || parameters.slot < 0 ||
						!isFinite(parameters.slot1) || parameters.slot1 < 0
					) {
						throw new SyntaxError("invalid slot");
					}
					
					destinationObject.inventory[
						parameters.slot1
					] = JSON.parse(JSON.stringify(
						originObject.inventory[parameters.slot] || null
					));
				}
			},
			
			"get-background-at": {
				help: "{x: [Number or null], y: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					object.log += JSON.stringify(game.realms.gameWorld.background[[
						parameters.x,
						parameters.y
					]] || null, null, 4) + "\n";
				}
			},
			
			"set-background-at": {
				help: "{x: [Number or null], y: [Number or null], type: [String]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					game.realms.gameWorld.background[[
						parameters.x,
						parameters.y
					]] = parameters.type;
				}
			},
			
			"clone-background-at": {
				help: "{x: [Number or null], y: [Number or null], x1: [Number or null], y1: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					if (parameters.x1 == null) {
						parameters.x1 = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y1 == null) {
						parameters.y1 = game.config.list.gameWorld.playerPosition.y;
					}
					
					game.realms.gameWorld.background[[
						parameters.x1,
						parameters.y1
					]] = game.realms.gameWorld.background[[
						parameters.x,
						parameters.y
					]];
				}
			},
			
			"get-object-at": {
				help: "{x: [Number or null], y: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					object.log += JSON.stringify(game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]] || null, null, 4) + "\n";
				}
			},
			
			"set-object-at": {
				help: "{x: [Number or null], y: [Number or null], type: [String]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					if (
						(game.realms.gameWorld.objects[[
							parameters.x,
							parameters.y
						]] || {}).type in
							game.realms.gameWorld.objectTypesYouMustNotTouch
					) {
						throw new ReferenceError("target is busy");
					}
					
					if (parameters.type == null) {
						game.realms.gameWorld.objects[[
							parameters.x,
							parameters.y
						]] = null;
						
						return;
					}
					
					if (!(parameters.type in game.realms.gameWorld.objectTypes)) {
						throw new ReferenceError("no such object type");
					}
					
					if (parameters.type in game.realms.gameWorld.objectTypesYouMustNotTouch) {
						throw new SyntaxError("forbidden object type");
					}
					
					game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]] = new game.realms.gameWorld.objectTypes[
						parameters.type
					].New();
				}
			},
			
			"clone-object-at": {
				help: "{x: [Number or null], y: [Number or null], x1: [Number or null], y1: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					if (parameters.x1 == null) {
						parameters.x1 = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y1 == null) {
						parameters.y1 = game.config.list.gameWorld.playerPosition.y;
					}
					
					const originObject = game.realms.gameWorld.objects[[
						parameters.x,
						parameters.y
					]];
					
					if (originObject.type in game.realms.gameWorld.objectTypesYouMustNotTouch) {
						throw new SyntaxError("forbidden origin object type");
					}
					
					const destinationObject = game.realms.gameWorld.objects[[
						parameters.x1,
						parameters.y1
					]];
					
					if ((destinationObject || {}).type in game.realms.gameWorld.objectTypesYouMustNotTouch) {
						throw new ReferenceError("target is busy");
					}
					
					game.realms.gameWorld.objects[[
						parameters.x1,
						parameters.y1
					]] = JSON.parse(JSON.stringify(originObject || null));
				}
			},
			
			"get-particle-at": {
				help: "{x: [Number or null], y: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					object.log += JSON.stringify(game.realms.gameWorld.particles[[
						parameters.x,
						parameters.y
					]] || null, null, 4) + "\n";
				}
			},
			
			"set-particle-at": {
				help: "{x: [Number or null], y: [Number or null], type: [String]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					
					if (parameters.type == null) {
						game.realms.gameWorld.particles[[
							parameters.x,
							parameters.y
						]] = null;
						
						return;
					}
					
					if (!(parameters.type in game.realms.gameWorld.particleTypes)) {
						throw new ReferenceError("no such particle type");
					}
					
					game.realms.gameWorld.particles[[
						parameters.x,
						parameters.y
					]] = new game.realms.gameWorld.particleTypes[
						parameters.type
					].New();
				}
			},
			
			"clone-particle-at": {
				help: "{x: [Number or null], y: [Number or null], x1: [Number or null], y1: [Number or null]}",
				
				run: function(parameters, object) {
					if (!parameters) {
						throw new SyntaxError("no parameters");
					}
					
					if (parameters.x == null) {
						parameters.x = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y == null) {
						parameters.y = game.config.list.gameWorld.playerPosition.y;
					}
					if (parameters.x1 == null) {
						parameters.x1 = game.config.list.gameWorld.playerPosition.x;
					}
					if (parameters.y1 == null) {
						parameters.y1 = game.config.list.gameWorld.playerPosition.y;
					}
					
					const
						originParticle = game.realms.gameWorld.particles[[
							parameters.x,
							parameters.y
						]],
						destinationParticle = game.realms.gameWorld.particles[[
							parameters.x1,
							parameters.y1
						]];
					
					game.realms.gameWorld.particles[[
						parameters.x1,
						parameters.y1
					]] = JSON.parse(JSON.stringify(originParticle || null));
				}
			}
		},
	
	//Methods
		handleInput: function() {
			const
				playerSelectedPoint = game.realms.gameWorld.objectTypes[
					game.realms.gameWorld.playerObject.type
				].selectedPointOf(
					game.realms.gameWorld.playerObject,
					
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				),
				playerSelectedObject = game.realms.gameWorld.objects[[
					playerSelectedPoint.x,
					playerSelectedPoint.y
				]];
			
			if (!((playerSelectedObject || {}).type in this.allowedObjects)) {
				game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
				
				game.input.keyboard.pressed.length = 0;
				game.input.keyboard.released.length = 0;
				
				return;
			}
			
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][0] >= 0 &&
					game.input.mouse.released[i][0] < 320 &&
					game.input.mouse.released[i][1] >= 0 &&
					game.input.mouse.released[i][1] < 240
				) {
					game.input.keyboard.pressed.push(game.config.list.main.controls.use);
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						game.dialogs.prompt(
							game.currentLocale.gui_dialogs_gameWorld_console,
						"").then(function(command) {
							if (command !== null) {
								command = String(command);
								
								playerSelectedObject.log += (command || "") + "\n";
								
								//Run command
									if (command) {
										const
											realCommand = command.split(" "),
											
											commandName = realCommand[0],
											parameters = realCommand.slice(1).join(" ") || "null";
										
										try {
											(
												game.realms.gameWorld.guiTypes.console.commands[commandName] ||
												game.realms.gameWorld.guiTypes.console.commands["no-such-command"]
											).run(JSON.parse(parameters), playerSelectedObject);
										} catch (error) {
											playerSelectedObject.log += error + "\n";
										}
									}
								
								playerSelectedObject.log += playerSelectedObject.prompt;
							}
						});
						
						break;
					case game.config.list.main.controls.down:
						if (playerSelectedObject.scroll > 0) {
							playerSelectedObject.scroll--;
						}
						
						break;
					case game.config.list.main.controls.up:
						if (
							playerSelectedObject.scroll <
								playerSelectedObject.log.split("\n").length - 30
						) {
							playerSelectedObject.scroll++;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			const
				playerSelectedPoint = game.realms.gameWorld.objectTypes[
					game.realms.gameWorld.playerObject.type
				].selectedPointOf(
					game.realms.gameWorld.playerObject,
					
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				),
				playerSelectedObject = game.realms.gameWorld.objects[[
					playerSelectedPoint.x,
					playerSelectedPoint.y
				]];
			
			if (!((playerSelectedObject || {}).type in this.allowedObjects)) {
				game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
				
				return [];
			}
				
			if (!game.realms.gameWorld.playerObject.isGod) {
				return [];
			}
			
			let
				log = (playerSelectedObject.log + [
					"",
					"_"
				][+(
					Math.floor(Date.now() / 500) % 2 == 0
				)]).split("\n"),
				
				layer = [
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_console,
						
						x: 0,
						y: 0
					}
				];
			
			for (let i = 0; i < log.length; i++) {
				layer.push({
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "left",
					
					string: log[
						log.length - i - 1 -
						playerSelectedObject.scroll
					].slice(0, 122),
					
					x: 0,
					y: 236 - i * 5
				});
				
				if (i == 47) {
					break;
				}
			}
			
			return layer;
		}
};
