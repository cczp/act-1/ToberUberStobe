game.realms.gameWorld.guiTypes._container = {
	renderInventory: function(inventory, layer, x, y) {
		for (let y1 = 0; y1 < 6; y1++) {
			for (let x1 = 0; x1 < 8; x1++) {
				const currentItem = inventory[y1 * 8 + x1];
				
				if (currentItem) {
					layer.push({
						type: "sprite",
						
						texture: game.realms.gameWorld.itemTypes[
							currentItem.type
						].textureOf(currentItem),
						
						x: x + x1 * 17,
						y: y + y1 * 17,
						
						width: 16,
						height: 16
					});
				}
			}
		}
	},
	renderInventory_backgroundTexture: function(inventory, layer, x, y) {
		for (let y1 = 0; y1 < 6; y1++) {
			for (let x1 = 0; x1 < 8; x1++) {
				const currentBackgroundTexture = inventory[y1 * 8 + x1];
				
				if (!currentBackgroundTexture) {
					continue;
				}
				
				layer.push(currentBackgroundTexture(x + x1 * 17, y + y1 * 17));
			}
		}
	},
	renderInventory_object: function(inventory, layer, x, y) {
		for (let y1 = 0; y1 < 6; y1++) {
			for (let x1 = 0; x1 < 8; x1++) {
				const currentObject = inventory[y1 * 8 + x1];
				
				if (!currentObject) {
					continue;
				}
				
				layer.push(game.realms.gameWorld.objectTypes[
					currentObject.type
				].render(
					currentObject,
					
					x + x1 * 17,
					y + y1 * 17
				));
			}
		}
	},
	renderInventory_particle: function(inventory, layer, x, y) {
		for (let y1 = 0; y1 < 6; y1++) {
			for (let x1 = 0; x1 < 8; x1++) {
				const currentParticle = inventory[y1 * 8 + x1];
				
				if (!currentParticle) {
					continue;
				}
				
				layer.push(game.realms.gameWorld.particleTypes[
					currentParticle.type
				].render(
					currentParticle,
					
					x + x1 * 17,
					y + y1 * 17
				));
			}
		}
	}
};
Object.defineProperty(
	game.realms.gameWorld.guiTypes, "_container",
	
	{
		enumerable: false
	}
);
