game.realms.gameWorld.guiTypes.default = {
	//Secondary variables
		data: {
			selectedControl: -1
		},
		
		controls: [
			{
				type: "squareButton",
				
				caption: "...",
				
				x: 150,
				y: -10,
				
				event: function() {
					game.realms.gameWorld.guiTypes.default.data.selectedControl = -1;
					
					game.input.keyboard.pressed.push(game.config.list.main.controls.command);
				}
			}
		],
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.pressed.length; i++) {
				if (
					game.input.mouse.pressed[i][0] >= 248 &&
					game.input.mouse.pressed[i][0] < 320 &&
					game.input.mouse.pressed[i][1] >= 204 &&
					game.input.mouse.pressed[i][1] < 240
				) {
					game.realms.gameWorld.playerObject.isKillingMachine = true;
					
					game.input.keyboard.pressed.push(game.config.list.main.controls.use);
				}
			}
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				const selectedControl = game.guiControls.checkMouseSelectionOf(
					this.controls,
					
					game.input.mouse.moved[i][0],
					game.input.mouse.moved[i][1]
				);
				
				if (this.data.selectedControl != selectedControl) {
					game.sounds.gui_scroll.play();
					
					this.data.selectedControl = selectedControl;
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][1] >= 0 &&
					game.input.mouse.released[i][1] < 24
				) {
					if (
						game.input.mouse.released[i][0] >= 0 &&
						game.input.mouse.released[i][0] < 24
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.magic);
					} else if (
						game.input.mouse.released[i][0] >= 296 &&
						game.input.mouse.released[i][0] < 320
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.switch);
					}
				} else if (
					game.input.mouse.released[i][1] >= 204 &&
					game.input.mouse.released[i][1] < 240
				) {
					if (
						game.input.mouse.released[i][0] >= 0 &&
						game.input.mouse.released[i][0] < 36
					) {
						game.realms.gameWorld.playerObject.isRunning = !game.realms.gameWorld.playerObject.isRunning;
					} else if (
						game.input.mouse.released[i][0] >= 248 &&
						game.input.mouse.released[i][0] < 320
					) {
						game.realms.gameWorld.playerObject.isKillingMachine = false;
						
					}
				} else if (
					game.input.mouse.released[i][1] >= 112 &&
					game.input.mouse.released[i][1] < 128
				) {
					if (
						game.input.mouse.released[i][0] >= 144 &&
						game.input.mouse.released[i][0] < 160
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.left);
					} else if (
						game.input.mouse.released[i][0] >= 176 &&
						game.input.mouse.released[i][0] < 192
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.right);
					}
				} else if (
					game.input.mouse.released[i][0] >= 160 &&
					game.input.mouse.released[i][0] < 176
				) {
					if (
						game.input.mouse.released[i][1] >= 128 &&
						game.input.mouse.released[i][1] < 144
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.down);
					} else if (
						game.input.mouse.released[i][1] >= 96 &&
						game.input.mouse.released[i][1] < 112
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.up);
					}
				}
				
				//Handle control selection
					const pressedControl = game.guiControls.checkMouseSelectionOf(
						this.controls,
						
						game.input.mouse.released[i][0],
						game.input.mouse.released[i][1]
					);
					
					if (pressedControl >= 0) {
						game.sounds.gui_click.play();
						
						(this.controls[pressedControl].event || function() {})();
					}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.pause;
						
						break;
					case game.config.list.main.controls.use:
						game.realms.gameWorld.playerObject.isUsingItem = true;
						
						break;
					case game.config.list.main.controls.switch:
						game.sounds.gui_click.play();
						
						const
							selectedPoint = game.realms.gameWorld.objectTypes[
								game.realms.gameWorld.playerObject.type
							].selectedPointOf(
								game.realms.gameWorld.playerObject,
								
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							),
							selectedObject = game.realms.gameWorld.objects[[
								selectedPoint.x,
								selectedPoint.y
							]];
						
						game.realms.gameWorld.currentGuiType = (game.realms.gameWorld.objectTypes[(
							selectedObject || {}
						).type] || {}).gui || [
							game.realms.gameWorld.guiTypes.inventory,
							game.realms.gameWorld.guiTypes.godInventory
						][+game.realms.gameWorld.playerObject.isGod];
						
						break;
					case game.config.list.main.controls.magic:
						game.sounds.gui_gameWorld_default_godModeSwitching.play();
						
						game.realms.gameWorld.playerObject.isGod = !game.realms.gameWorld.playerObject.isGod;
						
						break;
					case game.config.list.main.controls.command:
						game.sounds.gui_click.play();
						
						game.dialogs.prompt(
							game.currentLocale.gui_dialogs_gameWorld_default_commandEntry,
						"").then(function(message) {
							if (!message) {
								return;
							}
							
							message = String(message);
							
							const virtualConsole = new game.realms.gameWorld.objectTypes.console.New();
							
							virtualConsole.log = "";
							
							//Run command
								const
									realCommand = message.split(" "),
									
									commandName = realCommand[0],
									parameters = realCommand.slice(1).join(" ") || "null";
								
								try {
									(
										game.realms.gameWorld.guiTypes.console.commands[
											commandName
										] ||
										game.realms.gameWorld.guiTypes.console.commands["no-such-command"]
									).run(JSON.parse(parameters), virtualConsole);
								} catch (error) {
									virtualConsole.log += error + "\n";
								}
							
							if (virtualConsole.log) {
								game.dialogs.alert(virtualConsole.log);
							}
						});
						
						break;
					case game.config.list.main.controls.run:
						game.realms.gameWorld.playerObject.isRunning = true;
						
						break;
					case game.config.list.main.controls.down:
						game.realms.gameWorld.playerObject.walkingDirection = 0;
						
						break;
					case game.config.list.main.controls.left:
						game.realms.gameWorld.playerObject.walkingDirection = 1;
						
						break;
					case game.config.list.main.controls.up:
						game.realms.gameWorld.playerObject.walkingDirection = 2;
						
						break;
					case game.config.list.main.controls.right:
						game.realms.gameWorld.playerObject.walkingDirection = 3;
						
						break;
				}
			}
			for (let i = 0; i < game.input.keyboard.released.length; i++) {
				switch (game.input.keyboard.released[i]) {
					case game.config.list.main.controls.run:
						game.realms.gameWorld.playerObject.isRunning = false;
						
						break;
				}
			}
		},
		
		render: function() {
			const
				layer = [
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_default,
						
						x: 0,
						y: 0
					},
					
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_default_indicators,
						
						y0: 0,
						
						width0: Math.round(
							game.realms.gameWorld.playerObject.health * 0.62
						),
						height0: 2,
						
						x: 9,
						y: 213,
						
						width: Math.round(
							game.realms.gameWorld.playerObject.health * 0.62
						),
						height: 2
					},
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_default_indicators,
						
						y0: 2,
						
						width0: Math.round(
							game.realms.gameWorld.playerObject.hunger * 0.62
						),
						height0: 2,
						
						x: 9,
						y: 221,
						
						width: Math.round(
							game.realms.gameWorld.playerObject.hunger * 0.62
						),
						height: 2
					},
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_default_indicators,
						
						y0: 4,
						
						width0: Math.round(
							game.realms.gameWorld.playerObject.thirst * 0.62
						),
						height0: 2,
						
						x: 9,
						y: 229,
						
						width: Math.round(
							game.realms.gameWorld.playerObject.thirst * 0.62
						),
						height: 2
					},
					
					{
						type: "text",
						
						fontSize: 8,
						fontColor: game.colorScheme.text_light,
						textAlign: "left",
						
						string: [
							game.realms.gameWorld.playerObject.experience,
							game.currentLocale.gui_gameWorld_default_godMode
						][+game.realms.gameWorld.playerObject.isGod],
						
						x: 17,
						y: 11.35
					}
				];
			
			if (game.realms.gameWorld.playerObject.inventory[0]) {
				layer.push({
					type: "sprite",
					
					texture: game.realms.gameWorld.itemTypes[
						game.realms.gameWorld.playerObject.inventory[0].type
					].textureOf(
						game.realms.gameWorld.playerObject.inventory[0]
					),
					
					x: 294,
					y: 214
				});
			}
			
			return layer.concat(game.guiControls.render(
				this.controls,
			this.data.selectedControl));
		}
};
