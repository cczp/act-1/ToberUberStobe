game.realms.gameWorld.guiTypes.inventory = {
	//Secondary variables
		data: {
			selectedItem: {
				x: 0,
				y: 0
			},
			selectedForCraftingItems: [],
			selectedControl: -1
		},
		
		controls: [
			{
				type: "squareButton",
				
				caption: "DL",
				
				x: 292,
				y: 212
			},
			{
				type: "squareButton",
				
				caption: "CR",
				
				x: 292,
				y: 184
			}
		],
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				if (
					game.input.mouse.moved[i][0] >= 91 &&
					game.input.mouse.moved[i][0] < 227 &&
					game.input.mouse.moved[i][1] >= 69 &&
					game.input.mouse.moved[i][1] < 171
				) {
					const selectedItem = {
						x: Math.floor((game.input.mouse.moved[i][0] - 91) / 17),
						y: Math.floor((game.input.mouse.moved[i][1] - 69) / 17)
					};
					
					if (JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)) {
						game.sounds.gui_scroll.play();
						
						this.data.selectedItem = selectedItem;
					}
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][0] >= 91 &&
					game.input.mouse.released[i][0] < 227 &&
					game.input.mouse.released[i][1] >= 69 &&
					game.input.mouse.released[i][1] < 171
				) {
					switch (this.data.selectedControl) {
						case 0:
							game.input.keyboard.pressed.push(game.config.list.main.controls.magic);
							
							break;
						case 1:
							game.input.keyboard.pressed.push(game.config.list.main.controls.switch);
							
							break;
						default:
							game.input.keyboard.pressed.push(game.config.list.main.controls.use);
							
							break;
					}
				}
				
				//Handle control selection
					const selectedControl = game.guiControls.checkMouseSelectionOf(
						this.controls,
						
						game.input.mouse.released[i][0],
						game.input.mouse.released[i][1]
					);
					
					if (this.data.selectedControl != selectedControl) {
						game.sounds.gui_scroll.play();
						
						this.data.selectedControl = selectedControl;
					}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						if (this.data.selectedForCraftingItems.length == 0) {
							[
								game.realms.gameWorld.playerObject.inventory[0],
								game.realms.gameWorld.playerObject.inventory[
									this.data.selectedItem.y * 8 + this.data.selectedItem.x
								]
							] = [
								game.realms.gameWorld.playerObject.inventory[
									this.data.selectedItem.y * 8 + this.data.selectedItem.x
								],
								game.realms.gameWorld.playerObject.inventory[0]
							];
						} else {
							const selectedForCraftingItemsTypes = this.data.selectedForCraftingItems.map(
								function(currentItem) {
									return (game.realms.gameWorld.playerObject.inventory[
										currentItem.y * 8 + currentItem.x
									] || {type: null}).type;
								},
							this);
							
							for (let j in game.realms.gameWorld.craftingRecipes) {
								if (
									JSON.stringify(
										game.realms.gameWorld.craftingRecipes[j].in
									) == JSON.stringify(selectedForCraftingItemsTypes)
								) {
									for (
										let k = 0;
										k < this.data.selectedForCraftingItems.length;
										k++
									) {
										game.realms.gameWorld.playerObject.inventory[
											this.data.selectedForCraftingItems[k].y * 8 +
											this.data.selectedForCraftingItems[k].x
										] = null;
									}
									
									game.realms.gameWorld.playerObject.inventory[
										this.data.selectedItem.y * 8 +
										this.data.selectedItem.x
									] = new game.realms.gameWorld.craftingRecipes[j].Out();
									
									break;
								}
							}
							
							this.data.selectedForCraftingItems.length = 0;
						}
						
						break;
					case game.config.list.main.controls.switch:
						game.sounds.gui_click.play();
						
						let
							slotAvailable = true,
							oldSlotNumber;
						
						for (
							let j = 0;
							j < this.data.selectedForCraftingItems.length;
							j++
						) {
							if (
								this.data.selectedForCraftingItems[j].x == this.data.selectedItem.x &&
								this.data.selectedForCraftingItems[j].y == this.data.selectedItem.y
							) {
								slotAvailable = false;
								oldSlotNumber = j;
								
								break;
							}
						}
						
						if (slotAvailable) {
							this.data.selectedForCraftingItems.push({
								x: this.data.selectedItem.x,
								y: this.data.selectedItem.y
							});
						} else {
							this.data.selectedForCraftingItems.splice(oldSlotNumber, 1);
						}
						
						break;
					case game.config.list.main.controls.magic:
						game.sounds.entities_itemCatching.play();
						
						game.realms.gameWorld.playerObject.inventory[
							this.data.selectedItem.y * 8 + this.data.selectedItem.x
						] = null;
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.y < 5) {
							this.data.selectedItem.y++;
						}
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x > 0) {
							this.data.selectedItem.x--;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.y > 0) {
							this.data.selectedItem.y--;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x < 7) {
							this.data.selectedItem.x++;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			const layer = [
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_inventory,
					
					x: 0,
					y: 0
				},
				
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_dark,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameWorld_inventory_title,
					
					x: 160,
					y: 64
				}
			];
			
			game.realms.gameWorld.guiTypes._container.renderInventory(
				game.realms.gameWorld.playerObject.inventory,
			layer, 92, 70);
			
			for (
				let i = 0;
				i < this.data.selectedForCraftingItems.length;
				i++
			) {
				layer.push({
					type: "sprite",
					
					texture: game.textures.gui_itemSelector,
					
					y0: 16,
					
					width0: 16,
					height0: 16,
					
					x: 92 + this.data.selectedForCraftingItems[i].x * 17,
					y: 70 + this.data.selectedForCraftingItems[i].y * 17,
					
					width: 16,
					height: 16
				});
			}
			layer.push({
				type: "sprite",
				
				texture: game.textures.gui_itemSelector,
				
				width0: 16,
				height0: 16,
				
				x: 92 + this.data.selectedItem.x * 17,
				y: 70 + this.data.selectedItem.y * 17,
				
				width: 16,
				height: 16
			});
			
			return layer.concat(game.guiControls.render(
				this.controls,
			this.data.selectedControl));
		}
};
