game.realms.gameWorld.guiTypes.sign = {
	//Secondary variables
		data: {},
		
		allowedObjects: {
			sign: null
		},
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
				}
			}
		},
		
		render: function() {
			const layer = [
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_sign,
					
					x: 0,
					y: 0
				}
			];
			
			//Render sign text
				const
					playerSelectedPoint = game.realms.gameWorld.objectTypes[
						game.realms.gameWorld.playerObject.type
					].selectedPointOf(
						game.realms.gameWorld.playerObject,
						
						game.config.list.gameWorld.playerPosition.x,
						game.config.list.gameWorld.playerPosition.y
					),
					playerSelectedObject = game.realms.gameWorld.objects[[
						playerSelectedPoint.x,
						playerSelectedPoint.y
					]];
					
					if (!((
						playerSelectedObject ||
						{}
					).type in this.allowedObjects)) {
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						return [];
					}
					
					for (let i = 0; i < playerSelectedObject.text.length; i++) {
						layer.push({
							type: "text",
							
							fontSize: 16,
							fontColor: game.colorScheme.text_gameWorld_sign,
							textAlign: "center",
							
							string: playerSelectedObject.text[i].slice(0, 26),
							
							x: 160,
							y: 9 + i * 20
						});
					}
			
			return layer;
		}
};
