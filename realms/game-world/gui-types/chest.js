game.realms.gameWorld.guiTypes.chest = {
	//Secondary variables
		data: {
			mode: 0,
			selectedItem: {
				x: 0,
				y: 0
			}
		},
		
		allowedObjects: {
			chest: null
		},
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				if (
					game.input.mouse.moved[i][0] >= 91 &&
					game.input.mouse.moved[i][0] < 227
				) {
					if (
						game.input.mouse.moved[i][1] >= 13 &&
						game.input.mouse.moved[i][1] < 115
					) {
						const selectedItem = {
							x: Math.floor((game.input.mouse.moved[i][0] - 91) / 17),
							y: Math.floor((game.input.mouse.moved[i][1] - 13) / 17)
						};
						
						if (
							this.data.mode != 0 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 0;
							this.data.selectedItem = selectedItem;
						}
					} else if (
						game.input.mouse.moved[i][1] >= 125 &&
						game.input.mouse.moved[i][1] < 227
					) {
						const selectedItem = {
							x: Math.floor((game.input.mouse.moved[i][0] - 91) / 17),
							y: Math.floor((game.input.mouse.moved[i][1] - 125) / 17)
						};
						
						if (
							this.data.mode != 1 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 1;
							this.data.selectedItem = selectedItem;
						}
					}
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][0] >= 91 &&
					game.input.mouse.released[i][0] < 227 && (
						game.input.mouse.released[i][1] >= 13 &&
						game.input.mouse.released[i][1] < 115 ||
						
						game.input.mouse.released[i][1] >= 125 &&
						game.input.mouse.released[i][1] < 227
					)
				) {
					game.input.keyboard.pressed.push(game.config.list.main.controls.use);
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.blocks_chest_closing.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						const 
							playerSelectedPoint = game.realms.gameWorld.objectTypes[
								game.realms.gameWorld.playerObject.type
							].selectedPointOf(
								game.realms.gameWorld.playerObject,
								
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							),
							playerSelectedObject = game.realms.gameWorld.objects[[
								playerSelectedPoint.x,
								playerSelectedPoint.y
							]];
						
						if (!((
							playerSelectedObject ||
							{}
						).type in this.allowedObjects)) {
							game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
							
							break;
						}
						
						const chestInventory = playerSelectedObject.inventory;
						
						switch (this.data.mode) {
							case 0:
								game.realms.gameWorld.objectTypes[
									game.realms.gameWorld.playerObject.type
								].catchItems(
									game.realms.gameWorld.playerObject,
									
									[chestInventory[
										this.data.selectedItem.y * 8 + this.data.selectedItem.x
									]]
								);
								
								chestInventory[
									this.data.selectedItem.y * 8 + this.data.selectedItem.x
								] = null;
								
								break;
							case 1:
								game.realms.gameWorld.objectTypes[
									playerSelectedObject.type
								].catchItems(
									playerSelectedObject,
									
									[game.realms.gameWorld.playerObject.inventory[
										this.data.selectedItem.y * 8 + this.data.selectedItem.x
									]]
								);
								
								game.realms.gameWorld.playerObject.inventory[
									this.data.selectedItem.y * 8 + this.data.selectedItem.x
								] = null;
								
								break;
						}
						
						break;
					case game.config.list.main.controls.switch:
						game.sounds.gui_click.play();
						
						this.data.mode = [1, 0][this.data.mode];
						this.data.selectedItem.x = 0;
						this.data.selectedItem.y = 0;
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.y < 5) {
							this.data.selectedItem.y++;
						}
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x > 0) {
							this.data.selectedItem.x--;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.y > 0) {
							this.data.selectedItem.y--;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x < 7) {
							this.data.selectedItem.x++;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			const layer = [
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_container,
					
					x: 0,
					y: 0
				},
				
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_dark,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameWorld_chest_title,
					
					x: 160,
					y: 8
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_dark,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameWorld_chest_inventory,
					
					x: 160,
					y: 120
				}
			];
			
			const 
				playerSelectedPoint = game.realms.gameWorld.objectTypes[
					game.realms.gameWorld.playerObject.type
				].selectedPointOf(
					game.realms.gameWorld.playerObject,
					
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				),
				playerSelectedObject = game.realms.gameWorld.objects[[
					playerSelectedPoint.x,
					playerSelectedPoint.y
				]];
				
				if (!((
					playerSelectedObject ||
					{}
				).type in this.allowedObjects)) {
					game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
					
					return [];
				}
			
			game.realms.gameWorld.guiTypes._container.renderInventory(
				playerSelectedObject.inventory,
			layer, 92, 14);
			
			game.realms.gameWorld.guiTypes._container.renderInventory(
				game.realms.gameWorld.playerObject.inventory,
			layer, 92, 126);
			
			layer.push({
				type: "sprite",
				
				texture: game.textures.gui_itemSelector,
				
				width0: 16,
				height0: 16,
				
				x: 92 + this.data.selectedItem.x * 17,
				y: [
					14,
					126
				][this.data.mode] + this.data.selectedItem.y * 17,
				
				width: 16,
				height: 16
			});
			
			return layer;
		}
};
