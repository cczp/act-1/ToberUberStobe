game.realms.gameWorld.guiTypes.oven = {
	//Secondary variables
		data: {
			mode: 0,
			selectedItem: {
				x: 0,
				y: 0
			}
		},
		
		allowedObjects: {
			oven: null
		},
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				if (
					game.input.mouse.moved[i][1] >= 54 &&
					game.input.mouse.moved[i][1] < 72
				) {
					if (
						game.input.mouse.moved[i][0] >= 109 &&
						game.input.mouse.moved[i][0] < 127
					) {
						const selectedItem = {
							x: 0,
							y: 0
						};
						
						if (
							this.data.mode != 0 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 0;
							this.data.selectedItem = selectedItem;
						}
					} else if (
						game.input.mouse.moved[i][0] >= 143 &&
						game.input.mouse.moved[i][0] < 161
					) {
						const selectedItem = {
							x: 1,
							y: 0
						};
						
						if (
							this.data.mode != 0 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 0;
							this.data.selectedItem = selectedItem;
						}
					} else if (
						game.input.mouse.moved[i][0] >= 194 &&
						game.input.mouse.moved[i][0] < 212
					) {
						const selectedItem = {
							x: 2,
							y: 0
						};
						
						if (
							this.data.mode != 0 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 0;
							this.data.selectedItem = selectedItem;
						}
					}
				} else if (
					game.input.mouse.moved[i][0] >= 92 &&
					game.input.mouse.moved[i][0] < 228 &&
					game.input.mouse.moved[i][1] >= 98 &&
					game.input.mouse.moved[i][1] < 200
				) {
					const selectedItem = {
						x: Math.floor((game.input.mouse.moved[i][0] - 92) / 17),
						y: Math.floor((game.input.mouse.moved[i][1] - 98) / 17)
					};
					
					if (
						this.data.mode != 1 ||
						JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
					) {
						game.sounds.gui_scroll.play();
						
						this.data.mode = 1;
						this.data.selectedItem = selectedItem;
					}
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					(
						game.input.mouse.released[i][1] >= 54 &&
						game.input.mouse.released[i][1] < 72 && (
							game.input.mouse.released[i][0] >= 109 &&
							game.input.mouse.released[i][0] < 127 ||
							
							game.input.mouse.released[i][0] >= 143 &&
							game.input.mouse.released[i][0] < 161 ||
							
							game.input.mouse.released[i][0] >= 194 &&
							game.input.mouse.released[i][0] < 212
						)
					) || (
						game.input.mouse.released[i][0] >= 92 &&
						game.input.mouse.released[i][0] < 228 &&
						game.input.mouse.released[i][1] >= 98 &&
						game.input.mouse.released[i][1] < 200
					)
				) {
					game.input.keyboard.pressed.push(game.config.list.main.controls.use);
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						const
							playerSelectedPoint = game.realms.gameWorld.objectTypes[
								game.realms.gameWorld.playerObject.type
							].selectedPointOf(
								game.realms.gameWorld.playerObject,
								
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							),
							playerSelectedObject = game.realms.gameWorld.objects[[
								playerSelectedPoint.x,
								playerSelectedPoint.y
							]];
							
						if (!((
							playerSelectedObject ||
							{}
						).type in this.allowedObjects)) {
							game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
							
							break;
						}
						
						const ovenInventory = playerSelectedObject.inventory;
						
						switch (this.data.mode) {
							case 0:
								game.realms.gameWorld.objectTypes[
									game.realms.gameWorld.playerObject.type
								].catchItems(
									game.realms.gameWorld.playerObject,
									
									[ovenInventory[
										this.data.selectedItem.y * 8 + this.data.selectedItem.x
									]]
								);
								
								ovenInventory[
									this.data.selectedItem.y * 8 + this.data.selectedItem.x
								] = null;
								
								break;
							case 1:
								game.realms.gameWorld.objectTypes[
									playerSelectedObject.type
								].catchItems(
									playerSelectedObject,
									
									[game.realms.gameWorld.playerObject.inventory[
										this.data.selectedItem.y * 8 + this.data.selectedItem.x
									]]
								);
								
								game.realms.gameWorld.playerObject.inventory[
									this.data.selectedItem.y * 8 + this.data.selectedItem.x
								] = null;
								
								break;
						}
						
						break;
					case game.config.list.main.controls.switch:
						game.sounds.gui_click.play();
						
						this.data.mode = [1, 0][this.data.mode];
						this.data.selectedItem.x = 0;
						this.data.selectedItem.y = 0;
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (
							this.data.mode == 1 &&
							this.data.selectedItem.y < 5
						) {
							this.data.selectedItem.y++;
						}
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x > 0) {
							this.data.selectedItem.x--;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (
							this.data.mode == 1 &&
							this.data.selectedItem.y > 0
						) {
							this.data.selectedItem.y--;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x < [
							2,
							7
						][this.data.mode]) {
							this.data.selectedItem.x++;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			const 
				playerSelectedPoint = game.realms.gameWorld.objectTypes[
					game.realms.gameWorld.playerObject.type
				].selectedPointOf(
					game.realms.gameWorld.playerObject,
					
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				),
				playerSelectedObject = game.realms.gameWorld.objects[[
					playerSelectedPoint.x,
					playerSelectedPoint.y
				]];
			
				if (!((
					playerSelectedObject ||
					{}
				).type in this.allowedObjects)) {
					game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
					
					return [];
				}
			
			const ovenInventory = playerSelectedObject.inventory;
			
			//Try to cook ingridient
				if (ovenInventory[0] && ovenInventory[1]) {
					if (
						ovenInventory[0].type in
							game.realms.gameWorld.cookingRecipes._fuelTypes
					) {
						for (let i in game.realms.gameWorld.cookingRecipes) {
							if (
								ovenInventory[1].type ==
									game.realms.gameWorld.cookingRecipes[i].in
							) {
								ovenInventory[0] = null;
								ovenInventory[1] = null;
								ovenInventory[2] = new game.realms.gameWorld.cookingRecipes[
									i
								].Out();
								
								break;
							}
						}
					}
				}
			
			const layer = [
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_oven,
					
					x: 0,
					y: 0
				},
				
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_dark,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameWorld_oven_title,
					
					x: 160,
					y: 32
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_dark,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameWorld_oven_inventory,
					
					x: 160,
					y: 93
				}
			];
			
			//Render oven inventory
				for (let y = 0; y < 6; y++) {
					for (let x = 0; x < 8; x++) {
						const currentItem = ovenInventory[
							y * 8 + x
						];
						
						if (currentItem) {
							layer.push({
								type: "sprite",
								
								texture: game.realms.gameWorld.itemTypes[
									currentItem.type
								].textureOf(currentItem),
								
								x: [
									110,
									144,
									195
								][y * 8 + x],
								y: 55 + y * 17
							});
						}
					}
				}
			
			game.realms.gameWorld.guiTypes._container.renderInventory(
				game.realms.gameWorld.playerObject.inventory,
			layer, 93, 99);
			
			layer.push({
				type: "sprite",
				
				texture: game.textures.gui_itemSelector,
				
				width0: 16,
				height0: 16,
				
				x: [
					[
						110,
						144,
						195
					][this.data.selectedItem.y * 8 + this.data.selectedItem.x],
					93 + this.data.selectedItem.x * 17
				][this.data.mode],
				y: [
					55,
					99 + this.data.selectedItem.y * 17
				][this.data.mode],
				
				width: 16,
				height: 16
			});
			
			return layer;
		}
};
