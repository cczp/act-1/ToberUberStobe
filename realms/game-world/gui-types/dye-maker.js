game.realms.gameWorld.guiTypes.dyeMaker = {
	//Secondary variables
		data: {
			selectedColor: 0
		},
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][1] >= 62 &&
					game.input.mouse.released[i][1] < 178
				) {
					if (
						game.input.mouse.released[i][0] >= 82 &&
						game.input.mouse.released[i][0] < 160
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.left);
					} else if (
						game.input.mouse.released[i][0] >= 160 &&
						game.input.mouse.released[i][0] < 238
					) {
						game.input.keyboard.pressed.push(game.config.list.main.controls.right);
					}
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.objectTypes[
							game.realms.gameWorld.playerObject.type
						].catchItems(
							game.realms.gameWorld.playerObject,
							
							[
								new game.realms.gameWorld.itemTypes[
									"dye_" +
									game.realms.gameWorld.terracottaColors[this.data.selectedColor]
								].New()
							]
						);
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedColor > 0) {
							this.data.selectedColor--;
						} else {
							this.data.selectedColor = game.realms.gameWorld.terracottaColors.length - 1;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedColor < game.realms.gameWorld.terracottaColors.length - 1) {
							this.data.selectedColor++;
						} else {
							this.data.selectedColor = 0;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			return [
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_dyeMaker,
					
					width0: 320,
					
					x: 0,
					y: 0,
					
					width: 320
				},
				
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_dyeMaker_colors,
					
					x0: 156 * this.data.selectedColor,
					
					width0: 156,
					
					x: 82,
					y: 62,
					
					width: 156
				},
				
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_dyeMaker,
					
					x0: 320,
					
					width0: 320,
					
					x: 0,
					y: 0,
					
					width: 320
				}
			];
		}
};
