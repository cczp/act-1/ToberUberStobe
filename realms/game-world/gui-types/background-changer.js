game.realms.gameWorld.guiTypes.backgroundChanger = {
	//Secondary variables
		data: {
			selectedPage: 0,
			selectedBackgroundTexture: {
				x: 0,
				y: 0
			}
		},
		
		get allBackgroundTextures() {
			return [null].concat(
				Object.values(
					game.realms.gameWorld.backgroundTextures
				).map(function(currentBackgroundTexture) {
					return currentBackgroundTexture;
				})
			);
		},
		
		allowedObjects: {
			backgroundChanger: null
		},
	
	//Methods
		handleInput: function() {
			const allBackgroundTextures = this.allBackgroundTextures;
			
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				if (
					game.input.mouse.moved[i][0] >= 91 &&
					game.input.mouse.moved[i][0] < 227 &&
					game.input.mouse.moved[i][1] >= 69 &&
					game.input.mouse.moved[i][1] < 171
				) {
					const selectedBackgroundTexture = {
						x: Math.floor((game.input.mouse.moved[i][0] - 91) / 17),
						y: Math.floor((game.input.mouse.moved[i][1] - 69) / 17)
					};
					
					if (JSON.stringify(selectedBackgroundTexture) != JSON.stringify(this.data.selectedBackgroundTexture)) {
						game.sounds.gui_scroll.play();
						
						this.data.selectedBackgroundTexture = selectedBackgroundTexture;
					}
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][0] >= 91 &&
					game.input.mouse.released[i][0] < 227 &&
					game.input.mouse.released[i][1] >= 69 &&
					game.input.mouse.released[i][1] < 171
				) {
					game.input.keyboard.pressed.push(game.config.list.main.controls.use);
				} else if (
					game.input.mouse.released[i][0] >= 80 &&
					game.input.mouse.released[i][0] < 240
				) {
					if (
						game.input.mouse.released[i][1] >= 171 &&
						game.input.mouse.released[i][1] < 180
					) {
						this.data.selectedBackgroundTexture.y = 5;
						
						game.input.keyboard.pressed.push(game.config.list.main.controls.down);
					} else if (
						game.input.mouse.released[i][1] >= 60 &&
						game.input.mouse.released[i][1] < 69
					) {
						this.data.selectedBackgroundTexture.y = 0;
						
						game.input.keyboard.pressed.push(game.config.list.main.controls.up);
					}
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						const 
							playerSelectedPoint = game.realms.gameWorld.objectTypes[
								game.realms.gameWorld.playerObject.type
							].selectedPointOf(
								game.realms.gameWorld.playerObject,
								
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							),
							playerSelectedObject = game.realms.gameWorld.objects[[
								playerSelectedPoint.x,
								playerSelectedPoint.y
							]];
						
						if (!((
							playerSelectedObject ||
							{}
						).type in this.allowedObjects)) {
							game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
							
							break;
						}
						
						if (allBackgroundTextures[
							this.data.selectedPage * 48 +
							this.data.selectedBackgroundTexture.y * 8 +
							this.data.selectedBackgroundTexture.x
						]) {
							game.realms.gameWorld.background[[
								playerSelectedPoint.x,
								playerSelectedPoint.y
							]] = Object.keys(
								game.realms.gameWorld.backgroundTextures
							)[Object.values(
								game.realms.gameWorld.backgroundTextures
							).indexOf(allBackgroundTextures[
								this.data.selectedPage * 48 +
								this.data.selectedBackgroundTexture.y * 8 +
								this.data.selectedBackgroundTexture.x
							])];
						} else {
							game.realms.gameWorld.background[[
								playerSelectedPoint.x,
								playerSelectedPoint.y
							]] = null;
						}
						
						game.realms.gameWorld.objects[[
							playerSelectedPoint.x,
							playerSelectedPoint.y
						]] = null;
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedBackgroundTexture.y < 5) {
							this.data.selectedBackgroundTexture.y++;
						} else if ((this.data.selectedPage + 1) < Math.ceil(
								allBackgroundTextures.length / 48
						)) {
							this.data.selectedPage++;
							
							this.data.selectedBackgroundTexture.y = 0;
						}
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedBackgroundTexture.x > 0) {
							this.data.selectedBackgroundTexture.x--;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedBackgroundTexture.y > 0) {
							this.data.selectedBackgroundTexture.y--;
						} else if (this.data.selectedPage > 0) {
							this.data.selectedPage--;
							
							this.data.selectedBackgroundTexture.y = 5;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedBackgroundTexture.x < 7) {
							this.data.selectedBackgroundTexture.x++;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			const 
				playerSelectedPoint = game.realms.gameWorld.objectTypes[
					game.realms.gameWorld.playerObject.type
				].selectedPointOf(
					game.realms.gameWorld.playerObject,
					
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				),
				playerSelectedObject = game.realms.gameWorld.objects[[
					playerSelectedPoint.x,
					playerSelectedPoint.y
				]];
			
			if (!((
				playerSelectedObject ||
				{}
			).type in this.allowedObjects)) {
				game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
				
				return [];
			}
			
			const
				allBackgroundTextures = this.allBackgroundTextures,
				
				layer = [
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_magicBox,
						
						x: 0,
						y: 0
					},
					
					{
						type: "text",
						
						fontSize: 4,
						fontColor: game.colorScheme.text_dark,
						textAlign: "center",
						
						string: game.currentLocale.gui_gameWorld_backgroundChanger_title,
						
						x: 160,
						y: 64
					},
					
					{
						type: "text",
						
						fontSize: 4,
						fontColor: game.colorScheme.text_dark,
						textAlign: "right",
						
						string:
							game.currentLocale.gui_page +
							(this.data.selectedPage + 1) + "/" +
							Math.ceil(allBackgroundTextures.length / 48),
						
						x: 237,
						y: 64
					}
				];
			
			game.realms.gameWorld.guiTypes._container.renderInventory_backgroundTexture(
				allBackgroundTextures.slice(
					this.data.selectedPage * 48,
					(this.data.selectedPage + 1) * 48
				),
			layer, 92, 70);
			
			layer.push({
				type: "sprite",
				
				texture: game.textures.gui_itemSelector,
				
				width0: 16,
				height0: 16,
				
				x: 92 + this.data.selectedBackgroundTexture.x * 17,
				y: 70 + this.data.selectedBackgroundTexture.y * 17,
				
				width: 16,
				height: 16
			});
			
			return layer;
		}
};
