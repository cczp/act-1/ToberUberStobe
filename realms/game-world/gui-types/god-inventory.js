game.realms.gameWorld.guiTypes.godInventory = {
	//Secondary variables
		data: {
			mode: 0,
			selectedPage: 0,
			selectedItem: {
				x: 0,
				y: 0
			},
			selectedControl: -1
		},
		
		get allItemsPrototypes() {
			return [null].concat(
				Object.values(
					game.realms.gameWorld.itemTypes
				).map(function(currentItemType) {
					return currentItemType.prototype;
				})
			);
		},
		
		controls: [
			{
				type: "squareButton",
				
				caption: "DL",
				
				x: 292,
				y: 212
			}
		],
	
	//Methods
		handleInput: function() {
			const allItemsPrototypes = this.allItemsPrototypes;
			
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				if (
					game.input.mouse.moved[i][0] >= 91 &&
					game.input.mouse.moved[i][0] < 227
				) {
					if (
						game.input.mouse.moved[i][1] >= 13 &&
						game.input.mouse.moved[i][1] < 115
					) {
						const selectedItem = {
							x: Math.floor((game.input.mouse.moved[i][0] - 91) / 17),
							y: Math.floor((game.input.mouse.moved[i][1] - 13) / 17)
						};
						
						if (
							this.data.mode != 0 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 0;
							this.data.selectedItem = selectedItem;
						}
					} else if (
						game.input.mouse.moved[i][1] >= 125 &&
						game.input.mouse.moved[i][1] < 227
					) {
						const selectedItem = {
							x: Math.floor((game.input.mouse.moved[i][0] - 91) / 17),
							y: Math.floor((game.input.mouse.moved[i][1] - 125) / 17)
						};
						
						if (
							this.data.mode != 1 ||
							JSON.stringify(selectedItem) != JSON.stringify(this.data.selectedItem)
						) {
							game.sounds.gui_scroll.play();
							
							this.data.mode = 1;
							this.data.selectedItem = selectedItem;
						}
					}
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				if (
					game.input.mouse.released[i][0] >= 91 &&
					game.input.mouse.released[i][0] < 227 && (
						game.input.mouse.released[i][1] >= 13 &&
						game.input.mouse.released[i][1] < 115 ||
						
						game.input.mouse.released[i][1] >= 125 &&
						game.input.mouse.released[i][1] < 227
					)
				) {
					switch (this.data.selectedControl) {
						case 0:
							game.input.keyboard.pressed.push(game.config.list.main.controls.magic);
							
							break;
						default:
							game.input.keyboard.pressed.push(game.config.list.main.controls.use);
							
							break;
					}
				} else if (
					game.input.mouse.released[i][0] >= 80 &&
					game.input.mouse.released[i][0] < 240
				) {
					if (
						game.input.mouse.released[i][1] >= 115 &&
						game.input.mouse.released[i][1] < 125
					) {
						this.data.mode = 0;
						this.data.selectedItem.y = 5;
						
						game.input.keyboard.pressed.push(game.config.list.main.controls.down);
					} else if (
						game.input.mouse.released[i][1] >= 4 &&
						game.input.mouse.released[i][1] < 13
					) {
						this.data.mode = 0;
						this.data.selectedItem.y = 0;
						
						game.input.keyboard.pressed.push(game.config.list.main.controls.up);
					}
				}
				
				//Handle control selection
					const selectedControl = game.guiControls.checkMouseSelectionOf(
						this.controls,
						
						game.input.mouse.released[i][0],
						game.input.mouse.released[i][1]
					);
					
					if (this.data.selectedControl != selectedControl) {
						game.sounds.gui_scroll.play();
						
						this.data.selectedControl = selectedControl;
					}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						switch (this.data.mode) {
							case 0:
								if (allItemsPrototypes[
									this.data.selectedPage * 48 +
									this.data.selectedItem.y * 8 +
									this.data.selectedItem.x
								]) {
									game.realms.gameWorld.objectTypes[
										game.realms.gameWorld.playerObject.type
									].catchItems(
										game.realms.gameWorld.playerObject,
										
										[
											new game.realms.gameWorld.itemTypes[
												allItemsPrototypes[
													this.data.selectedPage * 48 +
													this.data.selectedItem.y * 8 +
													this.data.selectedItem.x
												].type
											].New()
										]
									);
								} else {
									game.sounds.entities_itemCatching.play();
								}
								
								break;
							case 1:
								game.sounds.gui_click.play();
								
								[
									game.realms.gameWorld.playerObject.inventory[0],
									game.realms.gameWorld.playerObject.inventory[
										this.data.selectedItem.y * 8 + this.data.selectedItem.x
									]
								] = [
									game.realms.gameWorld.playerObject.inventory[
										this.data.selectedItem.y * 8 + this.data.selectedItem.x
									],
									game.realms.gameWorld.playerObject.inventory[0]
								];
								
								break;
						}
						
						break;
					case game.config.list.main.controls.switch:
						game.sounds.gui_click.play();
						
						this.data.mode = [1, 0][this.data.mode];
						this.data.selectedItem.x = 0;
						this.data.selectedItem.y = 0;
						
						break;
					case game.config.list.main.controls.magic:
						if (this.data.mode == 1) {
							game.sounds.entities_itemCatching.play();
							
							game.realms.gameWorld.playerObject.inventory[
								this.data.selectedItem.y * 8 + this.data.selectedItem.x
							] = null;
						}
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.y < 5) {
							this.data.selectedItem.y++;
						} else if (
							this.data.mode == 0 &&
							(this.data.selectedPage + 1) < Math.ceil(
								allItemsPrototypes.length / 48
							)
						) {
							this.data.selectedPage++;
							
							this.data.selectedItem.y = 0;
						}
						
						break;
					case game.config.list.main.controls.left:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x > 0) {
							this.data.selectedItem.x--;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.y > 0) {
							this.data.selectedItem.y--;
						} else if (
							this.data.mode == 0 &&
							this.data.selectedPage > 0
						) {
							this.data.selectedPage--;
							
							this.data.selectedItem.y = 5;
						}
						
						break;
					case game.config.list.main.controls.right:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedItem.x < 7) {
							this.data.selectedItem.x++;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			const
				allItemsPrototypes = this.allItemsPrototypes,
				
				layer = [
					{
						type: "sprite",
						
						texture: game.textures.gui_gameWorld_godInventory,
						
						x: 0,
						y: 0
					},
					
					{
						type: "text",
						
						fontSize: 4,
						fontColor: game.colorScheme.text_dark,
						textAlign: "center",
						
						string: game.currentLocale.gui_gameWorld_godInventory_allItems,
						
						x: 160,
						y: 8
					},
					{
						type: "text",
						
						fontSize: 4,
						fontColor: game.colorScheme.text_dark,
						textAlign: "center",
						
						string: game.currentLocale.gui_gameWorld_godInventory_title,
						
						x: 160,
						y: 120
					},
					
					{
						type: "text",
						
						fontSize: 4,
						fontColor: game.colorScheme.text_dark,
						textAlign: "right",
						
						string:
							game.currentLocale.gui_page +
							(this.data.selectedPage + 1) + "/" +
							Math.ceil(allItemsPrototypes.length / 48),
						
						x: 237,
						y: 8
					}
				];
			
			game.realms.gameWorld.guiTypes._container.renderInventory(
				allItemsPrototypes.slice(
					this.data.selectedPage * 48,
					(this.data.selectedPage + 1) * 48
				),
			layer, 92, 14);
			
			game.realms.gameWorld.guiTypes._container.renderInventory(
				game.realms.gameWorld.playerObject.inventory,
			layer, 92, 126);
			
			layer.push({
				type: "sprite",
				
				texture: game.textures.gui_itemSelector,
				
				width0: 16,
				height0: 16,
				
				x: 92 + this.data.selectedItem.x * 17,
				y: [
					14,
					126
				][this.data.mode] + this.data.selectedItem.y * 17,
				
				width: 16,
				height: 16
			});
			
			return layer.concat(game.guiControls.render(
				this.controls,
			this.data.selectedControl));
		}
};
