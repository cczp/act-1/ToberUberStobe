game.realms.gameWorld.guiTypes.pause = {
	//Secondary variables
		data: {
			selectedControl: 0
		},
		
		controls: [
			{
				type: "button",
				
				get caption() {
					return game.currentLocale.gui_gameWorld_pause_resume;
				},
				
				x: 100,
				y: 92,
				
				event: function() {
					game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
				}
			},
			
			{
				type: "button",
				
				get caption() {
					return game.currentLocale.gui_gameWorld_pause_exit;
				},
				
				x: 100,
				y: 120,
				
				event: function() {
					game.input.keyboard.mode = -game.input.keyboard.mode - 1;
					game.input.mouse.mode = -game.input.mouse.mode - 1;
					
					game.loadingScreen.hidden = false;
					
					game.config.write("gameWorld").then(function() {
						game.input.keyboard.mode = -game.input.keyboard.mode - 1;
						game.input.mouse.mode = -game.input.mouse.mode - 1;
						
						game.loadingScreen.hidden = true;
						
						game.currentRealm = game.realms.menu;
					});
				}
			}
		],
	
	//Methods
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				const selectedControl = game.guiControls.checkMouseSelectionOf(
					this.controls,
					
					game.input.mouse.moved[i][0],
					game.input.mouse.moved[i][1]
				);
				
				if (selectedControl >= 0 && this.data.selectedControl != selectedControl) {
					game.sounds.gui_scroll.play();
					
					this.data.selectedControl = selectedControl;
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				const pressedControl = game.guiControls.checkMouseSelectionOf(
					this.controls,
					
					game.input.mouse.released[i][0],
					game.input.mouse.released[i][1]
				);
				
				if (pressedControl >= 0) {
					game.sounds.gui_click.play();
					
					(this.controls[pressedControl].event || function() {})();
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
						
						break;
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						(
							this.controls[this.data.selectedControl].event ||
							function() {}
						)();
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedControl < this.controls.length - 1) {
							this.data.selectedControl++;
						} else {
							this.data.selectedControl = 0;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.data.selectedControl > 0) {
							this.data.selectedControl--;
						} else {
							this.data.selectedControl = this.controls.length - 1;
						}
						
						break;
				}
			}
		},
		
		render: function() {
			return([
				{
					type: "sprite",
					
					texture: game.textures.gui_gameWorld_pause,
					
					x: 0,
					y: 0
				},
				
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameWorld_pause_title,
					
					x: 160,
					y: 44
				}
			]).concat(game.guiControls.render(
				this.controls,
			this.data.selectedControl));
		}
};
