game.realms.gameWorld.guiTypes.door = {
	//Secondary variables
		data: {},
	
	//Methods
		handleInput: function() {},
		
		render: function() {
			switch (game.realms.gameWorld.playerObject.rotation) {
				case 0:
					if (
						!game.realms.gameWorld.objects[[
							game.config.list.gameWorld.playerPosition.x,
							game.config.list.gameWorld.playerPosition.y + 2
						]]
					) {
						game.sounds.blocks_door_closing.play();
						
						[
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y + 2
							]]
						] = [
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y + 2
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]]
						];
						
						game.config.list.gameWorld.cameraPosition.y += 2;
						game.config.list.gameWorld.playerPosition.y += 2;
					}
					
					break;
				case 1:
					if (
						!game.realms.gameWorld.objects[[
							game.config.list.gameWorld.playerPosition.x - 2,
							game.config.list.gameWorld.playerPosition.y
						]]
					) {
						game.sounds.blocks_door_closing.play();
						
						[
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x - 2,
								game.config.list.gameWorld.playerPosition.y
							]]
						] = [
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x - 2,
								game.config.list.gameWorld.playerPosition.y
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]]
						];
						
						game.config.list.gameWorld.cameraPosition.x -= 2;
						game.config.list.gameWorld.playerPosition.x -= 2;
					}
					
					break;
				case 2:
					if (
						!game.realms.gameWorld.objects[[
							game.config.list.gameWorld.playerPosition.x,
							game.config.list.gameWorld.playerPosition.y - 2
						]]
					) {
						game.sounds.blocks_door_closing.play();
						
						[
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y - 2
							]]
						] = [
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y - 2
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]
						]];
						
						game.config.list.gameWorld.cameraPosition.y -= 2;
						game.config.list.gameWorld.playerPosition.y -= 2;
					}
					
					break;
				case 3:
					if (
						!game.realms.gameWorld.objects[[
							game.config.list.gameWorld.playerPosition.x + 2,
							game.config.list.gameWorld.playerPosition.y
						]]
					) {
						game.sounds.blocks_door_closing.play();
						
						[
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x + 2,
								game.config.list.gameWorld.playerPosition.y
							]]
						] = [
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x + 2,
								game.config.list.gameWorld.playerPosition.y
							]],
							game.realms.gameWorld.objects[[
								game.config.list.gameWorld.playerPosition.x,
								game.config.list.gameWorld.playerPosition.y
							]]
						];
						
						game.config.list.gameWorld.cameraPosition.x += 2;
						game.config.list.gameWorld.playerPosition.x += 2;
					}
					
					break;
			}
			
			game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
			
			return [];
		}
};
