for (let i = 0; i < 2; i++) {
	game.realms.gameWorld.backgroundTextures["parquet" + [
		"",
		"_" + i
	][+(i != 0)]] = function(x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.background_parquet,
			
			x0: 16 * i,
			
			width0: 16,
			height0: 16,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	}
}
