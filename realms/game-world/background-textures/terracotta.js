for (let i of game.realms.gameWorld.terracottaColors) {
	game.realms.gameWorld.backgroundTextures[
		"terracotta_" + i
	] = function(x, y) {
		return {
			type: "sprite",
			
			texture: game.textures.background_terracotta,
			
			x0: 16 * game.realms.gameWorld.terracottaColors.indexOf(
				i
			),
			
			width0: 16,
			height0: 16,
			
			x: x,
			y: y,
			
			width: 16,
			height: 16
		};
	}
}
