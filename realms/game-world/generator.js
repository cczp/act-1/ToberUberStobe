game.realms.gameWorld.generator = {
	//Secondary variables
		
	
	//Methods
		generateSpawnCluster: function() {
			game.config.list.gameWorld.cameraPosition.x = 0;
			game.config.list.gameWorld.cameraPosition.y = 0;
			
			game.config.list.gameWorld.playerPosition.x = game.realms.gameWorld.playerOffset.x;
			game.config.list.gameWorld.playerPosition.y = game.realms.gameWorld.playerOffset.y;
			
			game.realms.gameWorld.playerObject = new game.realms.gameWorld.objectTypes.player.New();
		},
		
		initCluster: function(x, y) {
			const cluster = game.config.list.gameWorld.clusters[[x, y]] = {
				type: "",
				
				background: [],
				objects: [],
				particles: []
			};
			
			for (
				let x1 = 0;
				x1 < game.config.list.gameWorld.clusterSize;
				x1++
			) {
				[
					cluster.background[x1],
					cluster.objects[x1],
					cluster.particles[x1]
				] = [
					[],
					[],
					[]
				];
				
				for (
					let y1 = 0;
					y1 < game.config.list.gameWorld.clusterSize;
					y1++
				) {
					[
						cluster.background[x1][y1],
						cluster.objects[x1][y1],
						cluster.particles[x1][y1]
					] = [
						null,
						null,
						null
					];
				}
			}
			
			return cluster;
		},
		fillCluster: function(x, y) {
			const cluster = game.config.list.gameWorld.clusters[[x, y]];
			
			//Choose biome
				if (!game.config.list.gameWorld.singleBiome) {
					for (let i in game.realms.gameWorld.biomes) {
						if (Math.random() < game.realms.gameWorld.biomes[i].chance) {
							cluster.biome = i;
							
							break;
						}
						cluster.biome = "desert";
					}
				} else {
					cluster.biome = game.config.list.gameWorld.singleBiome;
				}
			
			const biome = game.realms.gameWorld.biomes[
				cluster.biome
			] || game.realms.gameWorld.biomes.void;
			
			//Generate background
				for (
					let x1 = 0;
					x1 < game.config.list.gameWorld.clusterSize;
					x1++
				) {
					for (
						let y1 = 0;
						y1 < game.config.list.gameWorld.clusterSize;
						y1++
					) {
						if (cluster.background[x1][y1]) {
							continue;
						}
						
						cluster.background[x1][y1] = biome.backgroundTextures[
							Math.floor(
								Math.random() *
								biome.backgroundTextures.length
							)
						];
					}
				}
			
			//Generate objects
				for (
					let x1 = 0;
					x1 < game.config.list.gameWorld.clusterSize;
					x1++
				) {
					for (
						let y1 = 0;
						y1 < game.config.list.gameWorld.clusterSize;
						y1++
					) {
						if (cluster.objects[x1][y1]) {
							continue;
						}
						
						for (let i in biome.objectTypes) {
							if (Math.random() < biome.objectTypes[i].chance) {
								cluster.objects[x1][y1] = new biome.objectTypes[i].New();
								
								break;
							}
						}
					}
				}
		},
		
		spawnMobs: function() {
			const
				currentObjectPosition = {
					x:
						game.config.list.gameWorld.cameraPosition.x +
						
						Math.floor(
							Math.random() *
							game.realms.gameWorld.chunkSize.width
						),
					y:
						game.config.list.gameWorld.cameraPosition.y +
						
						Math.floor(
							Math.random() *
							game.realms.gameWorld.chunkSize.height
						),
				},
				
				clusterPosition = {
					x: Math.floor(currentObjectPosition.x / game.config.list.gameWorld.clusterSize),
					y: Math.floor(currentObjectPosition.y / game.config.list.gameWorld.clusterSize)
				};
			
			const spawningMobs = (game.realms.gameWorld.biomes[
				game.realms.gameWorld.clusters[[
					clusterPosition.x,
					clusterPosition.y
				]].biome
			] || game.realms.gameWorld.biomes.void).spawningMobs;
			
			if (!game.realms.gameWorld.objects[[
				currentObjectPosition.x,
				currentObjectPosition.y
			]]) {
				for (let i in spawningMobs) {
					if (Math.random() < spawningMobs[i].chance) {
						game.realms.gameWorld.objects[[
							currentObjectPosition.x,
							currentObjectPosition.y
						]] = new spawningMobs[i].New();
						
						break;
					}
				}
			}
		}
};
