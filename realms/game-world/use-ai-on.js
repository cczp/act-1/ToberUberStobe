game.realms.gameWorld.useAiOn = function(object, x, y) {
	const objectsAroundObject = [
		game.realms.gameWorld.objects[[x, y + 1]],
		game.realms.gameWorld.objects[[x - 1, y]],
		game.realms.gameWorld.objects[[x, y - 1]],
		game.realms.gameWorld.objects[[x + 1, y]]
	];
	
	//Walk around
		if (
			object.canWalkAround &&
			
			game.realms.gameWorld.tick % 10 == 0
		) {
			if (Math.random() < 0.25) {
				switch (Math.floor(Math.random() * 2)) {
					case 0:
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, [1, 2, 3, 0][object.rotation]);
						
						return [x, y];
					case 1:
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, [3, 0, 1, 2][object.rotation]);
						
						return [x, y];
				}
			}
			
			switch (object.rotation) {
				case 0:
					if (!objectsAroundObject[0]) {
						[
							game.realms.gameWorld.objects[[x, y]],
							game.realms.gameWorld.objects[[x, y + 1]]
						] = [
							game.realms.gameWorld.objects[[x, y + 1]],
							game.realms.gameWorld.objects[[x, y]]
						];
						
						y++;
						
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, object.rotation);
					} else {
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, [1, 2, 3, 0][object.rotation]);
					}
					
					break;
				case 1:
					if (!objectsAroundObject[1]) {
						[
							game.realms.gameWorld.objects[[x, y]],
							game.realms.gameWorld.objects[[x - 1, y]]
						] = [
							game.realms.gameWorld.objects[[x - 1, y]],
							game.realms.gameWorld.objects[[x, y]]
						];
						
						x--;
						
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, object.rotation);
					} else {
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, [1, 2, 3, 0][object.rotation]);
					}
					
					break;
				case 2:
					if (!objectsAroundObject[2]) {
						[
							game.realms.gameWorld.objects[[x, y]],
							game.realms.gameWorld.objects[[x, y - 1]]
						] = [
							game.realms.gameWorld.objects[[x, y - 1]],
							game.realms.gameWorld.objects[[x, y]]
						];
						
						y--;
						
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, object.rotation);
					} else {
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, [1, 2, 3, 0][object.rotation]);
					}
					
					break;
				case 3:
					if (!objectsAroundObject[3]) {
						[
							game.realms.gameWorld.objects[[x, y]],
							game.realms.gameWorld.objects[[x + 1, y]]
						] = [
							game.realms.gameWorld.objects[[x + 1, y]],
							game.realms.gameWorld.objects[[x, y]]
						];
						
						x++;
						
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, object.rotation);
					} else {
						game.realms.gameWorld.objectTypes[
							object.type
						].direct(object, [1, 2, 3, 0][object.rotation]);
					}
					
					break;
			}
		}
	
	//Go to player
		if (object.canWalk && !object.canWalkAround) {
			if (
				!objectsAroundObject[0] &&
				
				game.config.list.gameWorld.playerPosition.y > y
			) {
				[
					game.realms.gameWorld.objects[[x, y]],
					game.realms.gameWorld.objects[[x, y + 1]]
				] = [
					game.realms.gameWorld.objects[[x, y + 1]],
					game.realms.gameWorld.objects[[x, y]]
				];
				
				y++;
				
				game.realms.gameWorld.objectTypes[
					object.type
				].direct(object, 0);
			} else if (
				!objectsAroundObject[1] &&
				
				game.config.list.gameWorld.playerPosition.x < x
			) {
				[
					game.realms.gameWorld.objects[[x, y]],
					game.realms.gameWorld.objects[[x - 1, y]]
				] = [
					game.realms.gameWorld.objects[[x - 1, y]],
					game.realms.gameWorld.objects[[x, y]]
				];
				
				x--;
				
				game.realms.gameWorld.objectTypes[
					object.type
				].direct(object, 1);
			} else if (
				!objectsAroundObject[2] &&
				
				game.config.list.gameWorld.playerPosition.y < y
			) {
				[
					game.realms.gameWorld.objects[[x, y]],
					game.realms.gameWorld.objects[[x, y - 1]]
				] = [
					game.realms.gameWorld.objects[[x, y - 1]],
					game.realms.gameWorld.objects[[x, y]]
				];
				
				y--;
				
				game.realms.gameWorld.objectTypes[
					object.type
				].direct(object, 2);
			} else if (
				!objectsAroundObject[3] &&
				
				game.config.list.gameWorld.playerPosition.x > x
			) {
				[
					game.realms.gameWorld.objects[[x, y]],
					game.realms.gameWorld.objects[[x + 1, y]]
				] = [
					game.realms.gameWorld.objects[[x + 1, y]],
					game.realms.gameWorld.objects[[x, y]]
				];
				
				x++;
				
				game.realms.gameWorld.objectTypes[
					object.type
				].direct(object, 3);
			}
		}
	
	//Attack player
		if (object.canAttack && game.realms.gameWorld.tick % 5 == 0) {
			for (let i = 0; i < objectsAroundObject.length; i++) {
				if (objectsAroundObject[i] == game.realms.gameWorld.playerObject) {
					object.rotation = i;
					
					game.realms.gameWorld.objectTypes[
						object.type
					].attack(
						object,
						
						x, y
					);
					
					return [x, y];
				}
			}
		}
	
	return [x, y];
};
