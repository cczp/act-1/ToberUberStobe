game.realms.gameWorld.biomes.highGround = {
	backgroundTextures: [
		"stone",
		"stone_1",
		"stone_2",
		"stone_3"
	],
	objectTypes: {
		stone: {
			New: game.realms.gameWorld.objectTypes.stone.New,
			
			chance: 0.25
		},
		
		rock: {
			New: game.realms.gameWorld.objectTypes.rock.New,
			
			chance: 0.125
		},
		
		ironOre: {
			New: game.realms.gameWorld.objectTypes.ironOre.New,
			
			chance: 0.005
		},
		coal: {
			New: game.realms.gameWorld.objectTypes.coal.New,
			
			chance: 0.01
		}
	},
	
	spawningMobs: {
		fox: {
			New: game.realms.gameWorld.objectTypes.fox.New,
			
			chance: 0.01337
		},
		
		snake: {
			New: game.realms.gameWorld.objectTypes.snake.New,
			
			chance: 0.00666
		}
	},
	
	chance: 0.025
};
