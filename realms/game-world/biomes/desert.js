game.realms.gameWorld.biomes.desert = {
	backgroundTextures: [
		"sand",
		"sand_1",
		"sand_2",
		"sand_3",
		"sand_4",
		"sand_5",
		"sand_6",
		"sand_7"
	],
	objectTypes: {
		sand: {
			New: game.realms.gameWorld.objectTypes.sand.New,
			
			chance: 0.02
		},
		
		dryGrass: {
			New: game.realms.gameWorld.objectTypes.dryGrass.New,
			
			chance: 0.005
		},
		
		rock: {
			New: game.realms.gameWorld.objectTypes.rock.New,
			
			chance: 0.002
		},
		
		smallCactus: {
			New: game.realms.gameWorld.objectTypes.smallCactus.New,
			
			chance: 0.001
		},
		bigCactus: {
			New: game.realms.gameWorld.objectTypes.bigCactus.New,
			
			chance: 0.0005
		},
		
		haloxylon: {
			New: game.realms.gameWorld.objectTypes.haloxylon.New,
			
			chance: 0.00025
		},
	},
	
	spawningMobs: {
		cat: {
			New: game.realms.gameWorld.objectTypes.cat.New,
			
			chance: 0.003992
		},
		
		fox: {
			New: game.realms.gameWorld.objectTypes.fox.New,
			
			chance: 0.002674
		},
		
		snake: {
			New: game.realms.gameWorld.objectTypes.snake.New,
			
			chance: 0.000666
		},
		
		gazelle: {
			New: game.realms.gameWorld.objectTypes.gazelle.New,
			
			chance: 0.001332
		}
	},
	
	chance: 0
};
