game.realms.gameWorld.biomes.vegetableValley = {
	backgroundTextures: [
		"dirt",
		"dirt_1",
		"dirt_2",
		"dirt_3"
	],
	objectTypes: {
		dirt: {
			New: game.realms.gameWorld.objectTypes.dirt.New,
			
			chance: 0.015625
		},
		
		grass: {
			New: game.realms.gameWorld.objectTypes.grass.New,
			
			chance: 0.5
		},
		
		rock: {
			New: game.realms.gameWorld.objectTypes.rock.New,
			
			chance: 0.001
		},
		
		tomato: {
			New: game.realms.gameWorld.objectTypes.tomato.New,
			
			chance: 0.04
		},
		carrot: {
			New: game.realms.gameWorld.objectTypes.carrot.New,
			
			chance: 0.02
		},
		eggplant: {
			New: game.realms.gameWorld.objectTypes.eggplant.New,
			
			chance: 0.01
		}
	},
	
	spawningMobs: {
		cat: {
			New: game.realms.gameWorld.objectTypes.cat.New,
			
			chance: 0.01996
		}
	},
	
	chance: 0.0375
};
