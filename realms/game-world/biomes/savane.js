game.realms.gameWorld.biomes.savane = {
	backgroundTextures: [
		"dirt",
		"dirt_1",
		"dirt_2",
		"dirt_3"
	],
	objectTypes: {
		dirt: {
			New: game.realms.gameWorld.objectTypes.dirt.New,
			
			chance: 0.015625
		},
		
		acacia: {
			New: game.realms.gameWorld.objectTypes.acacia.New,
			
			chance: 0.0075
		},
		
		grass: {
			New: game.realms.gameWorld.objectTypes.grass.New,
			
			chance: 0.25
		},
		
		rock: {
			New: game.realms.gameWorld.objectTypes.rock.New,
			
			chance: 0.001
		},
		
		puddle: {
			New: game.realms.gameWorld.objectTypes.puddle.New,
			
			chance: 0.001875
		}
	},
	
	spawningMobs: {
		snake: {
			New: game.realms.gameWorld.objectTypes.snake.New,
			
			chance: 0.00666
		},
		
		gazelle: {
			New: function GameWorldObject() {
				return new game.realms.gameWorld.objectTypes.gazelle.New({
					canWalkAround: false,
					canWalk: true,
					canAttack: true
				});
			},
			
			chance: 0.00666
		}
	},
	
	chance: 0.05
};
