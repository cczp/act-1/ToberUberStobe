game.colorScheme = {
	//Text
		text_selected:
			"#E0B098",
		text_light:
			"#FFFFFF",
		text_dark:
			"#686050",
		text_good:
			"#308838",
		text_neutral:
			"#9020A8",
		text_bad:
			"#C82828",
		
		//Game world
			text_gameWorld_sign:
				"#404040",
	
	//Other
		get random() {
			function byte() {
				return (Math.floor(
					Math.random() * 256
				) >> 3 << 3).toString(16)
			}
			
			return "#" + byte() + byte() + byte();
		}
};
