game.realms.controls = {
	//Secondary variables
		
	
	//Methods
		update: function() {
			this.handleInput();
			
			return [[
				{
					type: "sprite",
					
					texture: game.textures.gui_controls,
					
					x: 0,
					y: 0
				},
				
				{
					type: "text",
					
					fontSize: 16,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Controls",
					
					x: 160,
					y: 14
				},
				
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "W",
					
					x: 80,
					y: 118
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "A",
					
					x: 66,
					y: 132
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "S",
					
					x: 80,
					y: 146
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "D",
					
					x: 94,
					y: 132
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "G",
					
					x: 137,
					y: 145
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "H",
					
					x: 166,
					y: 145
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "K",
					
					x: 212,
					y: 143
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "L",
					
					x: 245,
					y: 143
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "T",
					
					x: 128,
					y: 206
				},
				{
					type: "text",
					
					fontSize: 8,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Y",
					
					x: 193,
					y: 206
				},
				
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Move",
					
					x: 80,
					y: 108
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Switch",
					
					x: 137,
					y: 132
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Quit",
					
					x: 166,
					y: 132
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Use",
					
					x: 212,
					y: 129
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Run",
					
					x: 245,
					y: 129
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Command",
					
					x: 128,
					y: 192
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: "Magic",
					
					x: 193,
					y: 192
				},
				
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "left",
					
					string: "P.S. the back button of your browser/Android phone is also Quit",
					
					x: 2,
					y: 231
				},
				{
					type: "text",
					
					fontSize: 4,
					fontColor: game.colorScheme.text_light,
					textAlign: "left",
					
					string: "P.P.S. you can just your mouse/touchscreen",
					
					x: 2,
					y: 236
				}
			]];
		},
		
		handleInput: function() {
			const controls = Object.values(game.config.list.main.controls);
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				if (controls.includes(game.input.keyboard.pressed[i])) {
					game.sounds.gui_click.play();
					
					game.currentRealm = game.realms.menu;
					
					break;
				}
			}
		}
};
