game.realms.gameWorld = {
	//Secondary variables
		backgroundTextures: {},
		itemTypes: {},
		objectTypes: {},
		objectTypesYouMustNotTouch: {},
		particleTypes: {},
		guiTypes: {},
		biomes: {},
		craftingRecipes: {},
		cookingRecipes: {
			_fuelTypes: {}
		},
		
		renderingData: [],
		
		updatedAreas: [],
		
		lightingLevel: 0,
		
		//Config wrappers
			clusters: new Proxy({}, {
				get: function(object, selector) {
					const realSelector = selector.split(",").map(function(coordinate) {
						const realCoordinate = parseInt(coordinate);
						
						if (isFinite(realCoordinate)) {
							return realCoordinate;
						} else {
							return 0;
						}
					});
					
					if (!game.config.list.gameWorld.clusters[[
						realSelector[0],
						realSelector[1]
					]]) {
						game.realms.gameWorld.generator.initCluster(
							realSelector[0],
							realSelector[1]
						);
						game.realms.gameWorld.generator.fillCluster(
							realSelector[0],
							realSelector[1]
						);
					}
					
					return game.config.list.gameWorld.clusters[[
						realSelector[0],
						realSelector[1]
					]];
				},
				set: function(object, selector, value) {
					const realSelector = selector.split(",").map(function(coordinate) {
						const realCoordinate = parseInt(coordinate);
						
						if (isFinite(realCoordinate)) {
							return realCoordinate;
						} else {
							return 0;
						}
					});
					
					if (!game.config.list.gameWorld.clusters[[
						realSelector[0],
						realSelector[1]
					]]) {
						game.realms.gameWorld.generator.initCluster(
							realSelector[0],
							realSelector[1]
						);
						game.realms.gameWorld.generator.fillCluster(
							realSelector[0],
							realSelector[1]
						);
					}
					
					game.config.list.gameWorld.clusters[[
						realSelector[0],
						realSelector[1]
					]] = value;
					
					return true;
				}
			}),
			
			background: new Proxy({}, {
				get: function(object, selector) {
					const
						realSelector = selector.split(",").map(function(coordinate) {
							const realCoordinate = parseInt(coordinate);
							
							if (isFinite(realCoordinate)) {
								return realCoordinate;
							} else {
								return 0;
							}
						}),
						
						clusterPosition = {
							x: Math.floor(realSelector[0] / game.config.list.gameWorld.clusterSize),
							y: Math.floor(realSelector[1] / game.config.list.gameWorld.clusterSize)
						};
					
					return game.realms.gameWorld.clusters[[
						clusterPosition.x,
						clusterPosition.y
					]].background[
						realSelector[0] - clusterPosition.x * game.config.list.gameWorld.clusterSize
					][
						realSelector[1] - clusterPosition.y * game.config.list.gameWorld.clusterSize
					];
				},
				set: function(object, selector, value) {
					const
						realSelector = selector.split(",").map(function(coordinate) {
							const realCoordinate = parseInt(coordinate);
							
							if (isFinite(realCoordinate)) {
								return realCoordinate;
							} else {
								return 0;
							}
						}),
						
						clusterPosition = {
							x: Math.floor(realSelector[0] / game.config.list.gameWorld.clusterSize),
							y: Math.floor(realSelector[1] / game.config.list.gameWorld.clusterSize)
						};
					
					game.realms.gameWorld.clusters[[
						clusterPosition.x,
						clusterPosition.y
					]].background[
						realSelector[0] - clusterPosition.x * game.config.list.gameWorld.clusterSize
					][
						realSelector[1] - clusterPosition.y * game.config.list.gameWorld.clusterSize
					] = value;
					
					return true;
				}
			}),
			objects: new Proxy({}, {
				get: function(object, selector) {
					const
						realSelector = selector.split(",").map(function(coordinate) {
							const realCoordinate = parseInt(coordinate);
							
							if (isFinite(realCoordinate)) {
								return realCoordinate;
							} else {
								return 0;
							}
						}),
						
						clusterPosition = {
							x: Math.floor(realSelector[0] / game.config.list.gameWorld.clusterSize),
							y: Math.floor(realSelector[1] / game.config.list.gameWorld.clusterSize)
						};
					
					return game.realms.gameWorld.clusters[[
						clusterPosition.x,
						clusterPosition.y
					]].objects[
						realSelector[0] - clusterPosition.x * game.config.list.gameWorld.clusterSize
					][
						realSelector[1] - clusterPosition.y * game.config.list.gameWorld.clusterSize
					];
				},
				set: function(object, selector, value) {
					const
						realSelector = selector.split(",").map(function(coordinate) {
							const realCoordinate = parseInt(coordinate);
							
							if (isFinite(realCoordinate)) {
								return realCoordinate;
							} else {
								return 0;
							}
						}),
						
						clusterPosition = {
							x: Math.floor(realSelector[0] / game.config.list.gameWorld.clusterSize),
							y: Math.floor(realSelector[1] / game.config.list.gameWorld.clusterSize)
						};
					
					game.realms.gameWorld.clusters[[
						clusterPosition.x,
						clusterPosition.y
					]].objects[
						realSelector[0] - clusterPosition.x * game.config.list.gameWorld.clusterSize
					][
						realSelector[1] - clusterPosition.y * game.config.list.gameWorld.clusterSize
					] = value;
					
					return true;
				}
			}),
			particles: new Proxy({}, {
				get: function(object, selector) {
					const
						realSelector = selector.split(",").map(function(coordinate) {
							const realCoordinate = parseInt(coordinate);
							
							if (isFinite(realCoordinate)) {
								return realCoordinate;
							} else {
								return 0;
							}
						}),
						
						clusterPosition = {
							x: Math.floor(realSelector[0] / game.config.list.gameWorld.clusterSize),
							y: Math.floor(realSelector[1] / game.config.list.gameWorld.clusterSize)
						};
					
					return game.realms.gameWorld.clusters[[
						clusterPosition.x,
						clusterPosition.y
					]].particles[
						realSelector[0] - clusterPosition.x * game.config.list.gameWorld.clusterSize
					][
						realSelector[1] - clusterPosition.y * game.config.list.gameWorld.clusterSize
					];
				},
				set: function(object, selector, value) {
					const
						realSelector = selector.split(",").map(function(coordinate) {
							const realCoordinate = parseInt(coordinate);
							
							if (isFinite(realCoordinate)) {
								return realCoordinate;
							} else {
								return 0;
							}
						}),
						
						clusterPosition = {
							x: Math.floor(realSelector[0] / game.config.list.gameWorld.clusterSize),
							y: Math.floor(realSelector[1] / game.config.list.gameWorld.clusterSize)
						};
					
					game.realms.gameWorld.clusters[[
						clusterPosition.x,
						clusterPosition.y
					]].particles[
						realSelector[0] - clusterPosition.x * game.config.list.gameWorld.clusterSize
					][
						realSelector[1] - clusterPosition.y * game.config.list.gameWorld.clusterSize
					] = value;
					
					return true;
				}
			}),
			
			get playerObject() {
				return this.objects[[
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				]];
			},
			set playerObject(value) {
				this.objects[[
					game.config.list.gameWorld.playerPosition.x,
					game.config.list.gameWorld.playerPosition.y
				]] = value;
				
				return true;
			},
		
		playerOffset: {
			x: 10,
			y: 7
		},
		
		chunkSize: {
			width: 20,
			height: 15
		},
		
		terracottaColors: [
			"amber",
			"black",
			"blue",
			"blueGray",
			"brown",
			"cyan",
			"darkGray",
			"deepOrange",
			"deepPurple",
			"gray",
			"green",
			"indigo",
			"lightBlue",
			"lightGreen",
			"lime",
			"orange",
			"pink",
			"purple",
			"red",
			"teal",
			"white",
			"yellow"
		],
	
	//Methods
		update: function() {
			game.debugData.splice(2, 0,
				"Camera",
					"X: " + game.config.list.gameWorld.cameraPosition.x + ", " +
					"Y: " + game.config.list.gameWorld.cameraPosition.y +
					"    ",
				"Player",
					"X: " + game.config.list.gameWorld.playerPosition.x + ", " +
					"Y: " + game.config.list.gameWorld.playerPosition.y +
					"    "
			);
			
			this.currentGuiType.handleInput();
			
			return this.renderingData.concat([
				this.currentGuiType.render()
			]);
		},
		
		updateChunk: function() {
			for (let x = 0; x < this.chunkSize.width; x++) {
				for (let y = 0; y < this.chunkSize.height; y++) {
					const
						currentObject = this.objects[[
							game.config.list.gameWorld.cameraPosition.x + x,
							game.config.list.gameWorld.cameraPosition.y + y
						]],
						currentParticle = this.particles[[
							game.config.list.gameWorld.cameraPosition.x + x,
							game.config.list.gameWorld.cameraPosition.y + y
						]];
					
					if (currentObject) {
						if (!this.updatedAreas.includes(currentObject)) {
							const currentObjectType = this.objectTypes[currentObject.type];
							
							if (
								currentObject.type != "player" ||
								
								currentObject.type == "player" &&
								currentObject == this.playerObject
							) {
								if (currentObjectType.update) {
									currentObjectType.update(
										currentObject,
										
										game.config.list.gameWorld.cameraPosition.x + x,
										game.config.list.gameWorld.cameraPosition.y + y
									);
								}
								
								this.updatedAreas.push(currentObject);
							}
						}
					}
					if (currentParticle) {
						if (!this.updatedAreas.includes(currentParticle)) {
							const currentParticleType = this.particleTypes[currentParticle.type];
							
							if (currentParticleType.update) {
								currentParticleType.update(
									currentParticle,
									
									game.config.list.gameWorld.cameraPosition.x + x,
									game.config.list.gameWorld.cameraPosition.y + y
								);
							}
							
							this.updatedAreas.push(currentParticle);
						}
					}
				}
			}
			
			if (this.tick % 4 == 0) {
				this.generator.spawnMobs();
			}
			
			if (this.tick % 24 == 0) {
				const
					date = new Date(),
					
					hours = date.getHours() + date.getMinutes() / 60;
				
				switch (Math.floor(hours / 4)) {
					case 1:
						this.lightingLevel = hours / 4 - 1;
						
						break;
					case 2:
					case 3:
						this.lightingLevel = 1;
						
						break;
					case 4:
						this.lightingLevel = 1 - (hours / 4 - 4);
						
						break;
					default:
						this.lightingLevel = 0;
						
						break;
				}
			}
			
			this.renderingData = [
				this.renderBackground(),
				this.renderObjects(),
				this.renderParticles(),
				this.renderDarkness()
			];
		},
		
		renderBackground: function() {
			const layer = [];
			
			for (let y = 0; y < this.chunkSize.height; y++) {
				for (let x = 0; x < this.chunkSize.width; x++) {
					const currentBackgroundTexture = this.backgroundTextures[
						this.background[[
							game.config.list.gameWorld.cameraPosition.x + x,
							game.config.list.gameWorld.cameraPosition.y + y
						]]
					];
					
					if (!currentBackgroundTexture) {
						continue;
					}
					
					layer.push(currentBackgroundTexture(x * 16, y * 16));
				}
			}
			
			return layer;
		},
		renderObjects: function() {
			const layer = [];
			
			for (let y = 0; y < this.chunkSize.height; y++) {
				for (let x = 0; x < this.chunkSize.width; x++) {
					const currentObject = this.objects[[
						game.config.list.gameWorld.cameraPosition.x + x,
						game.config.list.gameWorld.cameraPosition.y + y
					]];
					
					if (!currentObject) {
						continue;
					}
					
					layer.push(this.objectTypes[currentObject.type].render(
						currentObject,
						
						x * 16,
						y * 16
					));
				}
			}
			
			return layer;
		},
		renderParticles: function() {
			const layer = [];
			
			for (let y = 0; y < this.chunkSize.height; y++) {
				for (let x = 0; x < this.chunkSize.width; x++) {
					const currentParticle = this.particles[[
						game.config.list.gameWorld.cameraPosition.x + x,
						game.config.list.gameWorld.cameraPosition.y + y
					]];
					
					if (!currentParticle) {
						continue;
					}
					
					layer.push(this.particleTypes[currentParticle.type].render(
						currentParticle,
						
						x * 16,
						y * 16
					));
				}
			}
			
			return layer;
		},
		renderDarkness: function() {
			return [{
				type: "sprite",
				
				texture: game.textures.gui_gameWorld_darkness,
				
				opacity: 1 - this.lightingLevel,
				
				x: 0,
				y: 0
			}];
		},
		
		fail: function() {
			if (game.config.list.gameWorld.version) {
				game.sounds.entities_death.play();
				
				delete game.config.list.gameWorld.version;
				
				game.input.keyboard.mode = -game.input.keyboard.mode - 1;
				game.input.mouse.mode = -game.input.mouse.mode - 1;
				
				game.loadingScreen.hidden = false;
				
				game.config.write("gameWorld").then(function() {
					game.input.keyboard.mode = -game.input.keyboard.mode - 1;
					game.input.mouse.mode = -game.input.mouse.mode - 1;
					
					game.loadingScreen.hidden = true;
				});
				
				game.currentRealm = game.realms.gameOver;
			}
		}
};
