game.realms.menu = {
	//Secondary variables
		controls: [
			{
				type: "button",
				
				get caption() {
					return game.currentLocale.gui_menu_play;
				},
				
				x: 180,
				y: 160,
				
				event: function() {
					if (!game.config.list.gameWorld.version) {
						game.config.list.gameWorld = JSON.parse(JSON.stringify(
							game.config.defaultList.gameWorld
						));
						
						game.config.list.gameWorld.version = game.version;
						
						game.realms.gameWorld.generator.generateSpawnCluster();
						
						game.input.keyboard.mode = -game.input.keyboard.mode - 1;
						game.input.mouse.mode = -game.input.mouse.mode - 1;
						
						game.loadingScreen.hidden = false;
						
						game.config.write("gameWorld").then(function() {
							game.input.keyboard.mode = -game.input.keyboard.mode - 1;
							game.input.mouse.mode = -game.input.mouse.mode - 1;
							
							game.loadingScreen.hidden = true;
							
							game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
							
							game.currentRealm = game.realms.gameWorld;
						});
					}
					
					game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
					
					game.currentRealm = game.realms.gameWorld;
				}
			},
			
			{
				type: "button",
				
				get caption() {
					return game.currentLocale.gui_menu_settings;
				},
				
				x: 180,
				y: 188,
				
				event: function() {
					game.currentRealm = game.realms.settings;
				}
			}
		],
		selectedControl: 0,
		
	//Methods
		update: function() {
			this.handleInput();
			
			return [
				this.renderBase(true),
				game.guiControls.render(this.controls, this.selectedControl)
			];
		},
		
		handleInput: function() {
			for (let i = 0; i < game.input.mouse.moved.length; i++) {
				const selectedControl = game.guiControls.checkMouseSelectionOf(
					this.controls,
					
					game.input.mouse.moved[i][0],
					game.input.mouse.moved[i][1]
				);
				
				if (selectedControl >= 0 && this.selectedControl != selectedControl) {
					game.sounds.gui_scroll.play();
					
					this.selectedControl = selectedControl;
				}
			}
			for (let i = 0; i < game.input.mouse.released.length; i++) {
				const pressedControl = game.guiControls.checkMouseSelectionOf(
					this.controls,
					
					game.input.mouse.released[i][0],
					game.input.mouse.released[i][1]
				);
				
				if (pressedControl >= 0) {
					game.sounds.gui_click.play();
					
					(this.controls[pressedControl].event || function() {})();
				}
			}
			
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.use:
						game.sounds.gui_click.play();
						
						(this.controls[this.selectedControl].event || function() {})();
						
						break;
					case game.config.list.main.controls.down:
						game.sounds.gui_scroll.play();
						
						if (this.selectedControl < this.controls.length - 1) {
							this.selectedControl++;
						} else {
							this.selectedControl = 0;
						}
						
						break;
					case game.config.list.main.controls.up:
						game.sounds.gui_scroll.play();
						
						if (this.selectedControl > 0) {
							this.selectedControl--;
						} else {
							this.selectedControl = this.controls.length - 1;
						}
						
						break;
				}
			}
		},
		
		renderBase: function(renderLogo) {
			const layer = [
				{
					type: "sprite",
					
					texture: game.textures.gui_menu_background,
					
					x: 0,
					y: 0
				}
			];
			
			if (renderLogo) {
				layer.push(
					{
						type: "sprite",
						
						texture: game.textures.gui_menu,
						
						x: 0,
						y: 0
					},
					
					{
						type: "sprite",
						
						texture: game.textures.gui_miniLogo,
						
						x: 48,
						y: 32,
						
						width: 64,
						height: 64
					}
				);
			}
			
			return layer;
		}
};
