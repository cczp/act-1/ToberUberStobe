Object.assign(game.sounds, {
	//Common
		gui_click:
			game.loadSound("sounds/gui/click.weba"),
		gui_scroll:
			game.loadSound("sounds/gui/scroll.weba"),
		
		gui_background:
			game.loadSound("sounds/gui/background.weba"),
	
	//Game world
		//GUI types
			gui_gameWorld_default_godModeSwitching:
				game.loadSound("sounds/gui/game-world/default/god-mode-switching.weba"),
		
		//Item types
			items_eating:
				game.loadSound("sounds/items/eating.weba"),
			
			items_drinking:
				game.loadSound("sounds/items/drinking.weba"),
			items_splashing:
				game.loadSound("sounds/items/splashing.weba"),
			
			items_placement:
				game.loadSound("sounds/items/placement.weba"),
		
		//Object types
			//Blocks
				blocks_death:
					game.loadSound("sounds/blocks/death.weba"),
				
				blocks_door_closing:
					game.loadSound("sounds/blocks/door/closing.weba"),
				
				blocks_chest_closing:
					game.loadSound("sounds/blocks/chest/closing.weba"),
			
			//Entities
				entities_death:
					game.loadSound("sounds/entities/death.weba"),
				
				entities_attack:
					game.loadSound("sounds/entities/attack.weba"),
				
				entities_itemCatching:
					game.loadSound("sounds/entities/item-catching.weba"),
				
				entities_walking:
					game.loadSound("sounds/entities/walking.weba")
});

game.sounds.gui_background.loop = true;
