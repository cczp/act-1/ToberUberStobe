Object.assign(game.textures, {
	//Common
		missingte:
			game.loadTexture("textures/missingte.png"),
		
		gui_miniLogo:
			game.loadTexture("textures/gui/mini-logo.png"),
		
		gui_button:
			game.loadTexture("textures/gui/button.png"),
		
		gui_squareButton:
			game.loadTexture("textures/gui/square-button.png"),
		
		gui_textField:
			game.loadTexture("textures/gui/text-field.png"),
		
		gui_itemSelector:
			game.loadTexture("textures/gui/item-selector.png"),
	
	//Controls
		gui_controls:
			game.loadTexture("textures/gui/controls.png"),
	
	//Menu
		gui_menu:
			game.loadTexture("textures/gui/menu.png"),
		gui_menu_background:
			game.loadTexture("textures/gui/menu/background.png"),
	
	//Settings
		gui_settings:
			game.loadTexture("textures/gui/settings.png"),
	
	//Game world
		gui_gameWorld_darkness:
			game.loadTexture("textures/gui/game-world/darkness.png"),
		
		//GUI types
			gui_gameWorld_default:
				game.loadTexture("textures/gui/game-world/default.png"),
			gui_gameWorld_default_indicators:
				game.loadTexture("textures/gui/game-world/default/indicators.png"),
			
			gui_gameWorld_pause:
				game.loadTexture("textures/gui/game-world/pause.png"),
			
			gui_gameWorld_inventory:
				game.loadTexture("textures/gui/game-world/inventory.png"),
			gui_gameWorld_godInventory:
				game.loadTexture("textures/gui/game-world/god-inventory.png"),
			
			gui_gameWorld_container:
				game.loadTexture("textures/gui/game-world/container.png"),
			
			gui_gameWorld_oven:
				game.loadTexture("textures/gui/game-world/oven.png"),
			
			gui_gameWorld_magicBox:
				game.loadTexture("textures/gui/game-world/magic-box.png"),
			
			gui_gameWorld_console:
				game.loadTexture("textures/gui/game-world/console.png"),
			
			gui_gameWorld_dyeMaker:
				game.loadTexture("textures/gui/game-world/dye-maker.png"),
			gui_gameWorld_dyeMaker_colors:
				game.loadTexture("textures/gui/game-world/dye-maker/colors.png"),
			
			gui_gameWorld_sign:
				game.loadTexture("textures/gui/game-world/sign.png"),
		
		//Background textures
			background_sand:
				game.loadTexture("textures/background/sand.png"),
			
			background_dirt:
				game.loadTexture("textures/background/dirt.png"),
			
			background_stone:
				game.loadTexture("textures/background/stone.png"),
			
			background_terracotta:
				game.loadTexture("textures/background/terracotta.png"),
			
			background_parquet:
				game.loadTexture("textures/background/parquet.png"),
		
		//Particle types
			particles_attack:
				game.loadTexture("textures/particles/attack.png"),
			
			particles_arrow:
				game.loadTexture("textures/particles/arrow.png"),
		
		//Item types
			//Food
				items_cactusPulp:
					game.loadTexture("textures/items/cactus-pulp.png"),
				
				items_catMeat:
					game.loadTexture("textures/items/cat-meat.png"),
				
				items_snakeMeat:
					game.loadTexture("textures/items/snake-meat.png"),
				
				items_foxMeat:
					game.loadTexture("textures/items/fox-meat.png"),
				
				items_gazelleMeat:
					game.loadTexture("textures/items/gazelle-meat.png"),
				
				items_tomato:
					game.loadTexture("textures/items/tomato.png"),
				
				items_carrot:
					game.loadTexture("textures/items/carrot.png"),
				
				items_eggplant:
					game.loadTexture("textures/items/eggplant.png"),
			
			//Drinks
				items_waterBottle:
					game.loadTexture("textures/items/water-bottle.png"),
				
				items_healingTea:
					game.loadTexture("textures/items/healing-tea.png"),
			
			//Tools
				items_stoneKnife:
					game.loadTexture("textures/items/stone-knife.png"),
				
				items_stoneAxe:
					game.loadTexture("textures/items/stone-axe.png"),
				
				items_stoneShovel:
					game.loadTexture("textures/items/stone-shovel.png"),
					
				items_ironKnife:
					game.loadTexture("textures/items/iron-knife.png"),
				
				items_ironAxe:
					game.loadTexture("textures/items/iron-axe.png"),
				
				items_ironShovel:
					game.loadTexture("textures/items/iron-shovel.png"),
				
				items_bow:
					game.loadTexture("textures/items/bow.png"),
			
			//Other
				items_dryGrass:
					game.loadTexture("textures/items/dry-grass.png"),
				
				items_grass:
					game.loadTexture("textures/items/grass.png"),
				
				items_log:
					game.loadTexture("textures/items/log.png"),
				
				items_leaves:
					game.loadTexture("textures/items/leaves.png"),
				
				items_sticks:
					game.loadTexture("textures/items/sticks.png"),
				
				items_bottle:
					game.loadTexture("textures/items/bottle.png"),
				
				items_sand:
					game.loadTexture("textures/items/sand.png"),
				
				items_dirt:
					game.loadTexture("textures/items/dirt.png"),
				
				items_rock:
					game.loadTexture("textures/items/rock.png"),
				
				items_sandCovering:
					game.loadTexture("textures/items/sand-covering.png"),
				
				items_dirtCovering:
					game.loadTexture("textures/items/dirt-covering.png"),
				
				items_stoneCovering:
					game.loadTexture("textures/items/stone-covering.png"),
				
				items_parquet:
					game.loadTexture("textures/items/parquet.png"),
				
				items_brick:
					game.loadTexture("textures/items/brick.png"),
				
				items_stoneWall:
					game.loadTexture("textures/items/stone-wall.png"),
				
				items_brickWall:
					game.loadTexture("textures/items/brick-wall.png"),
				
				items_woodenWall:
					game.loadTexture("textures/items/wooden-wall.png"),
				
				items_door:
					game.loadTexture("textures/items/door.png"),
				
				items_fence:
					game.loadTexture("textures/items/fence.png"),
				
				items_fenceGate:
					game.loadTexture("textures/items/fence-gate.png"),
				
				items_chest:
					game.loadTexture("textures/items/chest.png"),
				
				items_oven:
					game.loadTexture("textures/items/oven.png"),
				
				items_stool:
					game.loadTexture("textures/items/stool.png"),
				
				items_ironOre:
					game.loadTexture("textures/items/iron-ore.png"),
				
				items_ironIngot:
					game.loadTexture("textures/items/iron-ingot.png"),
				
				items_coal:
					game.loadTexture("textures/items/coal.png"),
				
				items_backgroundChanger:
					game.loadTexture("textures/items/background-changer.png"),
				
				items_selfreplacer:
					game.loadTexture("textures/items/selfreplacer.png"),
				
				items_particlePlacer:
					game.loadTexture("textures/items/particle-placer.png"),
				
				items_console:
					game.loadTexture("textures/items/console.png"),
				
				items_bones:
					game.loadTexture("textures/items/bones.png"),
				
				items_terracotta:
					game.loadTexture("textures/items/terracotta.png"),
				
				items_terracottaWall:
					game.loadTexture("textures/items/terracotta-wall.png"),
				
				items_floorTile:
					game.loadTexture("textures/items/floor-tile.png"),
				
				items_clay:
					game.loadTexture("textures/items/clay.png"),
				
				items_dyeMaker:
					game.loadTexture("textures/items/dye-maker.png"),
				
				items_dye:
					game.loadTexture("textures/items/dye.png"),
				
				items_sign:
					game.loadTexture("textures/items/sign.png"),
				
				items_zelyonkaBottle:
					game.loadTexture("textures/items/zelyonka-bottle.png"),
				
				items_shit:
					game.loadTexture("textures/items/shit.png"),
				
				items_glass:
					game.loadTexture("textures/items/glass.png"),
				
				items_arrow:
					game.loadTexture("textures/items/arrow.png"),
		
		//Object types
			//Blocks
				blocks_smallCactus:
					game.loadTexture("textures/blocks/small-cactus.png"),
				
				blocks_bigCactus:
					game.loadTexture("textures/blocks/big-cactus.png"),
				
				blocks_dryGrass:
					game.loadTexture("textures/blocks/dry-grass.png"),
				
				blocks_grass:
					game.loadTexture("textures/blocks/grass.png"),
				
				blocks_acacia:
					game.loadTexture("textures/blocks/acacia.png"),
				
				blocks_rock:
					game.loadTexture("textures/blocks/rock.png"),
				
				blocks_stoneWall:
					game.loadTexture("textures/blocks/stone-wall.png"),
				
				blocks_brickWall:
					game.loadTexture("textures/blocks/brick-wall.png"),
				
				blocks_woodenWall:
					game.loadTexture("textures/blocks/wooden-wall.png"),
				
				blocks_door:
					game.loadTexture("textures/blocks/door.png"),
				
				blocks_fence:
					game.loadTexture("textures/blocks/fence.png"),
				
				blocks_fenceGate:
					game.loadTexture("textures/blocks/fence-gate.png"),
				
				blocks_barrier:
					game.loadTexture("textures/blocks/barrier.png"),
				
				blocks_chest:
					game.loadTexture("textures/blocks/chest.png"),
				
				blocks_oven:
					game.loadTexture("textures/blocks/oven.png"),
				
				blocks_stool:
					game.loadTexture("textures/blocks/stool.png"),
				
				blocks_stone:
					game.loadTexture("textures/blocks/stone.png"),
				
				blocks_sand:
					game.loadTexture("textures/blocks/sand.png"),
				
				blocks_dirt:
					game.loadTexture("textures/blocks/dirt.png"),
				
				blocks_tomato:
					game.loadTexture("textures/blocks/tomato.png"),
				
				blocks_carrot:
					game.loadTexture("textures/blocks/carrot.png"),
				
				blocks_eggplant:
					game.loadTexture("textures/blocks/eggplant.png"),
				
				blocks_ironOre:
					game.loadTexture("textures/blocks/iron-ore.png"),
				
				blocks_coal:
					game.loadTexture("textures/blocks/coal.png"),
				
				blocks_backgroundChanger:
					game.loadTexture("textures/blocks/background-changer.png"),
				
				blocks_selfreplacer:
					game.loadTexture("textures/blocks/selfreplacer.png"),
				
				blocks_particlePlacer:
					game.loadTexture("textures/blocks/particle-placer.png"),
				
				blocks_console:
					game.loadTexture("textures/blocks/console.png"),
				
				blocks_haloxylon:
					game.loadTexture("textures/blocks/haloxylon.png"),
				
				blocks_puddle:
					game.loadTexture("textures/blocks/puddle.png"),
				
				blocks_terracotta:
					game.loadTexture("textures/blocks/terracotta.png"),
				
				blocks_terracottaWall:
					game.loadTexture("textures/blocks/terracotta-wall.png"),
				
				blocks_sign:
					game.loadTexture("textures/blocks/sign.png"),
			
			//Entities
				entities_player:
					game.loadTexture("textures/entities/player.png"),
				
				entities_cat:
					game.loadTexture("textures/entities/cat.png"),
				
				entities_snake:
					game.loadTexture("textures/entities/snake.png"),
				
				entities_fox:
					game.loadTexture("textures/entities/fox.png"),
				
				entities_gazelle:
					game.loadTexture("textures/entities/gazelle.png"),
	
	//Game over
		gui_gameOver:
			game.loadTexture("textures/gui/game-over.png")
});
