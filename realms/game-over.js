game.realms.gameOver = {
	//Secondary variables
		
	
	//Methods
		update: function() {
			this.handleInput();
			
			return [[
				{
					type: "sprite",
					
					texture: game.textures.gui_gameOver,
					
					x: 0,
					y: 0
				},
				
				{
					type: "text",
					
					fontSize: 16,
					fontColor: game.colorScheme.text_light,
					textAlign: "center",
					
					string: game.currentLocale.gui_gameOver_title,
					
					x: 160,
					y: 120
				}
			]];
		},
		
		handleInput: function() {
			for (let i = 0; i < game.input.keyboard.pressed.length; i++) {
				switch (game.input.keyboard.pressed[i]) {
					case game.config.list.main.controls.quit:
						game.sounds.gui_click.play();
						
						game.currentRealm = game.realms.menu;
						
						break;
				}
			}
		}
};
