TLDR: it's complicated; better try to figure everything out by yourself, it'll be more interesting

## Contents

* [Terminology](#terminology)
* [Controls](#controls)
* [Basic configuration](#basic-configuration)
	* [Changing the controls](#changing-the-controls)
	* [Changing the language](#changing-the-language)
* [Basic playing process](#basic-playing-process)
	* Too much sections with weird names

## Terminology

* Realm - room/scene/whatever normal game engines call it (e.g. menu, settings, etc)
* Game world - realm where you actually play
* Background - background part of the map (sand/rocks/etc)
* Object - block (oven, chest, grass, etc) or entity (mobs and "alive" things)
* Particle - uninteractible objects, displayed on the layer above the objects
* Settings - realm where you can configure the game
* GUI type - "subrealms" of the game world

## Controls

*This is the default controls, you can change them anytime in the settings*

* WASD (called Up, Left, Down and Right or Move) - move (**everywhere** including the menu, all GUI types, etc)
* G (called Switch) - go to the inventory (or GUI of the object the player facing)/change GUI type mode
* H (called Quit) - pause the game (when in the game world)/quit current realm or GUI type
* K (called Use) - press a button/select something/beat an object/move an item in a container
* L (called Run) - start running
* T (called Command) - enter command
* Y (called Magic) - toggle the God mode/remove the item (in the inventory)

Also you can just use your mouse/touchscreen + the back button of your browser/Android phone

## Basic configuration

### Changing the controls

1. Go to settings-\>controls/controls (2)
2. Select the control you want to change with the Up and Down keys
3. Press the Use key
4. Find the code of the key [here](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code#Code_values)
5. Enter it to the dialog
6. Press the okay button or Enter

### Changing the language

1. Go to settings-\>language page
2. Select your language with the Up and Down keys
3. Press the Use key to select it
or
2. Select the text field
3. Press the Use key
4. Enter the locale of your language

*List of the locales:*

* en\_US - English (US)
* ru\_RU - Russian (Russia)
* ec\_HO - prints the string name instead of the string itself
* cr\_ASH - crashes the game instantly

## Basic playing process

### Walking

1. Press one of the Move keys

### Beating and placing objects/using items (tools, food, drinks, etc)/moving items in containers

1. Press the Use key

### Opening the inventory/objects' GUIs/switching beetwen their elements

1. Press the Switch key

### Toggling the God mode/removing items in the inventory

1. Press the Magic key

### Navigating in the *some realm or GUI type*

1. Use the Move keys

### Working with the inventory

1. Open it with the Switch key
2. Select some item with the Move keys
3. Press the Use key to swap the selected item and the item in the hand slot (the first)
or
3. Press the Magic key to destroy it

### Working with containers (chests, ovens, players)

1. Rotate yourself (by walking) to the container you need
2. Open the container with the Switch key
3. Use the Switch key to select the container's/your inventory
4. Select the item you need with the Move keys
5. Press the Use key to transfer the item

### Crafting

*Works only without the God mode. Requires correct item order*

1. Open the inventory with the Switch key
2. Select items that are parts of the recipe with the Switch key
3. Select where crafted item will lie with the Move keys
4. Press the Use key

### Cooking

1. Get a oven
2. Place it somewhere
3. Rotate yourself (by walking) to it
4. Open it with the Switch key
5. Place fuel and the ingredient with the Use key
6. Switch to the oven's container with the Switch key
7. Select the cooked ingredient with the Move keys
8. Pick it up with the Use key

### Writing on signs

1. Get a sign
2. Press the Use key to place it
3. Enter the text in the dialog
4. Type \n if you want to break the line
5. Press Enter or the okay button to finish
