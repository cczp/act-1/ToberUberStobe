* Log + rock -> sticks + rock
* Rock + sand -> brick
* Deep orange terracotta + dye -> dyed terracotta
* Terracotta/sand/dirt/rock/log -> 2 floor tiles/sand/dirt/stone coverings/parquets
* 2 sand/dirt coverings -> sand/dirt
* 3 rocks -> sign
* 2 logs -> door
* 3 logs -> chest
* 3 bricks -> oven
* 4 bricks/logs/rocks/2 terracottas -> brick/wooden/stone/terracotta wall
* 2 rocks -> raw stone knife
* 4 sticks -> fence
* 6 sticks -> fence gate
* Sticks + 2 rocks -> raw stone axe
* Sticks + 4 rocks -> raw stone shovel
* 2 iron ingots -> stone knife
* Sticks + 2 iron ingots -> iron axe
* Sticks + 4 iron ingots -> iron shovel
* 2 sticks + 2 grass -> bow
* Damaged bow + grass -> bow
* Sticks + bones -> arrow
* Bottle + 3 cactus pulp/tomatos/eggplants -> water bottle
* Bottle + teal dye -> zelyonka bottle
* Bottle + amber dye  + 2 leaves -> raw healing tea
* Tomato + cactus pulp + eggplant -> dye maker
* Glass + sticks -> bottle
